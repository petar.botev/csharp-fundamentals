﻿using System;

namespace _2._Divison
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            int[] arr = new int[5] { 10, 7, 6, 3, 2};

            for (int i = 0; i < arr.Length; i++)
            {
                if (n % arr[i] == 0)
                {
                    Console.WriteLine($"The number is divisible by {arr[i]}");
                    return;
                }
            }

            Console.WriteLine("Not divisible");
        }
    }
}
