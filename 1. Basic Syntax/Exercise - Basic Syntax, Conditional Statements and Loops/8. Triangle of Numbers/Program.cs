﻿using System;
using System.Text;

namespace _8._Triangle_of_Numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int num = int.Parse(Console.ReadLine());
            StringBuilder strb = new StringBuilder();

            for (int i = 1; i <= num; i++)
            {
                for (int j = 0; j < i; j++)
                {
                    strb.Append($"{i} ");
                }
                strb.Append("\n");
            }
            Console.Write(strb.ToString());
        }
    }
}
