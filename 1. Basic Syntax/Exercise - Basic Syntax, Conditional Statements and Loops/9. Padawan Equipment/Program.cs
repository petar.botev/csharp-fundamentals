﻿using System;

namespace _9._Padawan_Equipment
{
    class Program
    {
        static void Main()
        {
            decimal availableMoney = decimal.Parse(Console.ReadLine());
            int studentsCount = int.Parse(Console.ReadLine());
            decimal lightsaberPrice = decimal.Parse(Console.ReadLine());
            decimal robePrice = decimal.Parse(Console.ReadLine());
            decimal beltPrice = decimal.Parse(Console.ReadLine());

            decimal totalPrice = studentsCount * lightsaberPrice + studentsCount * robePrice + studentsCount * beltPrice;
            decimal tenPercentFromStudents = Math.Ceiling((10m / 100) * studentsCount);
            decimal aditionalLightsaber = tenPercentFromStudents * lightsaberPrice;
           
            decimal beltDiscount = Math.Floor(studentsCount / 6m) * beltPrice;
            totalPrice = (totalPrice + aditionalLightsaber) - beltDiscount;

            if (availableMoney >= totalPrice)
            {
                Console.WriteLine($"The money is enough - it would cost {totalPrice:f2}lv.");

            }
            else
            {
                var difference = totalPrice - availableMoney;
                Console.WriteLine($"Ivan Cho will need {difference:f2}lv more.");

            }
        }
    }
}
