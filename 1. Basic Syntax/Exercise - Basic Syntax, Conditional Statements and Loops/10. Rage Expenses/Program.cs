﻿using System;

namespace _10._Rage_Expenses
{
    class Program
    {
        static void Main(string[] args)
        {
            int lostGames = int.Parse(Console.ReadLine());
            decimal headsetPrice = decimal.Parse(Console.ReadLine());
            decimal mousePrice = decimal.Parse(Console.ReadLine());
            decimal keyboardPrice = decimal.Parse(Console.ReadLine());
            decimal displayPrice = decimal.Parse(Console.ReadLine());

            decimal totalPrice = 0;
            int keyboardsToBuy = 0;

            totalPrice += Math.Floor(lostGames / 2m) * headsetPrice;

            for (int i = 3; i <= lostGames; i+=3)
            {
                if (i % 2 == 0)
                {
                    keyboardsToBuy++;
                }
            }
            int mouseToBuy = lostGames / 3;
            int displayToBuy = keyboardsToBuy / 2;
            totalPrice += keyboardsToBuy * keyboardPrice + displayToBuy * displayPrice + mouseToBuy * mousePrice;
            Console.WriteLine($"Rage expenses: {totalPrice:f2} lv.");
        }
    }
}
