﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _7._Vending_Machine
{
    class Program
    {
        static void Main(string[] args)
        {
            decimal[] coins = new decimal[] { 0.1m, 0.2m, 0.5m, 1m, 2m };
            Dictionary<string, decimal> products = new Dictionary<string, decimal>();
            products.Add("Nuts", 2.0m);
            products.Add("Water", 0.7m);
            products.Add("Crisps", 1.5m);
            products.Add("Soda", 0.8m);
            products.Add("Coke", 1.0m);

            decimal moneySum = 0;

            string insertedCoin;

            while (true)
            {
                insertedCoin = Console.ReadLine();
                if (insertedCoin == "Start")
                {
                    break;
                }
                if (!coins.Contains(decimal.Parse(insertedCoin)))
                {
                    Console.WriteLine($"Cannot accept {decimal.Parse(insertedCoin)}");
                    continue;
                }

                moneySum += decimal.Parse(insertedCoin);
            }


            string productToBuy;

            while (true)
            {
                productToBuy = Console.ReadLine();
                if (productToBuy == "End")
                {
                    break;
                }

                if (!products.ContainsKey(productToBuy))
                {
                    Console.WriteLine("Invalid product");
                    continue;
                }

                if (moneySum - products[productToBuy] >= 0)
                {
                    Console.WriteLine($"Purchased {productToBuy.ToLower()}");
                    moneySum -= products[productToBuy];
                }
                else
                {
                    Console.WriteLine("Sorry, not enough money");
                }
            }

            Console.WriteLine($"Change: {moneySum:f2}");
        }
    }
}
