﻿using System;
using System.Linq;
using System.Numerics;

namespace _6._Strong_Number
{
    class Program
    {
        static void Main(string[] args)
        {
            string number = Console.ReadLine();

            double sum = 0;

            foreach (var num in number)
            {
                sum += Factorial(int.Parse(num.ToString()));
            }

            if (sum.ToString() == string.Join("", number))
            {
                Console.WriteLine("yes");
            }
            else
            {
                Console.WriteLine("no");
            }
        }
        public static double Factorial(int number)
        {
            if (number == 0)
            {
                return 1;
            }
            double result = 1;
            while (number != 1)
            {
                result *= number;
                number -= 1;
            }
            return result;
        }
    }
}
