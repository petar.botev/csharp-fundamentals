﻿using System;

namespace _3._Vacation
{
    class Program
    {
        static int groupCount;
        static string groupCategory;
        static string dayOfTheWeek;
        static decimal price;

        static void Main()
        {
            groupCount = int.Parse(Console.ReadLine());
            groupCategory = Console.ReadLine();
            dayOfTheWeek = Console.ReadLine();

            decimal[] studentsPrices = new decimal[] { 8.45m, 9.80m, 10.46m };
            decimal[] businessPrices = new decimal[] { 10.90m, 15.60m, 16m };
            decimal[] regularPrices = new decimal[] { 15m, 20m, 22.50m };

            switch (groupCategory)
            {
                case "Students":
                    switch (dayOfTheWeek)
                    {
                        case "Friday":
                            CalculateTotalPrice(studentsPrices[0]);
                            break;
                        case "Saturday":
                            CalculateTotalPrice(studentsPrices[1]);
                            break;
                        case "Sunday":
                            CalculateTotalPrice(studentsPrices[2]);
                            break;
                        default:
                            break;
                    }
                    break;
                case "Business":
                    switch (dayOfTheWeek)
                    {
                        case "Friday":
                            CalculateTotalPrice(businessPrices[0]);
                            break;
                        case "Saturday":
                            CalculateTotalPrice(businessPrices[1]);
                            break;
                        case "Sunday":
                            CalculateTotalPrice(businessPrices[2]);
                            break;
                        default:
                            break;
                    }
                    break;
                case "Regular":
                    switch (dayOfTheWeek)
                    {
                        case "Friday":
                            CalculateTotalPrice(regularPrices[0]);
                            break;
                        case "Saturday":
                            CalculateTotalPrice(regularPrices[1]);
                            break;
                        case "Sunday":
                            CalculateTotalPrice(regularPrices[2]);
                            break;
                        default:
                            break;
                    }
                    break;
                default:
                    break;
            }

            Console.WriteLine($"Total price: {price:f2}");
        }

        static decimal CalculateTotalPrice(decimal specificPrice)
        {
            price = groupCount * specificPrice;
            if (groupCategory == "Students" && groupCount >= 30)
            {
                price -= price * (15m / 100);
            }
            else if (groupCategory == "Business" && groupCount >= 100)
            {
                    price -= specificPrice * 10m;
            }
            else if (groupCategory == "Regular" && groupCount >= 10 && groupCount <= 20)
            {
                price -= price * (5m / 100);
            }

            return price;
        }
    }
}
