﻿using System;
using System.Text;

namespace _4._Print_and_Sum
{
    class Program
    {
        static void Main(string[] args)
        {
            int fromNumber = int.Parse(Console.ReadLine());
            int toNumber = int.Parse(Console.ReadLine());
            int sum = 0;
            StringBuilder strb = new StringBuilder();

            for (int i = fromNumber; i <= toNumber; i++)
            {
                sum += i;
                strb.Append($"{i} ");
            }
            Console.WriteLine(strb.ToString());
            Console.WriteLine($"Sum: {sum}");
        }
    }
}
