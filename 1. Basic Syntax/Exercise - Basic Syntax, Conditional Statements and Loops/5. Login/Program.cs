﻿using System;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace _5._Login
{
    class Program
    {
        static void Main(string[] args)
        {
            string userName = Console.ReadLine();
            StringBuilder strb = new StringBuilder();
            for (int j = userName.Length - 1; j >= 0; j--)
            {
                strb.Append(userName[j]);
            }
            StringBuilder printResult = new StringBuilder();

            for (int i = 0; i < 3; i++)
            {
                string password = Console.ReadLine();

                if (password == strb.ToString())
                {
                    if (i != 0)
                    {
                        Console.Write(printResult.ToString());
                    }
                    
                    Console.WriteLine($"User {userName} logged in.");
                    return;
                }
                else
                {
                    if (i == 2)
                    {
                        printResult.Append("Incorrect password. Try again.");

                    }
                    else
                    {
                        printResult.Append("Incorrect password. Try again.\n");

                    }
                }
            }
            Console.WriteLine(printResult.ToString());
            Console.WriteLine($"User {userName} blocked!");
        }
    }
}
