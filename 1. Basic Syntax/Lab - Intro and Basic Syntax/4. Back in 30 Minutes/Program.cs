﻿using System;

namespace _4._Back_in_30_Minutes
{
    class Program
    {
        static void Main(string[] args)
        {
            int hours = int.Parse(Console.ReadLine());
            int minutes = int.Parse(Console.ReadLine());

            DateTime time = new DateTime(2020, 11 , 1, hours, minutes, 0);

            int additionalMinutes = 30;

            Console.WriteLine($"{time.AddMinutes(additionalMinutes):H:mm} ");
        }
    }
}
