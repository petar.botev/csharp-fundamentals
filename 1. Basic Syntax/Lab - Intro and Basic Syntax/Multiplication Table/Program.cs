﻿using System;
using System.Text;

namespace Multiplication_Table
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            StringBuilder strb = new StringBuilder();

            for (int i = 1; i <= 10; i++)
            {
                int res = n * i;
                strb.AppendLine($"{n} X {i} = {res}");
            }

            Console.WriteLine(strb.ToString());
        }
    }
}
