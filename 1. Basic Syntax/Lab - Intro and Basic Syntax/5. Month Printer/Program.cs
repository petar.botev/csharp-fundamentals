﻿using System;

namespace _5._Month_Printer
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            switch (n)
            {
                case 1: Console.WriteLine("January");
                    break;
                case 2: Console.WriteLine("February");
                    break;
                case 3: Console.WriteLine("March");
                    break;
                case 4: Console.WriteLine("April");
                    break;
                    
                default:
                    break;
            }

            Console.WriteLine("Hello World!");
        }
    }
}
