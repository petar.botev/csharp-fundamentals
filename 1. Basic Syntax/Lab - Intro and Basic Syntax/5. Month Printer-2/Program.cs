﻿using System;

namespace _5._Month_Printer_2
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());

            if (n < 1 || n > 12)
            {
                Console.WriteLine("Error!");
            }
            else
            {
                DateTime date = new DateTime(2020, n, 2);
                Console.WriteLine(date.ToString("MMMM"));
            }
        }
    }
}
