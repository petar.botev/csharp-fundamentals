﻿using System;
using System.Text;

namespace _11._Multiplication_Table_2._0
{
    class Program
    {
        static void Main(string[] args)
        {
            int n = int.Parse(Console.ReadLine());
            int multiplier = int.Parse(Console.ReadLine());
            StringBuilder strb = new StringBuilder();

            if (multiplier > 10)
            {
                int res = n * multiplier;
                strb.AppendLine($"{n} X {multiplier} = {res}");
            }
            else
            {
                for (int i = multiplier; i <= 10; i++)
                {
                    int res = n * i;
                    strb.AppendLine($"{n} X {i} = {res}");
                }
            }

            Console.WriteLine(strb.ToString());
        }
    }
}
