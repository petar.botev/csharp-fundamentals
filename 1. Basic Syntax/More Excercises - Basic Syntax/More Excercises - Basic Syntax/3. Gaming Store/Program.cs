﻿using System;
using System.Collections.Generic;

namespace _3._Gaming_Store
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, decimal> gamePriceDict = new Dictionary<string, decimal>();
            gamePriceDict.Add("OutFall 4", 39.99m);
            gamePriceDict.Add("CS: OG", 15.99m);
            gamePriceDict.Add("Zplinter Zell", 19.99m);
            gamePriceDict.Add("Honored 2", 59.99m);
            gamePriceDict.Add("RoverWatch", 29.99m);
            gamePriceDict.Add("RoverWatch Origins Edition", 39.99m);

            decimal budget = decimal.Parse(Console.ReadLine());
            decimal spentMoney = 0;

            while (true)
            {
                string gameToBuy = Console.ReadLine();

                if (gameToBuy == "Game Time")
                {
                    Console.WriteLine($"Total spent: ${spentMoney:f2}. Remaining: ${budget:f2}");
                    break;
                }

                if (!gamePriceDict.ContainsKey(gameToBuy))
                {
                    Console.WriteLine("Not Found");
                    continue;
                }

                if ((budget - gamePriceDict[gameToBuy]) < 0)
                {
                    Console.WriteLine("Too Expensive");
                    continue;
                }
                else if ((budget - gamePriceDict[gameToBuy]) == 0)
                {
                    Console.WriteLine("Out of money!");
                    return;
                }
                else
                {
                    budget -= gamePriceDict[gameToBuy];
                    spentMoney += gamePriceDict[gameToBuy];
                    Console.WriteLine($"Bought {gameToBuy}");
                }

            }
        }
    }
}
