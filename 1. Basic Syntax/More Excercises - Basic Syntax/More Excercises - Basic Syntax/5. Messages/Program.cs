﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _5._Messages
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int, Dictionary<int, char>> keyboard = new Dictionary<int, Dictionary<int, char>>();
            keyboard.Add(2, new Dictionary<int, char>() { { 0, 'a' },{ 1, 'b' }, { 2, 'c' }, });
            keyboard.Add(3, new Dictionary<int, char>() { { 0, 'd' },{ 1, 'e' }, { 2, 'f' }, });
            keyboard.Add(4, new Dictionary<int, char>() { { 0, 'g' },{ 1, 'h' }, { 2, 'i' }, });
            keyboard.Add(5, new Dictionary<int, char>() { { 0, 'j' },{ 1, 'k' }, { 2, 'l' }, });
            keyboard.Add(6, new Dictionary<int, char>() { { 0, 'm' },{ 1, 'n' }, { 2, 'o' }, });
            keyboard.Add(7, new Dictionary<int, char>() { { 0, 'p' },{ 1, 'q' }, { 2, 'r' }, { 3, 's'} });
            keyboard.Add(8, new Dictionary<int, char>() { { 0, 't' },{ 1, 'u' }, { 2, 'v' }, });
            keyboard.Add(9, new Dictionary<int, char>() { { 0, 'w' },{ 1, 'x' }, { 2, 'y' }, { 3, 'z'} });


            int wordLength = int.Parse(Console.ReadLine());
            List<char> theFinalWord = new List<char>();

            for (int i = 0; i < wordLength; i++)
            {
                int letter = int.Parse(Console.ReadLine());
                int lastDigit = letter % 10;

                if (lastDigit == 0)
                {
                    theFinalWord.Add(' ');
                    continue;
                }

                var momentLetter = keyboard[lastDigit][letter.ToString().Count() - 1];
                theFinalWord.Add(momentLetter);
            }

            Console.WriteLine(string.Join("", theFinalWord));
        }
    }
}
