﻿using System;
using System.Linq;

namespace _4._Reverse_String
{
    class Program
    {
        static void Main(string[] args)
        {
            string str = Console.ReadLine();
            //char[] str = Console.ReadLine().ToCharArray().Reverse().ToArray();
            char[] arr = str.Reverse().ToArray();
            string result = string.Join("", arr);
            
            Console.WriteLine(result);
        }
    }
}
