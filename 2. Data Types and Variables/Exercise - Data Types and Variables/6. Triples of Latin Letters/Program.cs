﻿using System;
using System.Text;

namespace _6._Triples_of_Latin_Letters
{
    class Program
    {
        static void Main(string[] args)
        {
            int numberOfLetters = int.Parse(Console.ReadLine());
            int asciiFirstLetter = 97;

            StringBuilder strb = new StringBuilder();

            for (int i = asciiFirstLetter; i < asciiFirstLetter + numberOfLetters; i++)
            {
                for (int j = asciiFirstLetter; j < asciiFirstLetter + numberOfLetters; j++)
                {
                    for (int k = asciiFirstLetter; k < asciiFirstLetter + numberOfLetters; k++)
                    {
                        strb.AppendLine($"{(char)i}{(char)j}{(char)k}");
                    }
                }
            }

            Console.WriteLine(strb.ToString());
        }
    }
}
