﻿using System;
using System.Text;

namespace _5._Print_Part_of_the_ASCII_Table
{
    class Program
    {
        static void Main()
        {
            int fromNumber = int.Parse(Console.ReadLine());
            int toNumber = int.Parse(Console.ReadLine());

            StringBuilder strb = new StringBuilder();

            for (int i = fromNumber; i <= toNumber; i++)
            {
                strb.Append($"{(char)i} ");
            }

            Console.WriteLine(strb.ToString());
        }
    }
}
