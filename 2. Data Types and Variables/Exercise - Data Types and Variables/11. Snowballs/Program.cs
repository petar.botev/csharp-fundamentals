﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace _11._Snowballs
{
    class Program
    {
        static void Main()
        {
            int snowballNumber = int.Parse(Console.ReadLine());
            SnowBall[] arrSnawballs = new SnowBall[snowballNumber];

            for (int i = 0; i < snowballNumber; i++)
            {
                double snowballSnow = double.Parse(Console.ReadLine());
                double snowballTime = double.Parse(Console.ReadLine());
                double snowballQuality = double.Parse(Console.ReadLine());
                double snowballValue = Math.Pow((snowballSnow / snowballTime), snowballQuality);

                SnowBall snowball = new SnowBall() { SnowballSnow = snowballSnow,
                                                     SnowballTime = snowballTime,
                                                     SnowballQuality = snowballQuality,
                                                     SnowballValue = snowballValue
                };

                arrSnawballs[i] = snowball;
            }


            SnowBall maxValue = new SnowBall();
            foreach (var snowball in arrSnawballs)
            {
                if (snowball.SnowballValue >= maxValue.SnowballValue)
                {
                    maxValue.SnowballValue = snowball.SnowballValue;
                    maxValue.SnowballTime = snowball.SnowballTime;
                    maxValue.SnowballSnow = snowball.SnowballSnow;
                    maxValue.SnowballQuality = snowball.SnowballQuality;
                }
                
            }

            Console.WriteLine($"{maxValue.SnowballSnow} : {maxValue.SnowballTime} = {maxValue.SnowballValue} ({maxValue.SnowballQuality})");
        }
    }

    class SnowBall
    {
        private double snowballSnow;
        private double snowballTime;
        private double snowballQuality;
        private double snowballValue;

        public double SnowballSnow { get => snowballSnow; set => snowballSnow = value; }
        public double SnowballTime { get => snowballTime; set => snowballTime = value; }
        public double SnowballQuality { get => snowballQuality; set => snowballQuality = value; }
        public double SnowballValue { get => snowballValue; set => snowballValue = value; }
    }
}
