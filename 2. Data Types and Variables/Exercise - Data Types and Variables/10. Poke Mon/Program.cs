﻿using System;

namespace _10._Poke_Mon
{
    class Program
    {
        static void Main()
        {
            int pokePower = int.Parse(Console.ReadLine());
            int distance = int.Parse(Console.ReadLine());
            int exhaustionFactor = int.Parse(Console.ReadLine());
            int momentPower = pokePower;
            int pokeNumber = 0;

            while (momentPower >= distance)
            {
                pokeNumber++;
                momentPower -= distance;
                if (momentPower == pokePower / 2 && exhaustionFactor > 0)
                {
                    momentPower /= exhaustionFactor;
                }
            }

            Console.WriteLine(momentPower);
            Console.WriteLine(pokeNumber);
        }
    }
}
