﻿using System;

namespace _4._Sum_of_Chars
{
    class Program
    {
        static void Main(string[] args)
        {
            int numberOfCharacters = int.Parse(Console.ReadLine());
            int sum = 0;

            for (int i = 0; i < numberOfCharacters; i++)
            {
                char character = char.Parse(Console.ReadLine());

                sum += character;
            }

            Console.WriteLine($"The sum equals: {sum}");
        }
    }
}
