﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _8._Beer_Kegs
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = int.Parse(Console.ReadLine());
            Dictionary<string, double> kegDict = new Dictionary<string, double>();


            for (int i = 0; i < number; i++)
            {
                string kegModel = Console.ReadLine();
                double kegRadius = double.Parse(Console.ReadLine());
                int kegHeight = int.Parse(Console.ReadLine());

                double kegVolume = Math.PI * Math.Pow(kegRadius, 2) * kegHeight;

                kegDict[kegModel] = kegVolume;
            }

             string result = kegDict.FirstOrDefault(k => k.Value == kegDict.Values.Max()).Key;
            
            Console.WriteLine(result);
        }
    }
}
