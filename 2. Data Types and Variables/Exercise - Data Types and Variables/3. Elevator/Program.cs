﻿using System;

namespace _3._Elevator
{
    class Program
    {
        static void Main(string[] args)
        {
            int people = int.Parse(Console.ReadLine());
            int peoplePerCourse = int.Parse(Console.ReadLine());

            int courses = (int)Math.Ceiling((double)people / peoplePerCourse);
            
            Console.WriteLine(courses);
        }
    }
}
