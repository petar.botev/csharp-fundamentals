﻿using System;
using System.Numerics;

namespace _9._Spice_Must_Flow
{
    class Program
    {
        static void Main()
        {
            uint startingYild = uint.Parse(Console.ReadLine());
            BigInteger yild = 0;
            uint consumedYild = 26;
            uint days = 0;

            if (startingYild > 99)
            {
                while (startingYild >= 100)
                {
                    yild += (startingYild - consumedYild);

                    startingYild -= 10;

                    days++;
                }

                yild -= consumedYild;
            }
            else
            {
                yild = 0;
            }

            Console.WriteLine(days);
            Console.WriteLine(yild);
        }
    }
}
