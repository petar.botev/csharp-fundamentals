﻿using System;

namespace _7._Water_Overflow
{
    class Program
    {
        static void Main(string[] args)
        {
            int numberVolumes = int.Parse(Console.ReadLine());
            int capacity = 255;
            int littersInTheTank = 0;

            for (int i = 0; i < numberVolumes; i++)
            {
                int volume = int.Parse(Console.ReadLine());

                capacity -= volume;

                if (capacity < 0)
                {
                    capacity += volume;
                    Console.WriteLine("Insufficient capacity!");
                }
                else
                {
                    littersInTheTank += volume;
                }
            }

            Console.WriteLine(littersInTheTank);
        }
    }
}
