﻿using System;
using System.Linq;

namespace _6._Reversed_Chars
{
    class Program
    {
        static void Main(string[] args)
        {
            char first = char.Parse(Console.ReadLine());
            char second = char.Parse(Console.ReadLine());
            char third = char.Parse(Console.ReadLine());

            char[] arr = new char[] { first, second, third };

            arr = arr.Reverse().ToArray();

            Console.WriteLine(string.Join(' ', arr));
        }
    }
}
