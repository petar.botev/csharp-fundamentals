﻿using System;

namespace _9._Chars_to_String
{
    class Program
    {
        static void Main()
        {
            char char1 = char.Parse(Console.ReadLine());
            char char2 = char.Parse(Console.ReadLine());
            char char3 = char.Parse(Console.ReadLine());

            Console.WriteLine(char1.ToString() + char2.ToString() + char3.ToString());
        }
    }
}
