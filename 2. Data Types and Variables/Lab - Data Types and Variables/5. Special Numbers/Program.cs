﻿using System;
using System.Text;

namespace _5._Special_Numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = int.Parse(Console.ReadLine());
            int digitSum = 0;
            int n;

            StringBuilder strb = new StringBuilder();

            for (int i = 1; i <= number; i++)
            {
                n = i;
                while (n > 0)
                {
                    digitSum += (n % 10);
                    n /= 10;
                }

                if (digitSum == 5 || digitSum == 7 || digitSum == 11)
                {
                    strb.AppendLine($"{i} -> True");
                }
                else
                {
                    strb.AppendLine($"{i} -> False");
                }

                digitSum = 0;
            }

            Console.WriteLine(strb.ToString());
        }
    }
}
