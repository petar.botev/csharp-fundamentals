﻿using System;

namespace _12._Refactor_Special_Numbers
{
    class Program
    {
        static void Main()
        {
            int number = int.Parse(Console.ReadLine());
            int sum = 0;
            int recentNumber = 0;
            bool isSpecial = false;

            for (int ch = 1; ch <= number; ch++)
            {
                recentNumber = ch;
                while (recentNumber > 0)
                {
                    sum += recentNumber % 10;
                    recentNumber = recentNumber / 10;
                }
                isSpecial = (sum == 5) || (sum == 7) || (sum == 11);
                
                Console.WriteLine("{0} -> {1}", ch, isSpecial);
                sum = 0;
            }
        }
    }
}
