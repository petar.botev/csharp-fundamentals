﻿using System;

namespace _3._Floating_Equality
{
    class Program
    {
        static void Main()
        {
            double numberA = double.Parse(Console.ReadLine());
            double numberB = double.Parse(Console.ReadLine());
            double epsConstant = 0.000001;

            double difference = 0;

            if (numberA > numberB)
            {
                difference = numberA - numberB;
            }
            else
            {
                difference = numberB - numberA;
            }

            if (difference >= epsConstant)
            {
                Console.WriteLine("False");
            }
            else
            {
                Console.WriteLine("True");
            }
        }
    }
}
