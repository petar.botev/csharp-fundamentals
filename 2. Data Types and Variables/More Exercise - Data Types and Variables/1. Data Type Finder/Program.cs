﻿using System;
using System.Numerics;

namespace _1._Data_Type_Finder
{
    class Program
    {
        static void Main()
        {
            string input = "";

            while ((input = Console.ReadLine()) != "END")
            {
                if (int.TryParse(input, out int intNumber))
                {
                    Console.WriteLine($"{input} is integer type");
                }
                else if (double.TryParse(input, out double decimalNumber ))
                {
                    Console.WriteLine($"{input} is floating point type");
                }
                else if (char.TryParse(input, out char isChar))
                {
                    Console.WriteLine($"{input} is character type");
                }
                else if (bool.TryParse(input, out bool isBool))
                {
                    Console.WriteLine($"{input} is boolean type");
                }
                else
                {
                    Console.WriteLine($"{input} is string type");
                }
            }
        }
    }
}
