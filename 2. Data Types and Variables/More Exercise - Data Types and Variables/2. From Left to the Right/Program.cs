﻿using System;
using System.Numerics;

namespace _2._From_Left_to_the_Right
{
    class Program
    {
        static void Main()
        {
            int number = int.Parse(Console.ReadLine());
            int numSum = 0;

            for (int i = 0; i < number; i++)
            {
                string numbers = Console.ReadLine();

                string[] splittedNumbers = numbers.Split(' ');

                if (BigInteger.Parse(splittedNumbers[0]) > BigInteger.Parse(splittedNumbers[1]))
                {
                    foreach (var num in splittedNumbers[0])
                    {
                        if (char.IsDigit(num))
                        {
                            numSum += int.Parse(num.ToString());
                        }
                        
                    }
                }
                else
                {
                    foreach (var num in splittedNumbers[1])
                    {
                        if (char.IsDigit(num))
                        {
                            numSum += int.Parse(num.ToString());
                        }
                    }
                }

                Console.WriteLine(numSum);
                numSum = 0;
            }
        }
    }
}
