﻿using System;
using System.Text.RegularExpressions;

namespace _01._Furniture_2
{
    class Program
    {
        static void Main()
        {
            string pattern = @">>(?<item>[A-Za-z]+)<<(?<price>\d+\.?\d*)!(?<count>\d+)";

            Regex regex = new Regex(pattern);

            string input = Console.ReadLine();

            decimal sum = 0;

            Console.WriteLine("Bought furniture:");

            while (input != "Purchase")
            {
                Match match = regex.Match(input);

                if (match.Success)
                {
                    string name = match.Groups[1].Value;
                    decimal price = decimal.Parse(match.Groups[2].Value);
                    int quantity = int.Parse(match.Groups[3].Value);

                    Console.WriteLine(name);

                    sum += price * quantity;
                }

                input = Console.ReadLine();
            }

        }
    }
}
