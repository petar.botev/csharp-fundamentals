﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace _06._Extract_Emails
{
    class Program
    {
        static void Main()
        {
            string input = Console.ReadLine();
            
            Regex regex = new Regex(@"(?<=\s|^)[A-Za-z0-9]([-._]?[A-Za-z0-9])*@[A-Za-z]([-]?[A-Za-z])*(\.[A-Za-z]([-]?[A-Za-z])*)+");

            var emails = regex.Matches(input).Select(x => x.Value).ToArray();

            foreach (var item in emails)
            {
                Console.WriteLine(item);
            }
        }
    }
}
