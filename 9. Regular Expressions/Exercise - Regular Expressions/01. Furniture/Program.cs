﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace _01._Furniture
{
    class Program
    {
        static void Main()
        {
            //List<string> furnitures = new List<string>();
            decimal totalPrice = 0;

            // StringBuilder strb = new StringBuilder();
            string command = Console.ReadLine();

            Console.WriteLine("Bought furniture:");

            string pattern = @">>(?<item>[A-Za-z]+)<<(?<price>\d+\.?\d*)!(?<count>\d+)";

            Regex regex = new Regex(pattern);

            while (command != "Purchase")
            {
                Match match = regex.Match(command);

                if (match.Success)
                {
                    string item = match.Groups[1].Value;
                    decimal price = decimal.Parse(match.Groups[2].Value);
                    int count = int.Parse(match.Groups[3].Value);

                    //furnitures.Add(item);

                    totalPrice += price * count;

                    Console.WriteLine($"{item}");
                }

                command = Console.ReadLine();
            }

            Console.WriteLine($"Total money spend: {totalPrice:f2}");

            //strb.AppendLine("Bought furniture: ");

            //foreach (var item in furnitures)
            //{
            //    strb.AppendLine($"{item}");
            //}

            //strb.AppendLine($"Total money spend: {totalPrice}");

            //Console.WriteLine(strb.ToString());
        }
    }
}
