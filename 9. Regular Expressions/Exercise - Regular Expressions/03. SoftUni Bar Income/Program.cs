﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace _03._SoftUni_Bar_Income
{
    class Program
    {
        static void Main()
        {
            string order;
            decimal totalIncome = 0;

            while ((order = Console.ReadLine()) != "end of shift")
            {
                Regex regex = new Regex(@"%(?<name>[A-Z][a-z]+)%[^%.|$]*<(?<product>\w+)>[^%.|$]*\|(?<count>\d+)\|[^%.|$\d]*(?<price>\d+\.?\d+)\$");

                Match inputMatch = regex.Match(order);

                if (inputMatch.Success)
                {
                    decimal price = decimal.Parse(inputMatch.Groups["count"].Value) * decimal.Parse(inputMatch.Groups["price"].Value);

                    Console.WriteLine($"{inputMatch.Groups["name"].Value}: {inputMatch.Groups["product"].Value} - {price:f2}");

                    totalIncome += price;
                }
            }

            Console.WriteLine($"Total income: {totalIncome:f2}");
        }
    }
}

