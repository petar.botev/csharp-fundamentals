﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace _02._Race
{
    class Program
    {
        static void Main()
        {
            Dictionary<string, double> nameDict = new Dictionary<string, double>();


            string namePattern = @"[A-Za-z]+";
            string kilometerPattern = @"\d+";

            string[] participants = Console.ReadLine().Split(", ", StringSplitOptions.RemoveEmptyEntries);

            string input = Console.ReadLine();

            Regex nameRegex = new Regex(namePattern);
            Regex kilometerRegex = new Regex(kilometerPattern);

            for (int i = 0; i < participants.Length; i++)
            {
                nameDict.Add(participants[i], 0);
            }

            while (input != "end of race")
            {
                MatchCollection kilometerMatch = kilometerRegex.Matches(input);
                MatchCollection nameMatch = nameRegex.Matches(input);

                if (nameMatch.Count > 0 && kilometerMatch.Count > 0)
                {
                    string name = string.Join("", nameMatch);
                    double kilometers = string.Join("", kilometerMatch).Sum(k => double.Parse(k.ToString()));

                    if (nameDict.ContainsKey(name))
                    {
                        nameDict[name] += kilometers;
                    }
                }

                input = Console.ReadLine();
            }

            nameDict = nameDict.OrderByDescending(n => n.Value).Take(3).ToDictionary(k => k.Key, v => v.Value);

            StringBuilder strb = new StringBuilder();
            int j = 1;

            foreach (var item in nameDict)
            {
                if (j == 1)
                {
                    strb.AppendLine($"{j}st place: {item.Key}");
                }
                else if (j == 2)
                {
                    strb.AppendLine($"{j}nd place: {item.Key}");
                }
                else
                {
                    strb.AppendLine($"{j}rd place: {item.Key}");
                }

                j++;
            }

            Console.WriteLine(strb.ToString());
        }
    }
}
