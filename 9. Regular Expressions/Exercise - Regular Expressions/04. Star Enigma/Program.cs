﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace _04._Star_Enigma
{
    class Program
    {
        static void Main()
        {
            int messageNumber = int.Parse(Console.ReadLine());
            List<string> destroyedPlanets = new List<string>();
            List<string> attackedPLanets = new List<string>();


            for (int i = 0; i < messageNumber; i++)
            {
                string message = Console.ReadLine();

                Regex regex = new Regex(@"[starSTAR]");

                MatchCollection matches = regex.Matches(message);

                int substractValue = matches.Count();

                string substractMessage = string.Join("", message.Select(ch => (char)(ch - substractValue)));

                Regex regexArg = new Regex(@"@(?<planet>[A-Za-z]+)[^@\-!:>]*:(?<population>\d+)[^@\-!:>]*!(?<status>[AD])![^@\-!:>]*->(?<soldierCount>\d+)");

                Match matchPlanet = regexArg.Match(substractMessage);

                if (matchPlanet.Success)
                {
                    if (matchPlanet.Groups["status"].Value == "A")
                    {
                        attackedPLanets.Add(matchPlanet.Groups["planet"].Value);
                    }
                    else if (matchPlanet.Groups["status"].Value == "D")
                    {
                        destroyedPlanets.Add(matchPlanet.Groups["planet"].Value);
                    }
                }

            }

            Console.WriteLine($"Attacked planets: {attackedPLanets.Count()}");

            attackedPLanets = attackedPLanets.OrderBy(a => a).ToList();

            foreach (var aPlanet in attackedPLanets)
            {
                Console.WriteLine($"-> {aPlanet}");
            }

            Console.WriteLine($"Destroyed planets: {destroyedPlanets.Count()}");

            destroyedPlanets = destroyedPlanets.OrderBy(d => d).ToList();

            foreach (var dPlanet in destroyedPlanets)
            {
                Console.WriteLine($"-> {dPlanet}");
            }
        }
    }
}
