﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace _05._Nether_Realms
{
    class Program
    {
        static void Main()
        {
            string[] strings = Console.ReadLine().Split(",", StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToArray();

            strings = strings.OrderBy(x => x).ToArray();

            for (int i = 0; i < strings.Length; i++)
            {
                Regex healthRegex = new Regex(@"[^0-9+\-*\/\.]");
                Regex damageRegex = new Regex(@"[-+]?\d+\.?\d*");

                var health = healthRegex.Matches(strings[i]).Select(x => (int)char.Parse(x.Value)).ToArray().Sum();
                var damage = damageRegex.Matches(strings[i]).Select(x => double.Parse(x.Value)).ToArray().Sum();

                var multiplyOrDevide = strings[i].Where(x => x == '*' || x == '/').ToArray();

                foreach (var item in multiplyOrDevide)
                {
                    if (item == '*')
                    {
                        damage *= 2;
                    }
                    else
                    {
                        damage /= 2;
                    }
                }

                Console.WriteLine($"{strings[i]} - {health} health, {damage:f2} damage");
                
            }
        }
    }
}
