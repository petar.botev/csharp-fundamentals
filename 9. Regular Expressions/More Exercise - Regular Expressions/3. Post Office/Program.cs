﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace _3._Post_Office
{
    class Program
    {
        static void Main()
        {
            string[] input = Console.ReadLine().Split('|');

            string firstPart = input[0];
            string secondPart = input[1];
            string[] thirdPart = input[2].Split(' ', StringSplitOptions.RemoveEmptyEntries);

            Regex firstReg = new Regex(@"([#$%*&])(?<capitalLetters>[A-Z]+)\1");
            Regex secondReg = new Regex(@"(?<letter>\d+):(?<length>\d{2})(?!\.)");

            Match firstMatch = firstReg.Match(firstPart);
            MatchCollection wordLengths = secondReg.Matches(secondPart);

            string capitalLetters = firstMatch.Groups["capitalLetters"].Value;
            
            List<string> allWords = new List<string>();

            for (int i = 0; i < capitalLetters.Length; i++)
            {
                for (int j = 0; j < wordLengths.Count; j++)
                {
                    if (capitalLetters[i] == int.Parse(wordLengths[j].Groups["letter"].Value))
                    {
                        int wordLength = int.Parse(wordLengths[j].Groups["length"].Value) + 1;

                        for (int k = 0; k < thirdPart.Length; k++)
                        {
                            if (thirdPart[k][0] == capitalLetters[i] && thirdPart[k].Length == wordLength)
                            {
                                if (!allWords.Contains(thirdPart[k]))
                                {
                                    allWords.Add(thirdPart[k]);
                                }
                            }
                        }
                    }
                }
            }

            foreach (var item in allWords)
            {
                Console.WriteLine(item);
            }
        }
    }
}
