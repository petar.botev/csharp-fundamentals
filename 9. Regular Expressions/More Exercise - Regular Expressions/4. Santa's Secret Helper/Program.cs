﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace _4._Santa_s_Secret_Helper
{
    class Program
    {
        static void Main()
        {
            int key = int.Parse(Console.ReadLine());

            StringBuilder decriptedMessage = new StringBuilder();

            string message;

            while ((message = Console.ReadLine()) != "end")
            {
                for (int i = 0; i < message.Length; i++)
                {
                    char modifiedChar = (char)(message[i] - key);
                    decriptedMessage.Append(modifiedChar);
                }

                string newMessage = decriptedMessage.ToString();

                Regex regex = new Regex(@"@(?<name>[A-Za-z]+)[^@\-!:>]+!(?<status>[G])!");

                Match match = regex.Match(newMessage);

                if (match.Success)
                {
                    Console.WriteLine(match.Groups["name"].Value);
                }

                decriptedMessage.Clear();
            }
        }
    }
}
