﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Linq;


namespace _2._Rage_Quit_2
{
    class Program
    {
        static void Main()
        {
            string input = Console.ReadLine().Trim().ToUpper();

            StringBuilder strbSymbols = new StringBuilder();
            StringBuilder strbDigits = new StringBuilder();
            StringBuilder strbResult = new StringBuilder();

            for (int i = 0; i < input.Length; i++)
            {
                if (char.IsDigit(input[i]))
                {
                    strbDigits.Append(input[i]);

                    if ( i == input.Length - 1 || !char.IsDigit(input[i+1]))
                    {
                        int repeats = int.Parse(strbDigits.ToString());

                        for (int j = 0; j < repeats; j++)
                        {
                            strbResult.Append(strbSymbols);
                        }
                        strbDigits.Clear();
                        strbSymbols.Clear();
                    }
                }
                else
                {
                    strbSymbols.Append(input[i]);
                }
            }

            int countUnique = strbResult.ToString().Distinct().Count();

            Console.WriteLine($"Unique symbols used: {countUnique}");
            Console.WriteLine(strbResult.ToString());
        }
    }
}
