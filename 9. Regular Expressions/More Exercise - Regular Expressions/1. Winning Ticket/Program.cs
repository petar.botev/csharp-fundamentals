﻿using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace _1._Winning_Ticket
{
    class Program
    {
        static void Main()
        {
            string[] input = Console.ReadLine()
                .Split(',', StringSplitOptions.RemoveEmptyEntries)
                .Select(x => x.Trim())
                .ToArray();

            foreach (var ticket in input)
            {
                if (!(ticket.Length == 20))
                {
                    Console.WriteLine("invalid ticket");
                }
                else
                {
                    string firstTicketPart = ticket.Substring(0, 10);
                    string secondTicketPart = ticket.Substring(10);

                    Regex countRegex = new Regex(@"([$#^@])\1{5,}");

                    var countMatchFirstPart = countRegex.Match(firstTicketPart);
                    var countMatchSecondPart = countRegex.Match(secondTicketPart);

                    if (countMatchFirstPart.Success && countMatchSecondPart.Success)
                    {
                        if (countMatchFirstPart.Value[1] == countMatchSecondPart.Value[1])
                        {
                            if (countMatchFirstPart.Length == 10 && countMatchSecondPart.Length == 10)
                            {
                                Console.WriteLine($"ticket \"{ticket}\" - {countMatchSecondPart.Length}{countMatchSecondPart.Value[4]} Jackpot!");
                            }
                            else
                            {

                                Console.WriteLine($"ticket \"{ticket}\" - {Math.Min(countMatchSecondPart.Length, countMatchFirstPart.Length)}{countMatchSecondPart.Value[4]}");

                            }
                        }
                    }
                    else
                    {
                        Console.WriteLine($"ticket \"{ticket}\" - no match");
                    }
                }
            }
        }
    }
}
