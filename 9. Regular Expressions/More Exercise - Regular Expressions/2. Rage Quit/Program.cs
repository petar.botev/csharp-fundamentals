﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;

namespace _2._Rage_Quit
{
    class Program
    {
        static void Main(string[] args)
        {
            var timer = new Stopwatch();

            timer.Start();
            string input = Console.ReadLine().Trim().ToUpper();

            Regex regex = new Regex(@"[^0-9]+\d+");

            var matches = regex.Matches(input).Select(x => x.Value).ToArray();

            StringBuilder strb = new StringBuilder();

            foreach (var match in matches)
            {
                var symbols = string.Join("", match.Where(x => !char.IsDigit(x)));
                var digits = string.Join("", match.Where(x => char.IsDigit(x)));

                for (int i = 0; i < int.Parse(digits); i++)
                {
                    strb.Append(symbols);
                }
            }
 
            Console.WriteLine($"Unique symbols used: {UniqueSymbolNumber(input)}");
            Console.WriteLine(strb.ToString());

            //timer.Stop();
            //Console.WriteLine(timer.Elapsed.TotalSeconds);
            //;
            //return;
        }

        private static int UniqueSymbolNumber(string input)
        {
            int count = 0;
            bool isUnique;

            for (int i = 0; i < input.Length - 1; i++)
            {
                isUnique = true;
                Regex digReg = new Regex(@"\d+");
                Match digmatch = digReg.Match(input);

                for (int j = i+1; j < input.Length; j++)
                {
                    if (char.IsDigit(input[i]) || input[i].ToString().ToLower() == input[j].ToString().ToLower())
                    {
                        isUnique = false;
                        break;
                    }
                }

                if (isUnique == true)
                {
                    count++;
                }
            }

            return count;
        }
    }
}
