﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace Match_Dates
{
    class Program
    {
        static void Main()
        {
            string input = Console.ReadLine();

            Regex reg = new Regex(@"\b(?<day>\d{2})(?<delim>[-|.|\/])(?<month>[A-Z]{1}[a-z]{2})\<delim>(?<year>\d{4})\b");

            MatchCollection matches = reg.Matches(input);

            StringBuilder strb = new StringBuilder();

            foreach (Match item in matches)
            {

                strb.Append($"Day: {item.Groups["day"].Value}, Month: {item.Groups["month"].Value}, Year: {item.Groups["year"].Value}");

                Console.WriteLine(strb.ToString());
                strb.Clear();
            }
        }
    }
}
