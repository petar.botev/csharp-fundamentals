﻿using System;
using System.Text.RegularExpressions;

namespace _01._Reverse_Strings
{
    class Program
    {
        static void Main()
        {
            string pattern = @"\b[A-Z][a-z]+ [A-Z][a-z]+";

            string input = Console.ReadLine();

            MatchCollection matches = Regex.Matches(input, pattern);

            foreach (Match item in matches)
            {
                Console.Write($"{item.Value} ");
            }
        }
    }
}
