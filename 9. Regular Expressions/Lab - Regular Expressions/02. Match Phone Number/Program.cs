﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace _02._Match_Phone_Number
{
    class Program
    {
        static void Main()
        {
            string pattern = @"\+359([-| ])2\1\d{3}\1\d{4}\b";

            string input = Console.ReadLine();


            MatchCollection matchedNums = Regex.Matches(input, pattern);
            StringBuilder strb = new StringBuilder();

            foreach (Match item in matchedNums)
            {
                strb.Append($"{item.Value}, ");
            }

            Console.Write(strb.ToString().Trim(' ', ','));
        }
    }
}
