﻿using System;
using System.Text;

namespace _04._Caesar_Cipher
{
    class Program
    {
        static void Main()
        {
            string str = Console.ReadLine();
            StringBuilder strb = new StringBuilder();

            foreach (var item in str)
            {
                strb.Append((char)(item + 3));
            }

            Console.WriteLine(strb.ToString());
        }
    }
}
