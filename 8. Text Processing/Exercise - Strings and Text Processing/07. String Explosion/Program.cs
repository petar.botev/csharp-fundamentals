﻿using System;

namespace _07._String_Explosion
{
    class Program
    {
        static void Main()
        {
            string str = Console.ReadLine();
            int removeCount = 0;

            for (int i = 0; i < str.Length; i++)
            {
                if (str[i] == '>')
                {
                    if (char.IsDigit(str[i + 1]))
                    {
                        removeCount += int.Parse(str[i + 1].ToString());
                    }
                }
                else
                {
                    if (removeCount > 0)
                    {
                        str = str.Remove(i, 1);
                        i--;
                        removeCount--;
                    }
                }
            }

            Console.WriteLine(str);
        }
    }
}
