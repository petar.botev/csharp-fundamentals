﻿using System;

namespace _03._Extract_File
{
    class Program
    {
        static void Main()
        {
            string[] filePath = Console.ReadLine().Split('\\', StringSplitOptions.RemoveEmptyEntries);

            string[] fileName = filePath[filePath.Length - 1].Split('.');

            Console.WriteLine($"File name: {fileName[0]}\nFile extension: {fileName[1]}");
        }
    }
}
