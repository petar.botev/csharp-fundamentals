﻿using System;

namespace _01._Valid_Usernames
{
    class Program
    {
        static void Main()
        {
            string[] names = Console.ReadLine().Split(", ", StringSplitOptions.RemoveEmptyEntries);
            bool hasValidChars = true;

            foreach (var item in names)
            {
                if (item.Length >= 3 && item.Length <= 16)
                {
                    for (int i = 0; i < item.Length; i++)
                    {
                        if (!(char.IsLetter(item[i]) || char.IsDigit(item[i]) || item[i] == '-' || item[i] == '_'))
                        {
                            hasValidChars = false;
                            break;
                        }
                    }

                    if (hasValidChars)
                    {
                        Console.WriteLine(item);
                    }

                    hasValidChars = true;
                }
            }
        }
    }
}
