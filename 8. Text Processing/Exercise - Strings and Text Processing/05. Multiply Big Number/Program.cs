﻿using System;
using System.Linq;
using System.Text;

namespace _05._Multiply_Big_Number
{
    class Program
    {
        static void Main()
        {
            string bigNumber = Console.ReadLine().TrimStart('0');
            byte num = byte.Parse(Console.ReadLine());

            string bigNumberWithoutDot;
            int subStrInt = 0;

            int tempInt = 0;

            StringBuilder strb = new StringBuilder();

            if (bigNumber.Contains('.'))
            {
                int tempDotIndex = bigNumber.IndexOf('.');
                subStrInt = bigNumber.Substring(tempDotIndex).Length - 1;

                bigNumberWithoutDot = bigNumber.Remove(tempDotIndex, 1);
            }
            else
            {
                bigNumberWithoutDot = bigNumber;
            }
            

            if (num == 0)
            {
                strb.Append(0);
            }
            else
            {
                for (int i = bigNumberWithoutDot.Length - 1; i >= 0; i--)
                {

                    int multipl = (byte.Parse(bigNumberWithoutDot[i].ToString()) * num) + tempInt;

                    if (i == 0)
                    {
                        strb.Insert(0, multipl);
                        break;
                    }

                    if (multipl > 9)
                    {
                        tempInt = multipl / 10;
                    }
                    else
                    {
                        tempInt = 0;
                    }

                    strb.Insert(0, multipl % 10);
                }
            }

            if (bigNumber.Contains('.'))
            {
                var dotIndex = strb.ToString().Length - subStrInt;
                strb.Insert(dotIndex, ".");
                Console.WriteLine(strb.ToString());
            }
            else
            {
                Console.WriteLine(strb.ToString());
            }
        }
    }
}
