﻿using System;
using System.Text;

namespace _06._Replace_Repeating_Chars
{
    class Program
    {
        static void Main()
        {
            string str = Console.ReadLine();

            StringBuilder strb = new StringBuilder();

            strb.Append(str[0]);

            for (int i = 1; i < str.Length; i++)
            {
                if (str[i] != str[i - 1])
                {
                    strb.Append(str[i]);
                }
            }

            Console.WriteLine(strb.ToString());
        }
    }
}
