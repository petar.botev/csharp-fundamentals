﻿using System;

namespace _02._Character_Multiplier
{
    class Program
    {
        static void Main()
        {
            string[] words = Console.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries);

            Console.WriteLine(CharSum(words[0], words[1]));
        }

        static int CharSum(string firstStr, string secondStr)
        {
            int sum = 0;

            int stringLength;
            string longerStringLength;

            if (firstStr.Length <= secondStr.Length)
            {
                longerStringLength = secondStr;
                stringLength = firstStr.Length;
            }
            else
            {
                longerStringLength = firstStr;
                stringLength = secondStr.Length;
            }

            for (int i = 0; i < stringLength; i++)
            {
                sum += firstStr[i] * secondStr[i];
            }

            for (int i = longerStringLength.Length - 1; i > stringLength - 1; i--)
            {
                sum += longerStringLength[i];
            }

            return sum;
        }
    }
}
