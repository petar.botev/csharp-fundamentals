﻿using System;

namespace _03._Substring
{
    class Program
    {
        static void Main()
        {
            string word = Console.ReadLine().ToLower();
            string text = Console.ReadLine().ToLower();

            while (text.Contains(word.ToLower()))
            {
                int startIndex = text.IndexOf(word);
                text = text.Remove(startIndex, word.Length);
            }

            Console.WriteLine(text);
        }
    }
}
