﻿using System;
using System.Collections.Generic;

namespace _05._Digits__Letters_and_Other
{
    class Program
    {
        static void Main()
        {
            string symbols = Console.ReadLine();
            List<char> letters = new List<char>();
            List<char> digits = new List<char>();
            List<char> symbolList = new List<char>();

            foreach (var item in symbols)
            {
                if (char.IsLetter(item))
                {
                    letters.Add(item);
                }
                else if (char.IsDigit(item))
                {
                    digits.Add(item);
                }
                else
                {
                    symbolList.Add(item);
                }
            }

            Console.WriteLine($"{string.Join("", digits)}\n{string.Join("", letters)}\n{string.Join("", symbolList)}");
        }
    }
}
