﻿using System;
using System.Linq;

namespace _01._Reverse_Strings
{
    class Program
    {
        static void Main()
        {
            string word;
            while ((word = Console.ReadLine()) != "end")
            {
                string reversedWord = string.Join("", word.Reverse().ToArray());

                Console.WriteLine($"{word} = {reversedWord}");
            }
        }
    }
}
