﻿using System;
using System.Text;

namespace _02._Repeat_Strings
{
    class Program
    {
        static void Main()
        {
            string[] words = Console.ReadLine().Split();
            StringBuilder strb = new StringBuilder();

            for (int i = 0; i < words.Length; i++)
            {
                for (int j = 0; j < words[i].Length; j++)
                {
                    strb.Append(words[i]);
                }
            }

            Console.WriteLine(strb.ToString());
        }
    }
}
