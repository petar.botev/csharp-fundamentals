﻿using System;

namespace _3._Calculations
{
    class Program
    {
        static void Main()
        {
            string command = Console.ReadLine();
            int num1 = int.Parse(Console.ReadLine());
            int num2 = int.Parse(Console.ReadLine());

            switch (command)
            {
                case "add":
                    Console.WriteLine(Add(num1, num2));
                    break;
                case "subtract":
                    Console.WriteLine(Subtract(num1, num2));
                    break;
                case "multiply":
                    Console.WriteLine(Multiply(num1, num2));
                    break;
                case "divide":
                    Console.WriteLine(Divide(num1, num2));
                    break;
                default:
                    break;
            }
        }

        static float Add(int num1, int num2) 
        {
            return num1 + num2;
        }
        static float Subtract(int num1, int num2)
        {
            return num1 - num2;
        }

        static float Multiply(int num1, int num2)
        {
            return num1 * num2;
        }
        static float Divide(int num1, int num2)
        {
            return num1 / num2;
        }
    }
}
