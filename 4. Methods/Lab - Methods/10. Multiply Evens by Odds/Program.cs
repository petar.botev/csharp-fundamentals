﻿using System;

namespace _10._Multiply_Evens_by_Odds
{
    class Program
    {
        static void Main(string[] args)
        {
            string number = Console.ReadLine();

            GetMultipleOfEvenAndOdds(number);
        }

        static void GetMultipleOfEvenAndOdds(string num)
        {
            Console.WriteLine(GetSumOfEvenDigits(num) * GetSumOfOddDigits(num));
        }

        static int GetSumOfEvenDigits(string num)
        {
            if (num[0] == '-')
            {
                num = num.Remove(0, 1);
            }

            int sumEven = 0;

            for (int i = 0; i < num.Length; i++)
            {
                if (int.Parse(num[i].ToString()) % 2 == 0)
                {
                    sumEven += int.Parse(num[i].ToString());
                }
            }

            return sumEven;
        }

        static int GetSumOfOddDigits(string num)
        {
            if (num[0] == '-')
            {
                num = num.Remove(0, 1);
            }

            int sumOdd = 0;

            for (int i = 0; i < num.Length; i++)
            {
                if (int.Parse(num[i].ToString()) % 2 != 0)
                {
                    sumOdd += int.Parse(num[i].ToString());
                }
            }

            return sumOdd;
        }
    }
}
