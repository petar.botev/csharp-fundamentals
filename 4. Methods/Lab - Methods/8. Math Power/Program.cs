﻿using System;

namespace _8._Math_Power
{
    class Program
    {
        static void Main()
        {
            double firstNum = double.Parse(Console.ReadLine());
            int secondNum = int.Parse(Console.ReadLine());

            Console.WriteLine(Power(firstNum, secondNum));
        }

        static double Power(double num1, int num2)
        {
            return Math.Pow(num1, num2);
        }
    }
}
