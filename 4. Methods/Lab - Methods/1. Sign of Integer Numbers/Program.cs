﻿using System;

namespace _1._Sign_of_Integer_Numbers
{
    class Program
    {
        static void Main(string[] args)
        {
            int number = int.Parse(Console.ReadLine());
            Console.WriteLine(PrintNumberSign(number));
        }

        static string PrintNumberSign(int num)
        {
            if (num < 0)
            {
                return $"The number {num} is negative.";
            }
            else if (num > 0)
            {
                return $"The number {num} is positive.";
            }
            else
            {
                return $"The number {num} is zero.";
            }
        }
    }
}
