﻿using System;
using System.Runtime.CompilerServices;

namespace _4._Printing_Triangle
{
    class Program
    {
        static void Main()
        {
            int number = int.Parse(Console.ReadLine());

            PrintTriangle(number);
        }

        static void PrintLine(int start, int end)
        {
            for (int i = start; i <= end; i++)
            {
                Console.Write(i + " ");
            }
        }

        static void PrintTriangle(int num)
        {
            for (int i = 1; i <= num; i++)
            {
               PrintLine(1,i);
                Console.WriteLine();
            }

            for (int i = num - 1; i >= 1; i--)
            {
                PrintLine(1, i);
                Console.WriteLine();
            }
        }
    }
}
