﻿using System;

namespace _9._Greater_of_Two_Values
{
    class Program
    {
        
        static void Main()
        {
            string valueType = Console.ReadLine();

            if (valueType == "int")
            {
                int val1 = int.Parse(Console.ReadLine());
                int val2 = int.Parse(Console.ReadLine());
                GetMax(val1, val2);
            }
            else if (valueType == "string")
            {
                string val1 = Console.ReadLine();
                string val2 = Console.ReadLine();
                GetMax(val1, val2);
            }
            else if (valueType == "char")
            {
                char val1 = char.Parse(Console.ReadLine());
                char val2 = char.Parse(Console.ReadLine());
                GetMax(val1, val2);
            }
        }
               
        static void GetMax(int val1, int val2)
        {
            if (val1 > val2)
            {
                Console.WriteLine(val1);
            }
            else
            {
                Console.WriteLine(val2);
            }
        }

        static void GetMax(string val1, string val2)
        {
            if (val1[0] > val2[0])
            {
                Console.WriteLine(val1);
            }
            else
            {
                Console.WriteLine(val2);
            }
            //int val1Sum = 0;
            //int val2Sum = 0;

            //foreach (var item in val1)
            //{
            //    val1Sum += item; 
            //}

            //foreach (var item in val2)
            //{
            //    val2Sum += item;
            //}

            //if (val1Sum > val2Sum)
            //{
            //    Console.WriteLine(val1);
            //}
            //else
            //{
            //    Console.WriteLine(val2);
            //}
        }

        static void GetMax(char val1, char val2)
        {
            if (val1 > val2)
            {
                Console.WriteLine(val1);
            }
            else
            {
                Console.WriteLine(val2);
            }
        }
    }
}
