﻿using System;
using System.Text;

namespace _7._Repeat_String
{
    class Program
    {
        static void Main()
        {
            string str = Console.ReadLine();
            int repeatTimes = int.Parse(Console.ReadLine());

            Console.WriteLine(Repeat(str, repeatTimes));
        }

        static string Repeat(string str, int repeatTimes)
        {
            StringBuilder strb = new StringBuilder();
            for (int i = 0; i < repeatTimes; i++)
            {
                strb.Append($"{str}");
            }

            return strb.ToString();
        }
    }
}
