﻿using System;

namespace _2._Grades
{
    class Program
    {
        static void Main(string[] args)
        {
            float grade = float.Parse(Console.ReadLine());

            Console.WriteLine(GradeWithWord(grade));
        }

        static string GradeWithWord(float grade)
        {
            if (grade < 3)
            {
                return "Fail";
            }
            else if (grade >= 3 && grade < 3.5)
            {
                return "Poor";
            }
            else if (grade >= 3.5 && grade < 4.5)
            {
                return "Good";
            }
            else if (grade >= 4.5 && grade < 5.5)
            {
                return "Very good";
            }
            else
            {
                return "Excellent";
            }
        }
    }
}
