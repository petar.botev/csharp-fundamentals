﻿using System;

namespace _11._Math_Operations
{
    class Program
    {
        static void Main()
        {
            int firstNumber = int.Parse(Console.ReadLine());
            char charOperator = char.Parse(Console.ReadLine());
            int secondNumber = int.Parse(Console.ReadLine());

            Calculate(firstNumber, charOperator, secondNumber);
        }

        static void Calculate(int num1, char charOperator, int num2)
        {
            switch (charOperator)
            {
                case '+': Console.WriteLine(num1 + num2);
                    break;
                case '-': Console.WriteLine(num1 - num2);
                    break;
                case '*': Console.WriteLine(num1 * num2);
                    break;
                case '/': Console.WriteLine(num1 / num2);
                    break;
                default:
                    break;
            }
        }
    }
}
