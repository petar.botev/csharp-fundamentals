﻿using System;

namespace _5._Orders
{
    class Program
    {
        private const decimal COFFEE_PRICE = 1.50m;
        private const decimal WATER_PRICE = 1.00m;
        private const decimal COKE_PRICE = 1.40m;
        private const decimal SNACKS_PRICE = 2.00m;

        static void Main()
        {
            string goods = Console.ReadLine();
            int quantity = int.Parse(Console.ReadLine());

            TotalBaveragePrice(goods, quantity);
        }

        static void TotalBaveragePrice(string goods, int quantity)
        {
            switch (goods)
            {
                case "coffee": Console.WriteLine($"{COFFEE_PRICE * quantity:f2}");
                    break;
                case "water": Console.WriteLine($"{WATER_PRICE * quantity:f2}");
                    break;
                case "coke": Console.WriteLine($"{COKE_PRICE * quantity}");
                    break;
                case "snacks": Console.WriteLine($"{SNACKS_PRICE * quantity}");
                    break;
                default:
                    break;
            }
        }
    }
}
