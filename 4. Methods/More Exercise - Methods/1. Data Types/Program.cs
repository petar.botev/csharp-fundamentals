﻿using System;

namespace _1._Data_Types
{
    class Program
    {
        static void Main()
        {
            string command = Console.ReadLine();
            string inputString = Console.ReadLine();

            switch (command)
            {
                case "int": Console.WriteLine(Calculate(int.Parse(inputString)));
                    break;
                case "real": Console.WriteLine($"{Calculate(double.Parse(inputString)):f2}");
                    break;
                case "string": Console.WriteLine(Calculate(inputString));
                    break;
                default:
                    break;
            }
        }

        static int Calculate(int value)
        {
            return value * 2;
        }

        static double Calculate(double value)
        {
            return value * 1.5;
        }

        static string Calculate(string value)
        {
            return $"${value}$";
        }
    }
}
