﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _4._Tribonacci_Sequence
{
    class Program
    {
        static void Main()
        {
            int number = int.Parse(Console.ReadLine());

            Console.WriteLine(string.Join(' ', AllTribonacciNums(number)));
        }

        static List<int> AllTribonacciNums (int num)
        {
            List<int>tribonacciNums = new List<int>();
            int tribonacciSum = 0;

            for (int i = 0; i < num; i++)
            {
                if (i < 3)
                {
                    if (i == 0)
                    {
                        tribonacciNums.Add(1);
                    }
                    else
                    {
                        tribonacciNums.Add(tribonacciNums.Sum());
                    }
                }
                else
                {
                    for (int j = tribonacciNums.Count - 1; j >= tribonacciNums.Count - 3; j--)
                    {
                        tribonacciSum += tribonacciNums[j];
                    }

                    tribonacciNums.Add(tribonacciSum);
                    tribonacciSum = 0;
                }
            }

            return tribonacciNums;
        }
    }
}
