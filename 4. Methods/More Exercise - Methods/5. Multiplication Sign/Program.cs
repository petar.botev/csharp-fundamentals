﻿using System;
using System.Linq;

namespace _5._Multiplication_Sign
{
    class Program
    {
        static void Main()
        {
            double num1 = double.Parse(Console.ReadLine());
            double num2 = double.Parse(Console.ReadLine());
            double num3 = double.Parse(Console.ReadLine());

            double[] numsArr = new double[] { num1, num2, num3 };

            SignOfProduct(numsArr);
        }

        static bool CheckForZero(double[] numsArr)
        {
            if (numsArr.Contains(0))
            {
                return true;
            }

            return false;
        }

        static int CountNegativeNumbers(double[] numsArr)
        {
            int countNegative = 0;
            
            if (numsArr[0] < 0)
            {
                countNegative++;
            }
            else if (numsArr[1] < 0)
            {
                countNegative++;
            }
            else if (numsArr[2] < 0)
            {
                countNegative++;
            }

            return countNegative;
        }

        static void SignOfProduct(double[] numsArr)
        {
            if (CountNegativeNumbers(numsArr) % 2 == 0 && CheckForZero(numsArr) == false)
            {
                Console.Write("positive");
            }
            else if (CountNegativeNumbers(numsArr) % 2 != 0 && CheckForZero(numsArr) == false)
            {
                Console.Write("negative");
            }
            else 
            {
                Console.Write("zero");
            }
        }
    }
}
