﻿using System;
using System.Text;

namespace _3._Longer_Line
{
    class Program
    {
        static void Main()
        {
            float x1 = float.Parse(Console.ReadLine());
            float y1 = float.Parse(Console.ReadLine());
            float x2 = float.Parse(Console.ReadLine());
            float y2 = float.Parse(Console.ReadLine());
            float x3 = float.Parse(Console.ReadLine());
            float y3 = float.Parse(Console.ReadLine());
            float x4 = float.Parse(Console.ReadLine());
            float y4 = float.Parse(Console.ReadLine());

            PrintCoordinates(x1, y1, x2, y2, x3, y3, x4, y4);
        }
        public static void CloseToZero(float x1, float y1, float x2, float y2)
        {
            if (Math.Abs(x1) + Math.Abs(y1) <= Math.Abs(x2) + Math.Abs(y2))
            {
                Console.Write($"({x1}, {y1})({x2}, {y2})");
            }
            else
            {
                Console.Write($"({x2}, {y2})({x1}, {y1})");
            }
        }

        static void PrintCoordinates(float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4)
        {
            StringBuilder strb = new StringBuilder();

            if (LineLength(x1, y1, x2, y2) > LineLength(x3, y3, x4, y4))
            {
                CloseToZero(x1, y1, x2, y2);
            }
            else
            {
                CloseToZero(x3, y3, x4, y4);
            }
        }
        
        static double LineLength(float x1, float y1, float x2, float y2)
        {
            float sideA;
            float sideB;

            if (x1 > x2)
            {
                sideA = x1 - x2;
            }
            else
            {
                sideA = x2 - x1;
            }

            if (y1 > y2)
            {
                sideB = y1 - y2;
            }
            else
            {
                sideB = y2 - y1;
            }

            double sideC = Math.Sqrt(Math.Pow(sideA, 2) + Math.Pow(sideB, 2));
            return sideC;
        }
    }
}
