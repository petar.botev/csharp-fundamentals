﻿using System;

namespace _2._Center_Point
{
    public class Program
    {
        static void Main()
        {
            float x1 = float.Parse(Console.ReadLine());
            float y1 = float.Parse(Console.ReadLine());
            float x2 = float.Parse(Console.ReadLine());
            float y2 = float.Parse(Console.ReadLine());

            CloseToZero(x1, y1, x2, y2);
        }

        public static void CloseToZero(float x1, float y1, float x2, float y2)
        {
            if (Math.Abs(x1) + Math.Abs(y1) <= Math.Abs(x2) + Math.Abs(y2))
            {
                Console.Write($"({x1}, {y1})");
            }
            else
            {
                Console.Write($"({x2}, {y2})");
            }
        }
    }
}
