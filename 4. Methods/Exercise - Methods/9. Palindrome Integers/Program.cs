﻿using System;
using System.Linq;

namespace _9._Palindrome_Integers
{
    class Program
    {
        static void Main()
        {
            string number;

            while ((number = Console.ReadLine()) != "END")
            {
                Console.WriteLine(IsPalindrom(number).ToString().ToLower());
            }
        }

        static bool IsPalindrom(string number) 
        {
            string[] toArray = number.Select(x => x.ToString()).ToArray();
            string[] numberArray = toArray.Reverse().ToArray();

            if (toArray.SequenceEqual(numberArray))
            {
                return true;
            }

            return false;
        }
    }
}
