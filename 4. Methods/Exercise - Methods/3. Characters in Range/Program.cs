﻿using System;
using System.Text;

namespace _3._Characters_in_Range
{
    class Program
    {
        static void Main()
        {
            char firstChar = char.Parse(Console.ReadLine());
            char secondChar = char.Parse(Console.ReadLine());

            PrintMiddleChars(firstChar, secondChar);
        }

        static void PrintMiddleChars(char firstChar, char secondChar)
        {
            StringBuilder strb = new StringBuilder();

            if (firstChar < secondChar)
            {
                for (char i = (char)(firstChar + 1); i < secondChar; i++)
                {
                    strb.Append($"{i} ");
                }
            }
            else
            {
                for (char i = (char)(secondChar + 1); i < firstChar; i++)
                {
                    strb.Append($"{i} ");
                }
            }
            

            Console.WriteLine(strb.ToString());
        }
    }
}
