﻿using System;

namespace _2._Vowels_Count
{
    class Program
    {
        static void Main()
        {
            string word = Console.ReadLine();

            VowelCount(word);
        }

        static void VowelCount(string word)
        {
            int count = 0;

            foreach (var item in word)
            {
                if (item == 97 || item == 65 || item == 69 || item == 101 || item == 73 || item == 105 || item == 79 || item == 111 || item == 85 || item == 117 || item == 89 || item == 121)
                {
                    count++;
                }
            }

            Console.WriteLine(count);
        }
    }
}
