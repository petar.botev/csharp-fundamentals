﻿using System;

namespace _10._Top_Number
{
    class Program
    {
        static void Main()
        {
            string number = Console.ReadLine();

            for (int i = 1; i <= int.Parse(number); i++)
            {
                PrintNumbers(i);
            }
        }

        static void PrintNumbers(int number)
        {
            if (DevisibleSum(number) && ContainsOddDigit(number))
            {
                Console.WriteLine(number);
            }
        }

        static bool DevisibleSum(int num)
        {
            int digitSum = 0;

            foreach (var item in num.ToString())
            {
                digitSum += int.Parse(item.ToString());
            }

            if (digitSum % 8 == 0)
            {
                return true;
            }

            return false;
        }

        static bool ContainsOddDigit(int num)
        {
            foreach (var item in num.ToString())
            {
                if (int.Parse(item.ToString()) % 2 != 0 )
                {
                    return true;
                }
            }

            return false;
        }
    }
}
