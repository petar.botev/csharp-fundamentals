﻿using System;

namespace _8._Factorial_Division
{
    class Program
    {
        static void Main(string[] args)
        {
            int num1 = int.Parse(Console.ReadLine());
            int num2 = int.Parse(Console.ReadLine());
            
            Console.WriteLine($"{Devide(num1, num2):f2}");
        }

        private static double Devide(int num1, int num2)
        {
            return Factorial(num1) / Factorial(num2);
        }

        private static double Factorial(int number)
        {
            double result = 1;
            while (number != 1)
            {
                result *= number;
                number -= 1;
            }
            return result;
        }
    }
}
