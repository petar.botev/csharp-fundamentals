﻿using System;

namespace _6._Middle_Characters
{
    class Program
    {
        static void Main()
        {
            string word = Console.ReadLine();

            Console.WriteLine(MiddleCharacters(word));
        }

        static string MiddleCharacters(string word)
        {
            if (word.Length % 2 == 0)
            {
                return word[word.Length / 2 - 1].ToString() + word[word.Length / 2].ToString();
            }
            else
            {
                return word[word.Length / 2].ToString();
            }
        }
    }
}
