﻿using System;

namespace _4._Password_Validator
{
    class Program
    {
        static void Main()
        {
            string password = Console.ReadLine();
            bool isValidLength = false;
            bool isValidSymbols = false;
            bool isValidDigits = false;

            isValidLength = CheckPasswordLength(password);
            isValidSymbols = CheckPasswordSymbols(password);
            isValidDigits = CheckNumberOfDigits(password);

            if (isValidLength && isValidSymbols && isValidDigits)
            {
                Console.WriteLine("Password is valid");
            }
        }

        static bool CheckPasswordLength(string password)
        {
            if (!(password.Length > 10 || password.Length < 6))
            {
                return true;
            }
            
            Console.WriteLine("Password must be between 6 and 10 characters");
            return false;
        }

        static bool CheckPasswordSymbols(string password)
        {
            for (var i = 0; i < password.Length; i++)
            {
                if (!char.IsDigit(password[i]) && !char.IsLetter(password[i]))
                {
                    Console.WriteLine("Password must consist only of letters and digits");
                    break;
                }

                if (i == password.Length - 1)
                {
                    return true;
                }
            }


            return false;
        }

        static bool CheckNumberOfDigits(string password)
        {
            int count = 0;
            foreach (var item in password)
            {
                if (char.IsDigit(item))
                {
                    count++;
                }
            }

            if (count < 2)
            {
                Console.WriteLine("Password must have at least 2 digits");
            }
            else
            {
                return true;
            }
            return false;
        }
    }
}
