﻿using System;
using System.Text;

namespace _7._NxN_Matrix
{
    class Program
    {
        static void Main()
        {
            int number = int.Parse(Console.ReadLine());
            PrintMatrix(number);
        }

        static void PrintMatrix(int num)
        {
            StringBuilder strb = new StringBuilder();

            for (int i = 0; i < num; i++)
            {
                for (int j = 0; j < num; j++)
                {
                    strb.Append($"{num} ");
                }
                strb.AppendLine();
            }

            Console.WriteLine(strb.ToString());
        }
    }
}
