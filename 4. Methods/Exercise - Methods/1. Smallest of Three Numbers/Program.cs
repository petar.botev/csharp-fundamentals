﻿using System;

namespace _1._Smallest_of_Three_Numbers
{
    class Program
    {
        static void Main()
        {
            int num1 = int.Parse(Console.ReadLine());
            int num2 = int.Parse(Console.ReadLine());
            int num3 = int.Parse(Console.ReadLine());

            GetSmallest(num1, num2, num3);
        }
        static void GetSmallest(int num1, int num2, int num3)
        {
            int smallestNum = num1;

            if (smallestNum > num2)
            {
                smallestNum = num2;
            }

            if (smallestNum > num3)
            {
                smallestNum = num3;
            }

            Console.WriteLine(smallestNum);
        }
    }
}
