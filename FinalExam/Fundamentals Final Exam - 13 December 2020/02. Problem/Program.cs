﻿using System;
using System.Text.RegularExpressions;

namespace _02._Problem
{
    class Program
    {
        static void Main()
        {
            int number = int.Parse(Console.ReadLine());

            Regex regex = new Regex(@"U\$(?<name>[A-Z][a-z]{2,})U\$P@\$(?<password>[A-Za-z]{5,}\d+)P@\$");

            int successfulRegistrationCount = 0;

            for (int i = 0; i < number; i++)
            {
                string usernameAndPassword = Console.ReadLine();

                Match match = regex.Match(usernameAndPassword);

                if (match.Success)
                {
                    Console.WriteLine("Registration was successful");

                    Console.WriteLine($"Username: {match.Groups["name"].Value}, Password: {match.Groups["password"]}");

                    successfulRegistrationCount++;
                }
                else
                {
                    Console.WriteLine("Invalid username or password");
                }
            }

            Console.WriteLine($"Successful registrations: {successfulRegistrationCount}");
        }
    }
}
