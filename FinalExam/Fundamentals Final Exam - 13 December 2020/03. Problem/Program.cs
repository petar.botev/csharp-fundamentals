﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _03._Problem
{
    class Program
    {
        static void Main()
        {

            List<User> users = new List<User>();

            int messagesCapacity = int.Parse(Console.ReadLine());

            string command;

            while ((command = Console.ReadLine()) != "Statistics")
            {
                string[] commandArgs = command.Split('=', StringSplitOptions.RemoveEmptyEntries);

                User user = new User();

                switch (commandArgs[0])
                {
                    case "Add":

                        string username = commandArgs[1];
                        int sentMessages = int.Parse(commandArgs[2]);
                        int receivedMessages = int.Parse(commandArgs[3]);

                        if (sentMessages >= messagesCapacity || receivedMessages >= messagesCapacity)
                        {
                            Console.WriteLine($"{username} reached the capacity!");
                        }
                        else
                        {
                            if (!users.Any(x => x.Name == username))
                            {
                                user.Name = username;
                                user.ReceicedMessages = receivedMessages;
                                user.SentMessages = sentMessages;

                                users.Add(user);
                            }
                        }

                        break;
                    case "Message":

                        string sender = commandArgs[1];
                        string receiver = commandArgs[2];

                        if (users.Any(x => x.Name == sender) && users.Any(x => x.Name == receiver))
                        {
                            User senderUser = users.First(x => x.Name == sender);
                            User receiverUser = users.First(x => x.Name == receiver);

                            senderUser.SentMessages++;
                            receiverUser.ReceicedMessages++;
                          
                            if (senderUser.SentMessages + senderUser.ReceicedMessages == messagesCapacity)
                            {
                                users.Remove(senderUser);

                                Console.WriteLine($"{senderUser.Name} reached the capacity!");
                            }

                            if (receiverUser.ReceicedMessages + receiverUser.SentMessages == messagesCapacity)
                            {
                                users.Remove(receiverUser);

                                Console.WriteLine($"{receiverUser.Name} reached the capacity!");
                            }
                        }
                        
                        break;
                    case "Empty":

                        string userToDelete = commandArgs[1];

                        if (userToDelete == "All")
                        {
                            users.Clear();
                        }
                        else
                        {
                            if (users.Any(x => x.Name == userToDelete))
                            {
                                User deleteUser = users.Find(x => x.Name == userToDelete);

                                users.Remove(deleteUser);
                            }
                        }
                        
                        break;

                    default:
                        break;
                }
            }

            Console.WriteLine($"Users count: {users.Count}");

            users = users.OrderByDescending(x => x.ReceicedMessages).ThenBy(x => x.Name).ToList();

            foreach (var item in users)
            {
                Console.WriteLine($"{item.Name} - {item.ReceicedMessages + item.SentMessages}");
            }
        }
    }

    class User
    {
        public string Name { get; set; }
        public int SentMessages { get; set; }
        public int ReceicedMessages { get; set; }
    }
}
