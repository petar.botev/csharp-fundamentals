﻿using System;
using System.Text;

namespace _01.Problem
{
    class Program
    {
        static void Main()
        {
            string text = Console.ReadLine();
            
            string command;

            while ((command = Console.ReadLine()) != "End")
            {
                                
                string[] commandArgs = command.Split();

                switch (commandArgs[0])
                {
                    
                    case "Translate":

                        string character = commandArgs[1];
                        string replacement = commandArgs[2];

                        text = text.Replace(character, replacement);

                        Console.WriteLine(text);

                        break;
                    case "Includes":

                        string textIn = commandArgs[1];

                        if (text.Contains(textIn))
                        {
                            Console.WriteLine(true);
                        }
                        else
                        {
                            Console.WriteLine(false);
                        }

                        break;
                    case "Start":

                        string start = commandArgs[1];

                        if (text.StartsWith(start))
                        {
                            Console.WriteLine(true);
                        }
                        else
                        {
                            Console.WriteLine(false);
                        }

                        break;
                    case "Lowercase":

                        text = text.ToLower();

                        Console.WriteLine(text);

                        break;
                    case "FindIndex":

                        string lastChar = commandArgs[1];

                        int lastIndex = text.LastIndexOf(lastChar);

                        Console.WriteLine(lastIndex);

                        break;
                    case "Remove":

                        int startIndex = int.Parse(commandArgs[1]);
                        int count = int.Parse(commandArgs[2]);

                        text = text.Remove(startIndex, count);

                        Console.WriteLine(text);

                        break;
                    default:
                        break;
                }
            }
        }
    }
}
