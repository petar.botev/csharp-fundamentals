﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _06._Courses
{
    class Program
    {
        static void Main()
        {
            Dictionary<string, List<string>> dict = new Dictionary<string, List<string>>();
            StringBuilder strb = new StringBuilder();

            string command;
            while ((command = Console.ReadLine()) != "end")
            {
                string[] commandArgs = command.Split(" : ");
                string courseName = commandArgs[0];
                string studentName = commandArgs[1];

                if (!dict.ContainsKey(courseName))
                {
                    dict.Add(courseName, new List<string> { studentName });
                }
                else
                {
                    dict[courseName].Add(studentName);
                }
            }

            dict = dict.OrderByDescending(x => x.Value.Count()).ToDictionary(k => k.Key, v => v.Value);

            foreach (var item in dict)
            {
                strb.AppendLine($"{item.Key}: {item.Value.Count}");
                
                List<string> students = item.Value.OrderBy(x => x).ToList();

                foreach (var student in students)
                {
                    strb.AppendLine($"-- {student}");
                }
            }

            Console.WriteLine(strb.ToString());
        }
    }
}
