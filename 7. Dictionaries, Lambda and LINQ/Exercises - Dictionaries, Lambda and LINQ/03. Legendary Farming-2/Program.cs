﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _03._Legendary_Farming_2
{
    class Program
    {
        static void Main()
        {
            string[] keyValue = Console.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries);

            string key = "";
            int value = 0;
            string keySource = "";

            Dictionary<string, int> dict = new Dictionary<string, int>() { { "fragments", 0 }, { "shards", 0 }, { "motes", 0 } }; 

            for (int i = 0; i < keyValue.Length; i++)
            {
                if ((i+1) % 2 != 0)
                {
                    value = int.Parse(keyValue[i]);
                }
                else
                {
                    key = keyValue[i].ToLower();

                    if (dict.ContainsKey(key))
                    {
                        dict[key] += value;
                    }
                    else
                    {
                        dict.Add(key, value);
                    }
                }

                
                if (key != "" && dict[key] > 249)
                {
                    keySource = key;
                    dict[key] -= 250;
                    break;
                }
            }

            StringBuilder strbImportant = new StringBuilder();
            StringBuilder strbGarbage = new StringBuilder();

            

            var importantItems = dict.Where(x => x.Key == "fragments" || x.Key == "shards" || x.Key == "motes").OrderByDescending(x => x.Value).ThenBy(x => x.Key).ToDictionary(k => k.Key, v => v.Value);

            var garbage = dict.Where(x => x.Key != "fragments" && x.Key != "shards" && x.Key != "motes").OrderBy(x => x.Key).ToDictionary(k => k.Key, v => v.Value);


            if (keySource != "")
            {
                if (keySource == "fragments")
                {
                    Console.WriteLine($"Valanyr obtained!");

                }
                else if (keySource == "shards")
                {
                    Console.WriteLine($"Shadowmourne obtained!");
                }
                else if (keySource == "motes")
                {
                    Console.WriteLine($"Dragonwrath obtained!");
                }
            }

            foreach (var item in importantItems)
            {
                strbImportant.AppendLine($"{item.Key}: {item.Value}");
            }

            Console.Write(strbImportant.ToString());

            foreach (var item in garbage)
            {
                strbGarbage.AppendLine($"{item.Key}: {item.Value}");
            }

            Console.Write(strbGarbage.ToString().Trim());
        }
    }
}
