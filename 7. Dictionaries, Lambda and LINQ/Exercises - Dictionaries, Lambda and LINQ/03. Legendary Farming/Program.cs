﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _03._Legendary_Farming
{
    class Program
    {
        static void Main()
        {
            Dictionary<string, int> keyMaterials = new Dictionary<string, int>();
            Dictionary<string, int> junkMaterials = new Dictionary<string, int>();

            StringBuilder strb = new StringBuilder();

            string command;
            bool theGameHasAWinner = false;

            while (!theGameHasAWinner)
            {
                command = Console.ReadLine();

                string[] splittedCommand = command.ToLower().Split(' ', StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < splittedCommand.Length - 1; i += 2)
                {
                    int value = int.Parse(splittedCommand[i]);
                    string key = splittedCommand[i + 1].ToLower();

                    if (key == "shards" || key == "fragments" || key == "motes")
                    {
                        if (keyMaterials.ContainsKey(key))
                        {
                            keyMaterials[key] += value;
                        }
                        else
                        {
                            keyMaterials.Add(key, value);
                        }

                        if (keyMaterials[key] > 249)
                        {
                            if (key == "shards")
                            {
                                Console.WriteLine("Shadowmourne obtained!");
                            }
                            else if (key == "fragments")
                            {
                                Console.WriteLine("Valanyr obtained!");
                            }
                            else
                            {
                                Console.WriteLine("Dragonwrath obtained!");
                            }
                            keyMaterials[key] -= 250;

                            theGameHasAWinner = true;
                            break;
                        }
                    }
                    else
                    {
                        if (junkMaterials.ContainsKey(key))
                        {
                            junkMaterials[key] += value;
                        }
                        else
                        {
                            junkMaterials.Add(key, value);
                        }
                    }
                }
            }

            if (!keyMaterials.ContainsKey("fragments"))
            {
                keyMaterials.Add("fragments", 0);
            }
            if (!keyMaterials.ContainsKey("shards"))
            {
                keyMaterials.Add("shards", 0);
            }
            if (!keyMaterials.ContainsKey("motes"))
            {
                keyMaterials.Add("motes", 0);
            }

            keyMaterials = keyMaterials.OrderByDescending(km => km.Value).ThenBy(km => km.Key).ToDictionary(k => k.Key, v => v.Value);
            junkMaterials = junkMaterials.OrderBy(jm => jm.Key).ToDictionary(k => k.Key, v => v.Value);
            
            foreach (var item in keyMaterials)
            {
                strb.AppendLine($"{item.Key}: {item.Value}");
            }
            strb.ToString().Trim();
            foreach (var item in junkMaterials)
            {
                strb.AppendLine($"{item.Key}: {item.Value}");
            }

            Console.WriteLine(strb.ToString());
        }
    }
}
