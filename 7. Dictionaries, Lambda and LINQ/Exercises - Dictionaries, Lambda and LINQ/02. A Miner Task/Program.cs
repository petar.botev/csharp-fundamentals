﻿using System;
using System.Collections.Generic;

namespace _02._A_Miner_Task
{
    class Program
    {
        static void Main()
        {

            Dictionary<string, int> dict = new Dictionary<string, int>();

            int commandCount = 0;
            string resource = "";
            int value = 0;

            string command;
            while ((command = Console.ReadLine()) != "stop")
            {
                commandCount++;
                if (commandCount % 2 != 0)
                {
                    
                    resource = command;
                }
                else
                {
                    value = int.Parse(command);

                    if (dict.ContainsKey(resource))
                    {
                        dict[resource] += value;
                    }
                    else
                    {
                        dict.Add(resource, value);
                    }

                }

            }

            foreach (var item in dict)
            {
                Console.WriteLine($"{item.Key} -> {item.Value}");
            }
        }
    }
}
