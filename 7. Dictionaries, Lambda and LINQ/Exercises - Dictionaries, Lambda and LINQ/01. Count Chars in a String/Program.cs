﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _01._Count_Chars_in_a_String
{
    class Program
    {
        static void Main()
        {
            Dictionary<char, int> dict = new Dictionary<char, int>();

            char[] charArr = Console.ReadLine().Where(x => x != ' ').ToArray();

            foreach (var item in charArr)
            {
                if (dict.ContainsKey(item))
                {
                    dict[item]++;
                }
                else
                {
                    dict.Add(item, 1);
                }
            }

            foreach (var item in dict)
            {
                Console.WriteLine($"{item.Key} -> {item.Value}");
            }
        }
    }
}
