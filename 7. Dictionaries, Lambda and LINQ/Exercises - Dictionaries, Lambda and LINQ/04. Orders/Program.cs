﻿using System;
using System.Collections.Generic;

namespace _04._Orders
{
    class Program
    {
        static void Main()
        {
            Dictionary<string, decimal> dictPrice = new Dictionary<string, decimal>();
            Dictionary<string, decimal> dictNumber = new Dictionary<string, decimal>();

            string products;

            while ((products = Console.ReadLine()) != "buy")
            {
                string[] productArgs = products.Split();

                string productName = productArgs[0];
                decimal priceForOne = decimal.Parse(productArgs[1]);
                int productNumber = int.Parse(productArgs[2]);

                

                if (dictPrice.ContainsKey(productName) && dictNumber.ContainsKey(productName))
                {
                    dictNumber[productName] += productNumber;
                    var price = dictNumber[productName] * priceForOne;
                    dictPrice[productName] = price;
                }
                else
                {
                    dictNumber.Add(productName, productNumber);

                    var price = dictNumber[productName] * priceForOne;
                    dictPrice.Add(productName, price);
                }
            }

            foreach (var item in dictPrice)
            {
                Console.WriteLine($"{item.Key} -> {item.Value}");
            }
        }
    }
}
