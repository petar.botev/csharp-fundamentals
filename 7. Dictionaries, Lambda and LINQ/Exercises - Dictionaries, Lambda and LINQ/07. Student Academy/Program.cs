﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _07._Student_Academy
{
    class Program
    {
        static void Main()
        {
            Dictionary<string, List<double>> dict = new Dictionary<string, List<double>>();

            int number = int.Parse(Console.ReadLine());

            for (int i = 0; i < number; i++)
            {
                string student = Console.ReadLine();
                double grade = double.Parse(Console.ReadLine());

                if (dict.ContainsKey(student))
                {
                    dict[student].Add(grade);
                }
                else
                {
                    dict.Add(student, new List<double> { grade });
                }
            }

            dict = dict.Where(x => x.Value.Average() >= 4.50).OrderByDescending(x => x.Value.Average()).ToDictionary(k => k.Key, v => v.Value);

            foreach (var item in dict)
            {
                Console.WriteLine($"{item.Key} -> {item.Value.Average():f2}");
            }
        }
    }
}
