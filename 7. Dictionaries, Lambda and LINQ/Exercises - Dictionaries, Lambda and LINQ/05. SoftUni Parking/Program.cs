﻿using System;
using System.Collections.Generic;

namespace _05._SoftUni_Parking
{
    class Program
    {
        static void Main()
        {
            int commandsNumber = int.Parse(Console.ReadLine());
            Dictionary<string, string> usersDict = new Dictionary<string, string>();

            for (int i = 0; i < commandsNumber; i++)
            {
                string[] commandArgs = Console.ReadLine().Split();

                string command = commandArgs[0];
                string name = commandArgs[1];

                if (command == "register")
                {
                    string plateNumber = commandArgs[2];

                    if (usersDict.ContainsKey(name))
                    {
                        Console.WriteLine($"ERROR: already registered with plate number {plateNumber}");
                    }
                    else
                    {
                        usersDict.Add(name, plateNumber);
                        Console.WriteLine($"{name} registered {plateNumber} successfully");
                    }
                }
                else
                {
                    if (!usersDict.ContainsKey(name))
                    {
                        Console.WriteLine($"ERROR: user {name} not found");
                    }
                    else
                    {
                        usersDict.Remove(name);
                        Console.WriteLine($"{name} unregistered successfully");
                    }
                }
            }

            foreach (var item in usersDict)
            {
                Console.WriteLine($"{item.Key} => {item.Value}");
            }
        }
    }
}
