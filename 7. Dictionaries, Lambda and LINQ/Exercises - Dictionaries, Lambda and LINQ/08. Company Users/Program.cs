﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _08._Company_Users
{
    class Program
    {
        static void Main()
        {
            Dictionary<string, List<string>> dict = new Dictionary<string, List<string>>();

            string command;
            while ((command = Console.ReadLine()) != "End")
            {
                string[] splittedCommand = command.Split(" -> ");
                string comapany = splittedCommand[0];
                string employerId = splittedCommand[1];

                if (dict.ContainsKey(comapany))
                {
                    if (!dict[comapany].Contains(employerId))
                    {
                        dict[comapany].Add(employerId);
                    }
                }
                else
                {
                    dict.Add(comapany, new List<string> { employerId });
                }
            }

            dict = dict.OrderBy(x => x.Key).ToDictionary(k => k.Key, v => v.Value);

            foreach (var company in dict)
            {
                Console.WriteLine($"{company.Key}");

                foreach (var item in company.Value)
                {
                    Console.WriteLine($"-- {item}");
                }
            }
        }
    }
}
