﻿using System;
using System.Collections.Generic;

namespace _02._Odd_Occurrences
{
    class Program
    {
        static void Main()
        {
            string[] words = Console.ReadLine().Split();
            Dictionary<string, int> dict = new Dictionary<string, int>();
            List<string> list = new List<string>();

            foreach (var word in words)
            {
                if (dict.ContainsKey(word.ToLower()))
                {
                    dict[word.ToLower()]++;
                }
                else
                {
                    dict.Add(word.ToLower(), 1);
                }
            }

            foreach (var item in dict)
            {
                if (item.Value % 2 != 0)
                {
                    list.Add(item.Key);
                }
            }

            Console.WriteLine(string.Join(' ', list));
        }
    }
}
