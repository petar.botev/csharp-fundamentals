﻿using System;
using System.Linq;

namespace _05._Word_Filter
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine(string.Join('\n', Console.ReadLine().Split().Where(x => x.Count() % 2 == 0).ToArray())); 
        }
    }
}
