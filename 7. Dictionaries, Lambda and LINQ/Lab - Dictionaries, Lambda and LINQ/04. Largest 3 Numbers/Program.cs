﻿using System;
using System.Linq;

namespace _04._Largest_3_Numbers
{
    class Program
    {
        static void Main()
        {
            Console.WriteLine(string.Join(' ', Console.ReadLine().Split().Select(int.Parse).OrderByDescending(x => x).Take(3).ToArray())); 
        }
    }
}
