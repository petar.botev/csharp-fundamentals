﻿using System;
using System.Text;

namespace _1._Randomize_Words
{
    class Program
    {
        static void Main()
        {
            string[] input = Console.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries);
            Random rnd = new Random();

            for (int i = 0; i < input.Length; i++)
            {
                var inputWord = input[i];
                int randomIndex = rnd.Next(0, input.Length);
                input[i] = input[randomIndex];
                input[randomIndex] = inputWord;
            }

            StringBuilder strb = new StringBuilder();

            foreach (var item in input)
            {
                strb.AppendLine(item);
            }

            Console.WriteLine(strb.ToString().Trim());
        }
    }
}
