﻿using System;
using System.Collections.Generic;
using System.Text;

namespace _3._Songs
{
    class Program
    {
        static void Main()
        {
            int inputNumber = int.Parse(Console.ReadLine());
            List<Song> songList = new List<Song>();

            for (int i = 0; i < inputNumber; i++)
            {
                Song song = new Song();
                string[] songInfo = Console.ReadLine().Split('_', StringSplitOptions.RemoveEmptyEntries);
                song.TypeList = songInfo[0];
                song.Name = songInfo[1];
                song.Time = songInfo[2];

                songList.Add(song);
            }

            string typeList = Console.ReadLine();

            PrintResult(songList, typeList);
        }

        static void PrintResult(List<Song> songList, string typeList)
        {
            StringBuilder strb = new StringBuilder();

            for (int i = 0; i < songList.Count; i++)
            {
                if (songList[i].TypeList == typeList)
                {
                    strb.AppendLine(songList[i].Name);
                }
                if (typeList == "all")
                {
                    strb.AppendLine(songList[i].Name);
                }
            }

            Console.WriteLine(strb.ToString());
        }
    }


    class Song
    {
        private string typeList;
        private string name;
        private string time;

        public string TypeList { get => typeList; set => typeList = value; }
        public string Name { get => name; set => name = value; }
        public string Time { get => time; set => time = value; }
    }
}
