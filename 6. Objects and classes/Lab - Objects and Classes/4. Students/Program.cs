﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _4._Students
{
    class Program
    {
        static void Main()
        {
            string studentStr;
            List<Student> studentList = new List<Student>();

            while ((studentStr = Console.ReadLine()) != "end")
            {
                string[] studentInfo = studentStr.Split();

                Student student = new Student()
                {
                    FirstName = studentInfo[0],
                    LastName = studentInfo[1],
                    Age = int.Parse(studentInfo[2]),
                    HomeTown = studentInfo[3]
                };

                studentList.Add(student);
            }

            string town = Console.ReadLine();

            PrintResult(studentList, town);
        }

        static void PrintResult(List<Student> students, string town)
        {
            var studentsFromTown = students.Where(s => s.HomeTown == town).ToArray();

            for (int i = 0; i < studentsFromTown.Count(); i++)
            {
                Console.WriteLine($"{studentsFromTown[i].FirstName} {studentsFromTown[i].LastName} is {studentsFromTown[i].Age} years old.");
            }
        }
    }

    class Student
    {
        private string firstName;
        private string lastName;
        private int age;
        private string homeTown;

        public string FirstName { get => firstName; set => firstName = value; }
        public string LastName { get => lastName; set => lastName = value; }
        public int Age { get => age; set => age = value; }
        public string HomeTown { get => homeTown; set => homeTown = value; }
    }
}
