﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _7._Order_by_Age
{
    class Program
    {
        static void Main()
        {
            string command;
            List<Person> personList = new List<Person>();

            while ((command = Console.ReadLine()) != "End")
            {
                string[] splittedCommand = command.Split();

                string name = splittedCommand[0];
                string id = splittedCommand[1];
                int age = int.Parse(splittedCommand[2]);

                Person person = new Person(name, id, age);

                personList.Add(person);
            }

            personList = personList.OrderBy(p => p.Age).ToList();

            foreach (var person in personList)
            {
                Console.WriteLine(person.ToString());
            }
        }
    }

    class Person
    {
        private string name;
        private string id;
        private int age;

        public Person(string name, string id, int age)
        {
            this.Name = name;
            this.Id = id;
            this.Age = age;
        }

        public string Name { get => name; set => name = value; }
        public string Id { get => id; set => id = value; }
        public int Age { get => age; set => age = value; }

        public override string ToString()
        {
            return $"{Name} with ID: {Id} is {Age} years old.";
        }
    }
}
