﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _5._Teamwork_Projects
{
    class Program
    {
        static void Main()
        {
            int teamsToRegister = int.Parse(Console.ReadLine());
            List<string> teamList = new List<string>();
            List<string> adminList = new List<string>();
            List<string> joinedMembers = new List<string>();
            List<Team> allTeams = new List<Team>();
            List<Team> disbandTeams = new List<Team>();
            List<Team> validTeams = new List<Team>();

            Admin admin;
            Team team = null;
            Member member = null;

            for (int i = 0; i < teamsToRegister; i++)
            {
                string[] teamInfo = Console.ReadLine().Split('-');

                if (adminList.Contains(teamInfo[0]))
                {
                    Console.WriteLine($"{teamInfo[0]} cannot create another team!");
                }
                else
                {
                    admin = new Admin() { AdminName = teamInfo[0] };
                    adminList.Add(admin.AdminName);
                }

                if (teamList.Contains(teamInfo[1]))
                {
                    Console.WriteLine($"Team {teamInfo[1]} was already created!");
                    adminList.RemoveAt(adminList.Count - 1);
                }
                else
                {
                    team = new Team() { TeamName = teamInfo[1] };
                    team.AdminName = teamInfo[0];

                    Console.WriteLine($"Team {teamInfo[1]} has been created by {teamInfo[0]}!");

                    teamList.Add(team.TeamName);
                    allTeams.Add(team);

                }
            }

            string joinToTeam;

            while ((joinToTeam = Console.ReadLine()) != "end of assignment")
            {
                string[] joinToTeamSplitted = joinToTeam.Split("->");

                if (!teamList.Contains(joinToTeamSplitted[1]))
                {
                    Console.WriteLine($"Team {joinToTeamSplitted[1]} does not exist!");
                }
                else
                {
                    if (joinedMembers.Contains(joinToTeamSplitted[0]) || adminList.Contains(joinToTeamSplitted[0]))
                    {
                        Console.WriteLine($"Member {joinToTeamSplitted[0]} cannot join team {joinToTeamSplitted[1]}!");
                    }
                    else
                    {
                        member = new Member() { MemberName = joinToTeamSplitted[0], Team = joinToTeamSplitted[1] };

                        var selectedTeam = allTeams.FirstOrDefault(t => t.TeamName == joinToTeamSplitted[1]);

                        selectedTeam.Members.Add(member);
                        joinedMembers.Add(member.MemberName);

                    }
                }
            }


            for (int i = 0; i < allTeams.Count; i++)
            {
                var currentTeam = allTeams[i];

                if (allTeams[i].Members.Count == 0)
                {
                    disbandTeams.Add(allTeams[i]);
                    allTeams.RemoveAt(i);
                    i--;
                }
            }

            allTeams = allTeams
                .OrderByDescending(t => t.Members.Count)
                .ThenBy(t => t.TeamName)
                .ToList();

            foreach (var item in allTeams)
            {
                item.Members = item.Members.OrderBy(m => m.MemberName).ToList();
                Console.Write(item.ToString());
            }

            PrintDisbandTeams(disbandTeams);

        }
        
        static void PrintDisbandTeams(List<Team> disbandT)
        {
            disbandT = disbandT.OrderBy(t => t.TeamName).ToList();

            StringBuilder strb = new StringBuilder();

            strb.AppendLine("Teams to disband:");

            foreach (var team in disbandT)
            {
                strb.AppendLine(team.TeamName);
            }

            Console.WriteLine(strb.ToString());
        }
    }

    class Team
    {
        private string teamName;
        private string adminName;
        private List<Member> members;

        public Team()
        {
            Members = new List<Member>();
        }

        public string TeamName { get => teamName; set => teamName = value; }
        public string AdminName { get => adminName; set => adminName = value; }
        internal List<Member> Members { get => members; set => members = value; }

        public override string ToString()
        {
            StringBuilder strb = new StringBuilder();

            strb.AppendLine(teamName);
            strb.AppendLine($"- {AdminName}");

            foreach (var member in members)
            {
                strb.AppendLine($"-- {member.MemberName}");
            }

            return strb.ToString();
        }
    }

    class Admin
    {
        private string adminName;
        private List<Team> teams;

        public string AdminName { get => adminName; set => adminName = value; }
        internal List<Team> Teams { get => teams; set => teams = value; }
    }

    class Member
    {
        private string memberName;
        private string team;

        public string MemberName { get => memberName; set => memberName = value; }
        internal string Team { get => team; set => team = value; }
    }
}
