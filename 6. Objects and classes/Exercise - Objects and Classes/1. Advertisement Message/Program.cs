﻿using System;
using System.Text;

namespace _1._Advertisement_Message
{
    class Program
    {

        static void Main()
        {
            int numberMessages = int.Parse(Console.ReadLine());

            Addvertisement addvert = new Addvertisement();

            CreateMessage(addvert.Phrases, addvert.Events, addvert.Authors, addvert.Cities, numberMessages);
        }

        static void CreateMessage(string[] phrases, string[] events, string[] authors, string[] cities, int numberOfMesages)
        {
            StringBuilder strb = new StringBuilder();

            for (int i = 0; i < numberOfMesages; i++)
            {
                Random randomPhrase = new Random();
                Random randomEvent = new Random();
                Random randomAuthor = new Random();
                Random randomCity = new Random();

                int phraseIndex = randomPhrase.Next(0, phrases.Length);
                int eventIndex = randomPhrase.Next(0, events.Length);
                int authorIndex = randomPhrase.Next(0, authors.Length);
                int cityIndex = randomPhrase.Next(0, cities.Length);
                
                strb.AppendLine($"{phrases[phraseIndex]} {events[eventIndex]} {authors[authorIndex]} - {cities[cityIndex]}.");
            }

            Console.WriteLine(strb.ToString());
        }
    }

    class Addvertisement
    {
        private string[] phrases = new string[] { "Excellent product.", "Such a great product.", "I always use that product.", "Best product of its category.", "Exceptional product.", "I can’t live without this product." };

        private string[] events = new string[] { "Now I feel good.", "I have succeeded with this product.", "Makes miracles. I am happy of the results!", "I cannot believe but now I feel awesome.", "Try it yourself, I am very satisfied.", "I feel great!" };

        private string[] authors = new string[] { "Diana", "Petya", "Stella", "Elena", "Katya", "Iva", "Annie", "Eva" };

        private string[] cities = new string[] { "Burgas", "Sofia", "Plovdiv", "Varna", "Ruse" };

        public string[] Phrases { get => phrases; set => phrases = value; }
        public string[] Events { get => events; set => events = value; }
        public string[] Authors { get => authors; set => authors = value; }
        public string[] Cities { get => cities; set => cities = value; }
    }
}
