﻿using System;

namespace _2._Articles
{
    class Program
    {
        static void Main()
        {
            string[] commandsStr = Console.ReadLine().Split(", ");
            Article article = new Article(commandsStr[0], commandsStr[1], commandsStr[2]);

            int numberCommands = int.Parse(Console.ReadLine());

            for (int i = 0; i < numberCommands; i++)
            {
                string[] commandInfo = Console.ReadLine().Split(':');

                if (commandInfo[0] == "Edit")
                {
                    article.Edit(commandInfo[1].Trim());
                }
                else if (commandInfo[0] == "ChangeAuthor")
                {
                    article.ChangeAuthor(commandInfo[1].Trim());
                }
                else if (commandInfo[0] == "Rename")
                {
                    article.Rename(commandInfo[1].Trim());
                }
            }

            Console.WriteLine(article.ToString());
        }
    }

    class Article
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string Author { get; set; }


        public Article(string title, string content, string author)
        {
            Title = title;
            Content = content;
            Author = author;
        }


        internal void Edit(string content)
        {
            Content = content;
        }

        internal void ChangeAuthor(string author)
        {
            Author = author;
        }

        internal void Rename(string title)
        {
            Title = title;
        }

        public override string ToString()
        {
            return $"{Title} - {Content}: {Author}";
        }
    }
}
