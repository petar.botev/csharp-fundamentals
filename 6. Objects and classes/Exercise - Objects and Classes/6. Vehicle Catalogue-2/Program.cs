﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _6._Vehicle_Catalogue_2
{
    class Program
    {
        static void Main()
        {
            string command;
            List<Vehicle> vehicleList = new List<Vehicle>();


            while ((command = Console.ReadLine()) != "End")
            {
                string[] splittedCommand = command.Split();

                string typeOfVehicle = splittedCommand[0].ToLower();
                string model = splittedCommand[1];
                string color = splittedCommand[2].ToLower();
                int horsePOwer = int.Parse(splittedCommand[3]);

                if (vehicleList.Any(v => v.Model == model))
                {
                    continue;
                }

                Vehicle vehicleObj = new Vehicle(typeOfVehicle, model, color, horsePOwer);

                vehicleList.Add(vehicleObj);
            }

            string vehicle;

            while ((vehicle = Console.ReadLine()) != "Close the Catalogue")
            {
                var vehicleForPrinting = vehicleList.FirstOrDefault(v => v.Model == vehicle);

                if (vehicleForPrinting == null)
                {
                    continue;
                }

                Console.WriteLine(vehicleForPrinting.ToString());
            }

            List<Vehicle> carOnly = new List<Vehicle>();
            List<Vehicle> truckOnly = new List<Vehicle>();

            carOnly = vehicleList.Where(v => v.Type == "car").ToList();
            truckOnly = vehicleList.Where(v => v.Type == "truck").ToList();

            double carAvgHP = 0d;
            double truckAvgHP = 0d;

            if (carOnly.Count > 0)
            {
                carAvgHP = (double)carOnly.Sum(c => c.HorsePower) / carOnly.Count();
                Console.WriteLine($"Cars have average horsepower of: {carAvgHP:f2}.");

            }
            else
            {
                Console.WriteLine($"Cars have average horsepower of: {carAvgHP:f2}.");
            }

            if (truckOnly.Count > 0)
            {
                truckAvgHP = (double)truckOnly.Sum(t => t.HorsePower) / truckOnly.Count();
                Console.WriteLine($"Trucks have average horsepower of: {truckAvgHP:f2}.");

            }
            else
            {
                Console.WriteLine($"Trucks have average horsepower of: {truckAvgHP:f2}.");
            }


        }

        class Vehicle
        {
            private string type;
            private string model;
            private string color;
            private int horsePower;

            public Vehicle(string type, string model, string color, int horsePower)
            {
                this.Type = type;
                this.Model = model;
                this.Color = color;
                this.HorsePower = horsePower;
            }

            public string Type { get => type; set => type = value; }
            public string Model { get => model; set => model = value; }
            public string Color { get => color; set => color = value; }
            public int HorsePower { get => horsePower; set => horsePower = value; }

            public override string ToString()
            {
                StringBuilder strb = new StringBuilder();

                strb.AppendLine($"Type: {(Type == "car" ? "Car" : "Truck")}");
                strb.AppendLine($"Model: {Model}");
                strb.AppendLine($"Color: {Color}");
                strb.AppendLine($"Horsepower: {HorsePower}");

                return strb.ToString().Trim();
            }
        }
    }
}
