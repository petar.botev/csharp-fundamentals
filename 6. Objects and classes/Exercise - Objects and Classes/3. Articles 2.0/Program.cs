﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _3._Articles_2._0
{
    class Program
    {
        static void Main()
        {
            int numberOfArticles = int.Parse(Console.ReadLine());
            List<Article> articles = new List<Article>();
            Article article;


            for (int i = 0; i < numberOfArticles; i++)
            {
                string[] articleInfo = Console.ReadLine().Split(", ");

                article = new Article()
                {
                    Title = articleInfo[0],
                    Content = articleInfo[1],
                    Author = articleInfo[2]
                };

                articles.Add(article);
            }

            string forOrder = Console.ReadLine();

            if (forOrder == "title")
            {
                articles = articles.OrderBy(a => a.Title).ToList();
            }
            else if (forOrder == "content")
            {
                articles = articles.OrderBy(a => a.Content).ToList();
            }
            else if (forOrder == "author")
            {
                articles = articles.OrderBy(a => a.Author).ToList();
            }

            foreach (var item in articles)
            {
                Console.WriteLine(item.ToString());
            }
        }
    }

    class Article
    {
        public string Title { get; set; }
        public string Content { get; set; }
        public string Author { get; set; }


        public Article()
        {
            
        }

        public override string ToString()
        {
            return $"{Title} - {Content}: {Author}";
        }
    }
}
