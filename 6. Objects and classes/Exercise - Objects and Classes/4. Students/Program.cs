﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _4._Students
{
    class Program
    {
        static void Main()
        {
            int studentNumner = int.Parse(Console.ReadLine());
            List<Student> studentList = new List<Student>();

            for (int i = 0; i < studentNumner; i++)
            {
                string[] studentInfo = Console.ReadLine().Split();
                Student student = new Student(studentInfo[0], studentInfo[1], float.Parse(studentInfo[2]));

                studentList.Add(student);
            }

            studentList = studentList.OrderByDescending(s => s.Grade).ToList();

            foreach (var student in studentList)
            {
                Console.WriteLine($"{student.FirstName} {student.SecondName}: {student.Grade:f2}");
            }
        }
    }

    class Student
    {
        public string FirstName { get; set; }
        public string SecondName { get; set; }
        public float Grade { get; set; }

        public Student(string firstName, string secondName, float grade)
        {
            FirstName = firstName;
            SecondName = secondName;
            Grade = grade;
        }
    }
}
