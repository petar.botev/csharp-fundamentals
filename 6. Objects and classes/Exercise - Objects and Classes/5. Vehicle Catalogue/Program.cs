﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _6._Vehicle_Catalogue
{
    class Program
    {
        static void Main()
        {
            string command;
            List<Vehicle> vehicleList = new List<Vehicle>();
            

            while ((command = Console.ReadLine()) != "End")
            {
                string[] splittedCommand = command.Split();

                string typeOfVehicle = splittedCommand[0].ToLower();
                string model = splittedCommand[1];
                string color = splittedCommand[2].ToLower();
                int horsePOwer = int.Parse(splittedCommand[3]);

                if (vehicleList.Any(v => v.Model == model))
                {
                    continue;
                }

                if (typeOfVehicle == "car")
                {
                    Car car = new Car(typeOfVehicle, model, color, horsePOwer);
                    vehicleList.Add(car);
                }
                else if (typeOfVehicle == "truck")
                {
                    Truck truck = new Truck(typeOfVehicle, model, color, horsePOwer);
                    vehicleList.Add(truck);
                }
            }

            string vehicle;

            while ((vehicle = Console.ReadLine()) != "Close the Catalogue")
            {
                var vehicleForPrinting = vehicleList.FirstOrDefault(v => v.Model == vehicle);

                if (vehicleForPrinting == null)
                {
                    continue;
                }

                Console.WriteLine(vehicleForPrinting.ToString()); 
            }

            float carAvgHP = 0;
            float truckAvgHP = 0;

            if (vehicleList.Count > 0)
            {
                carAvgHP = (float)vehicleList.Where(v => v.Type == "car").Sum(v => v.HorsePower) / vehicleList.Where(v => v.Type == "car").Count();

                truckAvgHP = (float)vehicleList.Where(v => v.Type == "truck").Sum(v => v.HorsePower) / vehicleList.Where(v => v.Type == "truck").Count();

                if (!float.IsNaN(carAvgHP))
                {
                    Console.WriteLine($"Cars have average horsepower of: {carAvgHP:f2}.");
                }


                if (!float.IsNaN(truckAvgHP))
                {
                    Console.Write($"Trucks have average horsepower of: {truckAvgHP:f2}.");
                }
            }
        }
    }

    class Vehicle
    {
        private string type;
        private string model;
        private string color;
        private int horsePower;

        public Vehicle(string type, string model, string color, int horsePower)
        {
            this.Type = type;
            this.Model = model;
            this.Color = color;
            this.HorsePower = horsePower;
        }

        public string Type { get => type; set => type = value; }
        public string Model { get => model; set => model = value; }
        public string Color { get => color; set => color = value; }
        public int HorsePower { get => horsePower; set => horsePower = value; }


       
    }

    class Car : Vehicle
    {
        public Car(string type, string model, string color, int horsepower)
            : base(type, model, color, horsepower)
        {

        }

        public override string ToString()
        {
            StringBuilder strb = new StringBuilder();

            strb.AppendLine($"Type: Car");
            strb.AppendLine($"Model: {Model}");
            strb.AppendLine($"Color: {Color}");
            strb.AppendLine($"Horsepower: {HorsePower}");

            return strb.ToString().Trim();
        }
    }

    class Truck : Vehicle
    {
        public Truck(string type, string model, string color, int horsepower)
            :base(type, model, color, horsepower)
        {

        }

        public override string ToString()
        {
            StringBuilder strb = new StringBuilder();

            strb.AppendLine($"Type: Truck");
            strb.AppendLine($"Model: {Model}");
            strb.AppendLine($"Color: {Color}");
            strb.AppendLine($"Horsepower: {HorsePower}");

            return strb.ToString().Trim();
        }
    }
}
