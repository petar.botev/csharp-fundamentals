﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _7._Max_Sequence_of_Equal_Elements
{
    class Program
    {
        static void Main()
        {
            int[] array = Console.ReadLine().Split().Select(int.Parse).ToArray();
            int counter = 1;
            int maxLength = 0;
            int step = 1;
            List<int> list = new List<int>();
            List<int> finalList = new List<int>();

            for (int j = 0; j < array.Length - 1; j += step)
            {
                int newIndex = j;
                step = 1;
                counter = 1;
                list.Clear();

                while (array[newIndex] == array[newIndex + 1])
                {
                    counter++;
                    step++;
                    list.Add(array[newIndex]);

                    newIndex++;
                    if (newIndex == array.Length - 1)
                    {
                        break;
                    }
                }

                if (counter > 1)
                {
                    list.Add(array[newIndex - 1]);
                }

                if (maxLength < counter)
                {
                    maxLength = counter;
                    finalList = list.Select(x => x).ToList();
                }

            }

            Console.WriteLine(string.Join(' ', finalList));
        }
    }
}
