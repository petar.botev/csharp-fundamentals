﻿using System;
using System.Linq;

namespace _3._Zig_Zag_Arrays
{
    class Program
    {
        static void Main()
        {
            int integersNumber = int.Parse(Console.ReadLine());
            int[] array1 = new int[integersNumber];
            int[] array2 = new int[integersNumber];

            for (int i = 1; i <= integersNumber; i++)
            {
                int[] integers = Console.ReadLine().Split().Select(x => int.Parse(x)).ToArray();

                if (i % 2 != 0)
                {
                    array1[i - 1] = integers[0];
                    array2[i - 1] = integers[1];
                }
                else
                {
                    array1[i - 1] = integers[1];
                    array2[i - 1] = integers[0];
                }
            }


            Console.WriteLine(string.Join(' ',array1));
            Console.WriteLine(string.Join(' ',array2));
        }
    }
}
