﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;

namespace _9._Kamino_Factory
{
    class Program
    {
        static void Main()
        {
            int sequenceLength = int.Parse(Console.ReadLine());
            int countOfOnes = 0;
            int firstIndex = 0;
            int maxLengthOfOnes = 0;
            int minFirstIndex = 0;
            int elementsSum = 0;
            List<string> list = new List<string>();
            //List<string> tempList = new List<string>();

            DnaSequence dnaSeq = new DnaSequence();

            List<DnaSequence> dnaSeqList = new List<DnaSequence>();

            Dictionary<int, DnaSequence> dnaSeqDict = new Dictionary<int, DnaSequence>();
            int dictIndex = 0;

            while (true)
            {
                string sequence = Console.ReadLine();
                countOfOnes = 0;
                maxLengthOfOnes = 0;
                
                if (sequence == "Clone them!")
                {
                    break;
                }

                string[] splittedSequence = sequence.Split('!', StringSplitOptions.RemoveEmptyEntries);
                list.AddRange(splittedSequence);

                for (int i = 0; i < splittedSequence.Length; i++)
                {
                    if (splittedSequence[i] == "1")
                    {
                        countOfOnes++;

                        if (maxLengthOfOnes < countOfOnes)
                        {
                            if (countOfOnes == 1)
                            {
                                firstIndex = i;
                            }
                            maxLengthOfOnes = countOfOnes;
                        }
                    }
                    else
                    {
                        countOfOnes = 0;
                        list.Clear();
                    }
                }

                dnaSeq = new DnaSequence();
                dnaSeq.FirstIndexOfMaxCountOfOnes = firstIndex;
                dnaSeq.MaxCountOfOnes = maxLengthOfOnes;
                dnaSeq.FullDnaSequence = splittedSequence;
                dnaSeqList.Add(dnaSeq);
                dnaSeqDict[dictIndex] = dnaSeq;
                dictIndex++;
            }

            List<DnaSequence> maxOnes = new List<DnaSequence>();
            Dictionary<int, DnaSequence> maxOnesDict = new Dictionary<int, DnaSequence>();

            List<DnaSequence> minFirstSeq = new List<DnaSequence>();
            Dictionary<int, DnaSequence> minFirstSeqDict = new Dictionary<int, DnaSequence>();

            int finalSum = 0;

            if (dnaSeqDict.Count > 1)
            {
                for (int i = 0; i < dnaSeqDict.Count - 1; i++)
                {
                    if (dnaSeqDict[i].MaxCountOfOnes >= dnaSeqDict[i + 1].MaxCountOfOnes)
                    {
                        maxOnesDict[i] = dnaSeqDict[i];
                    }
                }

                if (maxOnes.Count > 1)
                {
                    for (int i = 0; i < maxOnes.Count - 1; i++)
                    {
                        if (maxOnes[i].FirstIndexOfMaxCountOfOnes <= maxOnes[i + 1].FirstIndexOfMaxCountOfOnes)
                        {
                            minFirstSeq.Add(maxOnes[i]);
                        }
                    }
                }
                else
                {
                    finalSum = CalculateFinalSum(maxOnesDict[0].FullDnaSequence);
                    //dnaSeq = maxOnes[0];
                }

                if (minFirstSeq.Count > 1)
                {
                    for (int i = 0; i < minFirstSeq.Count; i++)
                    {
                        for (int j = 0; j < minFirstSeq[i].FullDnaSequence.Length; j++)
                        {
                            if (minFirstSeq[i].FullDnaSequence[j] == "1")
                            {
                                finalSum++;
                            }
                        }
                    }
                }
                else
                {
                    if (finalSum == 0)
                    {
                        finalSum = CalculateFinalSum(minFirstSeq[0].FullDnaSequence);
                        dnaSeq = minFirstSeq[0];
                    }
                }
            }
            else
            {
                if (finalSum == 0)
                {
                    finalSum = CalculateFinalSum(dnaSeqList[0].FullDnaSequence);
                    dnaSeq = dnaSeqList[0];
                }
            }
            int bestDnaSample = maxOnesDict.Keys.FirstOrDefault();

            Console.WriteLine($"Best DNA sample {maxOnesDict.Keys.FirstOrDefault() + 1} with sum: {finalSum}.\n{string.Join(' ', dnaSeq.FullDnaSequence)}");
        }

        static int CalculateFinalSum(string[] strArr)
        {
            int counter = 0;

            for (int i = 0; i < strArr.Length; i++)
            {
                if (strArr[i] == "1")
                {
                    counter++;
                }
            }
            return counter;
        }
    }

    class DnaSequence
    {
        public int FirstIndexOfMaxCountOfOnes { get; set; }
        public int MaxCountOfOnes { get; set; }
        public string[] FullDnaSequence { get; set; }
    }
}
