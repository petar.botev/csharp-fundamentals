﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _6._Equal_Sums
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = Console.ReadLine().Split().Select(int.Parse).ToArray();
            int leftSum = 0;
            int rightSum = 0;
            List<int> list = new List<int>();

            for (int i = 0; i < array.Length; i++)
            {
                leftSum = 0;
                rightSum = 0;

                for (int j = i+1; j < array.Length; j++)
                {
                    rightSum += array[j];
                }


                if (i > 0)
                {
                    for (int k = i - 1; k >= 0; k--)
                    {
                        leftSum += array[k];
                    }
                }

                if (leftSum == rightSum)
                {
                    list.Add(i);
                }
            }

            if (list.Count == 0)
            {
                Console.WriteLine("no");
            }
            else
            {
                Console.WriteLine(string.Join(' ', list));
            }
        }
    }
}
