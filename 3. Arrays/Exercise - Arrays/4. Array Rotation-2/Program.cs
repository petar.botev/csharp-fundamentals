﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _4._Array_Rotation_2
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<int> queue = new Queue<int>();
            int[] array = Console.ReadLine().Split().Select(x => int.Parse(x)).ToArray();
            int rotationNumber = int.Parse(Console.ReadLine());

            for (int i = 0; i < array.Length; i++)
            {
                queue.Enqueue(array[i]);
            }

            for (int j = 0; j < rotationNumber; j++)
            {
                int firstElement = queue.Dequeue();
                queue.Enqueue(firstElement);

            }

            Console.WriteLine(string.Join(' ', queue));
        }
    }
}
