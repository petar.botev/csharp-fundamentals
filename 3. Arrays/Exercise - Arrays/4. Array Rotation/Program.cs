﻿using System;
using System.Linq;

namespace _4._Array_Rotation
{
    class Program
    {
        static void Main()
        {
            int[] array = Console.ReadLine().Split().Select(x => int.Parse(x)).ToArray();

            int rotationNumber = int.Parse(Console.ReadLine());

            for (int i = 0; i < rotationNumber; i++)
            {
                int temp = array[0];
                for (int j = 1; j < array.Length; j++)
                {
                    array[j-1] = array[j];
                }
                array[array.Length - 1] = temp;
            }


            Console.WriteLine(string.Join(' ', array));
        }
    }
}
