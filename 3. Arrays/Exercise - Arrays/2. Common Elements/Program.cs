﻿using System;
using System.Collections.Generic;

namespace _2._Common_Elements
{
    class Program
    {
        static void Main()
        {
            string[] firstStringArray = Console.ReadLine().Split();
            string[] secondStringArray = Console.ReadLine().Split();
            
            List<string> commonElements = new List<string>();

            for (int i = 0; i < secondStringArray.Length; i++)
            {
                for (int j = 0; j < firstStringArray.Length; j++)
                {
                    if (secondStringArray[i] == firstStringArray[j] && firstStringArray[j] != "true")
                    {
                        commonElements.Add(secondStringArray[i]);
                        firstStringArray[j] = "true";
                    }
                }
                
            }

            //commonElements.Reverse();

            Console.WriteLine(string.Join(' ', commonElements));
        }
    }
}
