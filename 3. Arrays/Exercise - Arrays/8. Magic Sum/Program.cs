﻿using System;
using System.Linq;
using System.Text;

namespace _8._Magic_Sum
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] array = Console.ReadLine().Split().Select(int.Parse).ToArray();
            int targetSum = int.Parse(Console.ReadLine());
            StringBuilder strb = new StringBuilder();

            for (int i = 0; i < array.Length - 1; i++)
            {
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (array[i] + array[j] == targetSum)
                    {
                        strb.AppendLine($"{array[i]} {array[j]}");
                    }
                }
                
            }

            Console.WriteLine(strb.ToString());
        }
    }
}
