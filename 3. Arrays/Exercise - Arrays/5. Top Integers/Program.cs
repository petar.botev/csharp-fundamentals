﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _5._Top_Integers
{
    class Program
    {
        static void Main()
        {
            int[] array = Console.ReadLine().Split().Select(int.Parse).ToArray();
            List<int> list = new List<int>();
            bool isTop = true;

            for (int i = 0; i < array.Length; i++)
            {
                isTop = true;
                if (i == array.Length - 1)
                {
                    list.Add(array[i]);
                    break;
                }
                for (int j = i + 1; j < array.Length; j++)
                {
                    if (array[i] <= array[j])
                    {
                        isTop = false;
                        break;
                    }
                }

                if (isTop == true)
                {
                    list.Add(array[i]);
                }
            }

            Console.WriteLine(string.Join(' ', list));
        }
    }
}
