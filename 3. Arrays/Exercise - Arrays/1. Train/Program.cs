﻿using System;
using System.Linq;

namespace _1._Train
{
    class Program
    {
        static void Main()
        {

            int wagons = int.Parse(Console.ReadLine());

            int[] peopleInWagons = new int[wagons];

            for (int i = 0; i < wagons; i++)
            {
                int people = int.Parse(Console.ReadLine());

                peopleInWagons[i] = people;
            }

            Console.WriteLine(string.Join(' ', peopleInWagons));
            Console.WriteLine(peopleInWagons.Sum());
        }
    }
}
