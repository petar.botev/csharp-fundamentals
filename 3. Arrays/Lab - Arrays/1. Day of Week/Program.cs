﻿using System;

namespace _1._Day_of_Week
{
    class Program
    {
        static void Main()
        {
            int dayNumber = int.Parse(Console.ReadLine());

            string[] daysOfWeek = new string[] { "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday" };

            if (dayNumber > 7 || dayNumber < 1)
            {
                Console.WriteLine("Invalid day!");
            }
            else
            {
                Console.WriteLine(daysOfWeek[dayNumber - 1]);
            }
        }
    }
}
