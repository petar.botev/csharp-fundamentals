﻿using System;
using System.Linq;

namespace _2._Print_Numbers_in_Reverse_Order
{
    class Program
    {
        static void Main(string[] args)
        {
            int numbers = int.Parse(Console.ReadLine());

            int[] arrNums = new int[numbers];

            for (int i = 0; i < numbers; i++)
            {
                int number = int.Parse(Console.ReadLine());
                arrNums[i] = number;
            }

            arrNums = arrNums.Reverse().ToArray();

            Console.WriteLine(string.Join(' ', arrNums));
        }
    }
}
