﻿using System;
using System.Linq;
using System.Text;

namespace _3._Rounding_Numbers
{
    class Program
    {
        static void Main()
        {
            string array = Console.ReadLine();

            double[] parsedArray = array.Split(' ').Select(double.Parse).ToArray();

            StringBuilder strb = new StringBuilder();

            foreach (var number in parsedArray)
            {
                double rounded;

                rounded = Math.Round(number, MidpointRounding.AwayFromZero);

                strb.AppendLine($"{number} => {rounded}");
            }

            Console.WriteLine(strb.ToString().Trim());
        }
    }
}
