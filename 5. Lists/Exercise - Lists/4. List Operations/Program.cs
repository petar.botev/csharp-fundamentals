﻿using System;
using System.Collections.Generic;
using System.Numerics;

namespace _4._List_Operations
{
    class Program
    {
        static void Main()
        {
            string integers = Console.ReadLine();
            string[] splittedIntegers = integers.Split();
            List<string> intList = new List<string>();

            for (int i = 0; i < splittedIntegers.Length; i++)
            {
                intList.Add(splittedIntegers[i]);
            }

            string command;

            while ((command = Console.ReadLine()) != "End")
            {
                string[] splittedCommand = command.Split();

                if (splittedCommand[0] == "Add")
                {
                    intList.Add(splittedCommand[1]);
                }
                else if (splittedCommand[0] == "Insert")
                {
                    if (double.Parse(splittedCommand[2]) > intList.Count - 1 || double.Parse(splittedCommand[2]) < 0)
                    {
                        Console.WriteLine("Invalid index");
                    }
                    else
                    {
                        intList.Insert(int.Parse(splittedCommand[2]), splittedCommand[1]);
                    }
                }
                else if (splittedCommand[0] == "Remove")
                {
                    if (double.Parse(splittedCommand[1]) > intList.Count - 1 || double.Parse(splittedCommand[1]) < 0)
                    {
                        Console.WriteLine("Invalid index");
                    }
                    else
                    {
                        intList.RemoveAt(int.Parse(splittedCommand[1]));
                    }
                }
                else 
                {
                    if (splittedCommand[1] == "left")
                    {
                        for (int i = 0; i < int.Parse(splittedCommand[2]); i++)
                        {
                            var firstValue = intList[0];
                            intList.RemoveAt(0);
                            intList.Add(firstValue);
                        }
                    }
                    else
                    {
                        for (int i = 0; i < int.Parse(splittedCommand[2]); i++)
                        {
                            var lastValue = intList[intList.Count - 1];
                            intList.RemoveAt(intList.Count - 1);
                            intList.Insert(0, lastValue);
                        }
                    }
                }
            }


            Console.WriteLine(string.Join(' ', intList));
        }
    }
}
