﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _6._Cards_Game
{
    class Program
    {
        static void Main()
        {
            List<int> firstPlayerCards = Console.ReadLine().Split().Select(x => int.Parse(x)).ToList();
            List<int> secondPlayerCards = Console.ReadLine().Split().Select(x => int.Parse(x)).ToList();

            while (firstPlayerCards.Count > 0 && secondPlayerCards.Count > 0)
            {
                if (firstPlayerCards[0] > secondPlayerCards[0])
                {
                    var loosingCard = secondPlayerCards[0];
                    secondPlayerCards.RemoveAt(0);
                    var winningCard = firstPlayerCards[0];
                    firstPlayerCards.RemoveAt(0);
                    firstPlayerCards.Add(winningCard);
                    firstPlayerCards.Add(loosingCard);
                }
                else if (firstPlayerCards[0] < secondPlayerCards[0])
                {
                    var loosingCard = firstPlayerCards[0];
                    firstPlayerCards.RemoveAt(0);
                    var winningCard = secondPlayerCards[0];
                    secondPlayerCards.RemoveAt(0);
                    secondPlayerCards.Add(winningCard);
                    secondPlayerCards.Add(loosingCard);
                }
                else
                {
                    firstPlayerCards.RemoveAt(0);
                    secondPlayerCards.RemoveAt(0);
                }
            }

            if (firstPlayerCards.Count > 0)
            {
                Console.WriteLine($"First player wins! Sum: {firstPlayerCards.Sum()}");
            }
            else
            {
                Console.WriteLine($"Second player wins! Sum: {secondPlayerCards.Sum()}");
            }
        }
    }
}
