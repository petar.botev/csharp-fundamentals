﻿using System;
using System.Collections.Generic;

namespace _2._Change_List
{
    class Program
    {
        static void Main()
        {
            string integers = Console.ReadLine();
            string[] splittedIntegers = integers.Split();
            List<string> intList = new List<string>();

            for (int i = 0; i < splittedIntegers.Length; i++)
            {
                intList.Add(splittedIntegers[i]);
            }

            string command;

            while ((command = Console.ReadLine()) != "end")
            {
                string[] splittedCommand = command.Split();

                if (splittedCommand[0] == "Delete")
                {
                    for (int i = 0; i < intList.Count; i++)
                    {
                        if (intList[i] == splittedCommand[1])
                        {
                            intList.Remove(intList[i]);

                            i -= 1;
                        }
                    }
                }
                else
                {
                    intList.Insert(int.Parse(splittedCommand[2]), splittedCommand[1]);
                }
            }
            


            Console.WriteLine(string.Join(' ', intList));
        }
    }
}
