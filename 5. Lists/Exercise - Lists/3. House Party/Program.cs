﻿using System;
using System.Collections.Generic;

namespace _3._House_Party
{
    class Program
    {
        static void Main()
        {
            int commandNumber = int.Parse(Console.ReadLine());
            List<string> personList = new List<string>();

            for (int i = 0; i < commandNumber; i++)
            {
                string command = Console.ReadLine();
                string[] splittedCommand = command.Split();

                if (splittedCommand.Length == 3)
                {
                    if (personList.Contains(splittedCommand[0]))
                    {
                        Console.WriteLine($"{splittedCommand[0]} is already in the list!");
                    }
                    else
                    {
                        personList.Add(splittedCommand[0]);
                    }
                    
                }
                else
                {
                    if (!personList.Contains(splittedCommand[0]))
                    {
                        Console.WriteLine($"{splittedCommand[0]} is not in the list!");
                    }
                    else
                    {
                        personList.Remove(splittedCommand[0]);
                    }
                }
            }


            Console.WriteLine(string.Join('\n', personList));
        }
    }
}
