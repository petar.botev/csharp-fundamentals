﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _1._Train
{
    class Program
    {
        static void Main()
        {
            List<int> wagonsList = new List<int>();
            string wagons = Console.ReadLine();
            string[] splittedWagons = wagons.Split();

            for (int i = 0; i < splittedWagons.Count(); i++)
            {
                wagonsList.Add(int.Parse(splittedWagons[i]));
            }
            
            int wagonMaxCapacity = int.Parse(Console.ReadLine());
            string command;

            while ((command = Console.ReadLine()) != "end")
            {
                string[] splittedCommand = command.Split();

                if (splittedCommand[0].ToString() == "Add")
                {
                    wagonsList.Add(int.Parse(splittedCommand[1].ToString()));
                }
                else
                {
                    for (int i = 0; i < wagonsList.Count(); i++)
                    {
                        if ((wagonsList[i] + int.Parse(splittedCommand[0].ToString())) <= wagonMaxCapacity)
                        {
                            wagonsList[i] += int.Parse(splittedCommand[0].ToString());

                            //if (wagonsList[i] < wagonMaxCapacity)
                            //{
                            //    i -= 1;
                            //}
                            break;
                        }
                    }
                }
            }

            Console.WriteLine(string.Join(' ', wagonsList));
        }
    }
}
