﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace _5._Bomb_Numbers
{
    class Program
    {
        static void Main()
        {
            List<string> numbers = Console.ReadLine().Split(' ').ToList();
            string[] bombParameters = Console.ReadLine().Split();

            for (int i = 0; i < numbers.Count; i++)
            {
                if (numbers[i] == bombParameters[0])
                {
                    int counter1 = 0;
                    while (counter1 < int.Parse(bombParameters[1]) && numbers.Count > i)
                    {
                        if (numbers.Count > i + 1)
                        {
                            numbers.RemoveAt(i + 1);
                            counter1++;
                        }
                        else
                        {
                            break;
                        }
                    }

                    numbers.RemoveAt(i);
                    int counter2 = 0;
                    int removeIndex = i - 1;

                    while (counter2 < int.Parse(bombParameters[1]) && removeIndex > -1)
                    {
                        numbers.RemoveAt(removeIndex);
                        counter2++;
                        removeIndex--;
                    }
                    i = -1;
                }
            }

            Console.WriteLine(numbers.Select(n => double.Parse(n)).Sum());
        }
    }
}
