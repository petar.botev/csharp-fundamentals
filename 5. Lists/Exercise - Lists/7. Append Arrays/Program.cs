﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _7._Append_Arrays
{
    class Program
    {
        static void Main()
        {
            List<string> arrays = Console.ReadLine().Split('|', StringSplitOptions.RemoveEmptyEntries).Select(x => x.Trim()).ToList();

            arrays.Reverse();

            var finalArray = arrays.Select(x => x.Split(' ', StringSplitOptions.RemoveEmptyEntries)).ToList();

            for (int i = 0; i < finalArray.Count; i++)
            {
                if (finalArray[i].Count() == 0)
                {
                    finalArray.RemoveAt(i);
                    i--;
                }
            }

            Console.WriteLine(string.Join(' ', finalArray.Select(x => string.Join(' ', x))));
        }
    }
}
