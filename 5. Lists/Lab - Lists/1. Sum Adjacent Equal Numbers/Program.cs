﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _1._Sum_Adjacent_Equal_Numbers
{
    class Program
    {
        static void Main()
        {
            List<float> input = Console.ReadLine().Split(' ').Select(x => float.Parse(x)).ToList();

            for (int i = 0; i < input.Count - 1; i++)
            {
                if (input[i] == input[i + 1])
                {
                    float sum = input[i] + input[i + 1];
                    input.Remove(input[i]);
                    input.Remove(input[i]);
                    input.Insert(i, sum);
                    i = -1;
                }
            }

            Console.WriteLine(string.Join(" ", input));
        }
    }
}
