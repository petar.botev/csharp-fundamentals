﻿using System;

namespace _01._Spring_Vacation_Trip
{
    class Program
    {
        static void Main()
        {
            int daysOfTrip = int.Parse(Console.ReadLine());
            decimal budget = decimal.Parse(Console.ReadLine());
            int people = int.Parse(Console.ReadLine());
            decimal fuelPricePerKm = decimal.Parse(Console.ReadLine());
            decimal foodExpencesPerPersonPerDay = decimal.Parse(Console.ReadLine());
            decimal roomPricePerNught = decimal.Parse(Console.ReadLine());

            double travelledDistance = 0;

            decimal expences = 0;

            decimal hotelPrice = (roomPricePerNught * people) * daysOfTrip;

            
            expences += (foodExpencesPerPersonPerDay * people) * daysOfTrip;
            expences += hotelPrice;
            decimal currExp = expences;

            for (int i = 0; i < daysOfTrip; i++)
            {
                double distance = double.Parse(Console.ReadLine());

                travelledDistance += distance;
            }

            expences += ((decimal)travelledDistance * fuelPricePerKm);

            

            for (int i = 1; i <= daysOfTrip; i++)
            {
                if (i % 3 == 0 || i % 5 == 0)
                {
                    expences += currExp * 0.4m;

                }

                if (i % 7 == 0)
                {
                    expences -= currExp / people;
                }
            }
            

            if (people > 10)
            {
                expences -= hotelPrice * 0.25m;
            }

            decimal leftMoney = budget - expences;

            Console.WriteLine($"You have reached the destination. You have {leftMoney}$ budget left.");
        }
    }
}
