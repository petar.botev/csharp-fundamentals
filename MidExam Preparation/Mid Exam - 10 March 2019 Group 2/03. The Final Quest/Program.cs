﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _03._The_Final_Quest
{
    class Program
    {
        static List<string> words = null;

        static void Main()
        {
            words = Console.ReadLine().Split(' ', StringSplitOptions.RemoveEmptyEntries).ToList();

            string command;

            while ((command = Console.ReadLine()) != "Stop")
            {
                string[] splittedCommand = command.Split(' ', StringSplitOptions.RemoveEmptyEntries);

                string theCommand = splittedCommand[0];
                int deleteIndex = 0;
                string firstWord = "";
                int putIndex = 0;
                string secondWord = "";

                if (splittedCommand.Length == 2)
                {
                    deleteIndex = int.Parse(splittedCommand[1]);
                }
                if (splittedCommand.Length == 3)
                {
                    firstWord = splittedCommand[1];

                    if (int.TryParse(splittedCommand[2], out int res))
                    {
                        putIndex = res;
                    }
                    else
                    {
                        secondWord = splittedCommand[2];
                    }
                }

                if (theCommand == "Delete")
                {
                    Delete(deleteIndex);
                }
                else if (theCommand == "Swap")
                {
                    Swap(firstWord, secondWord);
                }
                else if (theCommand == "Put")
                {
                    Put(firstWord, putIndex);
                }
                else if (theCommand == "Sort")
                {
                    Sort(words);
                }
                else if (theCommand == "Replace")
                {
                    Replace(firstWord, secondWord);
                }
            }

            Console.WriteLine(string.Join(' ', words).Trim());
        }

        static void Delete(int index)
        {
            if (index < words.Count - 1 && index >= -1)
            {
                words.RemoveAt(index + 1);
            }
        }

        static void Swap(string firstWord, string secondWord)
        {
            bool firstWordExists = words.Any(x => x == firstWord);
            bool secondWordExists = words.Any(x => x == secondWord);
            int firstWordIndex = words.FindIndex(x => x == firstWord);
            int secondWordIndex = words.FindIndex(x => x == secondWord);

            if (firstWordExists && secondWordExists)
            {
                words.RemoveAt(firstWordIndex);
                words.Insert(firstWordIndex, secondWord);

                words.RemoveAt(secondWordIndex);
                words.Insert(secondWordIndex, firstWord);
            }
        }

        static void Put(string firstWord, int index)
        {
            if (index <= words.Count + 1 && index > 0)
            {

                if (index == words.Count + 1)
                {
                    words.Add(firstWord);
                }
                else
                {
                    words.Insert(index - 1, firstWord);
                }

            }
        }

        static void Sort(List<string> wordsList)
        {
            words = wordsList.OrderByDescending(x => x).ToList();
        }

        static void Replace(string firstWord, string secondWord)
        {
            if (words.Any(x => x == secondWord))
            {
                int secondWordIndex = words.FindIndex(x => x == secondWord);

                words.RemoveAt(secondWordIndex);
                words.Insert(secondWordIndex, firstWord);
            }
        }
    }
}
