﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _02._Seize_the_Fire
{
    class Program
    {
        static void Main()
        {
            string[] fires = Console.ReadLine().Split('#');

            int water = int.Parse(Console.ReadLine());

            List<string> fireType = new List<string>();
            List<int> cellValue = new List<int>();
            List<int> putOutCells = new List<int>();
            double effort = 0;

            foreach (var item in fires)
            {
                fireType.Add(item.Split(" = ")[0]);
                cellValue.Add(int.Parse(item.Split(" = ")[1]));
            }

            for (int i = 0; i < cellValue.Count; i++)
            {
                if (CheckFireType(fireType[i], cellValue[i]))
                {
                    if (water >= cellValue[i])
                    {
                        water -= cellValue[i];
                        effort += cellValue[i] * 0.25;

                        putOutCells.Add(cellValue[i]);
                    }
                }
            }

            StringBuilder strb = new StringBuilder();

            strb.AppendLine("Cells:");

            foreach (var item in putOutCells)
            {
                strb.AppendLine($" - {item}");
            }

            Console.WriteLine(strb.ToString().Trim());

            double totalFire = putOutCells.Sum();

            Console.WriteLine($"Effort: {effort:f2}");
            Console.WriteLine($"Total Fire: {totalFire}");
            
        }

        static bool CheckFireType(string fireType, int cellValue)
        {
            if ((fireType == "High" && cellValue <= 125 && cellValue >= 81) || (fireType == "Medium" && cellValue <= 80 && cellValue >= 51) || (fireType == "Low" && cellValue <= 50 && cellValue >= 1))
            {
                return true;
            }

            return false;
        }
    }
}
