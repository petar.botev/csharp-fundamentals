﻿using System;

namespace _01._The_Hunting_Games
{
    class Program
    {
        static void Main()
        {
            int days = int.Parse(Console.ReadLine());
            int playersCount = int.Parse(Console.ReadLine());
            double groupEnergy = double.Parse(Console.ReadLine());
            double waterPerDayForPerson = double.Parse(Console.ReadLine());
            double foodPerDayForPerson = double.Parse(Console.ReadLine());

            double waterForGroup = (waterPerDayForPerson * playersCount) * days;
            double foodPerGroup = (foodPerDayForPerson * playersCount) * days;

            for (int i = 1; i <= days; i++)
            {
                double energyLost = double.Parse(Console.ReadLine());

                groupEnergy -= energyLost;

                if (groupEnergy <= 0)
                {
                    Console.WriteLine($"You will run out of energy. You will be left with {foodPerGroup:f2} food and {waterForGroup:f2} water.");
                    break;
                }

                if (i % 2 == 0)
                {
                    groupEnergy += (groupEnergy * 0.05);
                    waterForGroup -= (waterForGroup * 0.3);
                }

                if (i % 3 == 0)
                {
                    foodPerGroup -= foodPerGroup / playersCount;
                    groupEnergy += (groupEnergy * 0.1);
                }
            }

            if (groupEnergy > 0)
            {
                Console.WriteLine($"You are ready for the quest. You will be left with - {groupEnergy:f2} energy!");
            }

        }
    }
}
