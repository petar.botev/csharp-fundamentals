﻿using System;

namespace _01._Bonus_Scoring_System
{
    class Program
    {
        static void Main()
        {
            int studentsCount = int.Parse(Console.ReadLine());

            int lecturesCont = int.Parse(Console.ReadLine());
            int courseBonus = int.Parse(Console.ReadLine());

            double maxStudentBonus = 0;
            int attendancesForMaxBonus = 0;

            for (int i = 0; i < studentsCount; i++)
            {
                int attendances = int.Parse(Console.ReadLine());

                double studentBonus = TotalBonus(courseBonus, lecturesCont, attendances);

                if (maxStudentBonus < studentBonus)
                {
                    maxStudentBonus = studentBonus;
                    attendancesForMaxBonus = attendances;
                }
            }

            Console.WriteLine($"Max Bonus: {Math.Ceiling(maxStudentBonus)}.");
            Console.WriteLine($"The student has attended {attendancesForMaxBonus} lectures.");
        }

        static double TotalBonus(int bonus, int lectures, int attendances)
        {
            double totalBonus = (double)attendances / lectures * (5 + bonus);

            return totalBonus;
        }
    }
}
