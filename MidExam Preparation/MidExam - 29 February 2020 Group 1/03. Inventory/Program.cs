﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _03._Inventory
{
    class Program
    {
        static void Main()
        {
            List<string> items = Console.ReadLine().Split(", ").ToList();

            StringBuilder strb = new StringBuilder();

            string command;
            while ((command = Console.ReadLine()) != "Craft!")
            {
                string[] commandArgs = command.Split(" - ");

                if (commandArgs[0] == "Collect")
                {
                    Collect(items, commandArgs[1]);
                }
                else if (commandArgs[0] == "Drop")
                {
                    Drop(items, commandArgs[1]);
                }
                else if (commandArgs[0] == "Combine Items")
                {
                    CombineItems(items, commandArgs[1]);
                }
                else if (commandArgs[0] == "Renew")
                {
                    Renew(items, commandArgs[1]);
                }
            }

            for (int i = 0; i < items.Count; i++)
            {
                if (i < items.Count - 1)
                {
                    strb.Append($"{items[i]}, ");
                }
                else
                {
                    strb.Append($"{items[i]}");
                }
            }


            Console.WriteLine(strb.ToString());
        }

        static void Renew(List<string> items, string item)
        {
            if (items.Contains(item))
            {
                int index = items.FindIndex(x => x == item);
                string tempItem = items[index];

                items.RemoveAt(index);
                items.Add(tempItem);
            }
        }

        static void CombineItems(List<string> items, string item)
        {
            string[] itemArgs = item.Split(':');

            if (items.Contains(itemArgs[0]))
            {
                int index = items.FindIndex(x => x == itemArgs[0]);

                items.Insert(index + 1, itemArgs[1]);
            }
        }

        static void Drop(List<string> items, string item)
        {
            if (items.Contains(item))
            {
                int index = items.FindIndex(x => x == item);
                items.RemoveAt(index);
            }
        }

        static void Collect(List<string> items, string item)
        {
            if (!items.Contains(item))
            {
                items.Add(item);
            }
        }
    }
}
