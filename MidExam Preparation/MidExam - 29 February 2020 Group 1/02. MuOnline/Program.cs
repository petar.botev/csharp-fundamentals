﻿using System;
using System.Linq;
using System.Text;

namespace _02._MuOnline
{
    class Program
    {
        private static int health = 100;
        private static int allBitcoints = 0;
        private static int bestRoom = 0;
        private static bool isDead = false;


        static void Main()
        {
            string[] rooms = Console.ReadLine().Split('|').ToArray();

            StringBuilder strb = new StringBuilder();

            for (int i = 0; i < rooms.Length; i++)
            {
                bestRoom++; // coud not be here!
                string[] roomArgs = rooms[i].Split();

                if (roomArgs[0] == "potion")
                {
                    UsePotion(roomArgs[1]);
                }
                else if (roomArgs[0] == "chest")
                {
                    GetChest(roomArgs[1]);
                }
                else
                {
                    EncounterMonster(roomArgs[0], roomArgs[1]);

                    if (isDead)
                    {
                        return;
                    }
                }
            }

            strb.AppendLine("You've made it!")
                .AppendLine($"Bitcoins: {allBitcoints}")
                .Append($"Health: {health}");

            Console.WriteLine(strb.ToString());

        }

        static void EncounterMonster(string monster, string attackPower)
        {
            health -= int.Parse(attackPower);

            if (health <= 0)
            {
                Console.WriteLine($"You died! Killed by {monster}.");
                Console.WriteLine($"Best room: {bestRoom}");
                isDead = true;
            }
            else
            {
                Console.WriteLine($"You slayed {monster}.");
            }
        }

        static void GetChest(string bitcoins)
        {
            allBitcoints += int.Parse(bitcoins);

            Console.WriteLine($"You found {int.Parse(bitcoins)} bitcoins.");
        }

        static void UsePotion(string potionHealth)
        {
            int addedHealth = 100 - health;

            health += int.Parse(potionHealth);

            if (health > 100)
            {
                health = 100;
            }
            else
            {
                addedHealth = int.Parse(potionHealth);
            }

            Console.WriteLine($"You healed for {addedHealth} hp.");
            Console.WriteLine($"Current health: {health} hp.");
        }
    }
}
