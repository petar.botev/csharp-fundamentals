﻿using System;

namespace _01._Disneyland_Journey
{
    class Program
    {
        static void Main()
        {
            decimal journeyCosts = decimal.Parse(Console.ReadLine());

            int months = int.Parse(Console.ReadLine());

            decimal allMoney = 0;

            for (int i = 1; i <= months; i++)
            {
                decimal monthlySavedMoney = journeyCosts / 4;

                if (i % 2 != 0 && i != 1)
                {
                    decimal monthlySpentMoney = allMoney * 0.16m;

                    allMoney -= monthlySpentMoney;
                }

                if (i % 4 == 0)
                {
                    decimal bonus = allMoney * 0.25m;
                    allMoney += bonus;
                }

                allMoney += monthlySavedMoney;

                

                
            }

            if (journeyCosts > allMoney)
            {
                decimal notEnoughMoney = journeyCosts - allMoney;
                Console.WriteLine($"Sorry. You need {notEnoughMoney:f2}lv. more.");
            }
            else
            {
                decimal moneyForSouvenirs = allMoney - journeyCosts;
                Console.WriteLine($"Bravo! You can go to Disneyland and you will have {moneyForSouvenirs:f2}lv. for souvenirs.");
            }
        }
    }
}
