﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _02._Archery_Tournament
{
    class Program
    {
        static void Main()
        {
            List<int> targets = Console.ReadLine().Split('|').Select(x => int.Parse(x)).ToList();

            int archerPoints = 0;

            string command;

            while ((command = Console.ReadLine()) != "Game over")
            {
                string[] commandArgs = command.Split();

                if (commandArgs.Length == 1)
                {
                    targets.Reverse();
                }
                else
                {
                    string[] moveParams = commandArgs[1].Split('@');

                    string direction = moveParams[0];
                    int startIndex = int.Parse(moveParams[1]);
                    int moveLength = int.Parse(moveParams[2]);

                    if (startIndex < 0 || startIndex > targets.Count - 1)
                    {
                        continue;
                    }

                    if (direction == "Left")
                    {
                        int targetIndex = LeftTargetIndex(targets, startIndex, ref moveLength);

                        if (targetIndex > targets.Count - 1)
                        {
                            targetIndex = 0;
                        }

                        archerPoints = HitTarget(targets, targetIndex, archerPoints);
                    }
                    else if (direction == "Right")
                    {

                        int targetIndex = RightTargetIndex(moveLength, startIndex, targets);

                        if (targetIndex < 0)
                        {
                            targetIndex = targets.Count - 1;
                        }

                        archerPoints = HitTarget(targets, targetIndex, archerPoints);
                    }
                    
                }
            }

            Console.WriteLine(string.Join(" - ", targets));
            Console.WriteLine($"Iskren finished the archery tournament with {archerPoints} points!");
        }

        private static int LeftTargetIndex(List<int> targets, int startIndex, ref int moveLength)
        {
            int targetIndex = startIndex;

            while (moveLength > 0)
            {
                targetIndex -= 1;

                if (targetIndex < 0)
                {
                    targetIndex = targets.Count - 1;
                }
                moveLength--;
            }

            return targetIndex;
        }

        static int HitTarget(List<int> targets, int targetIndex, int archerPoints)
        {
            if (targets[targetIndex] < 5)
            {
                archerPoints += targets[targetIndex];
                targets[targetIndex] -= targets[targetIndex];
            }
            else
            {
                archerPoints += 5;
                targets[targetIndex] -= 5;
            }

            return archerPoints;
        }

        static int RightTargetIndex(int moveLength, int startIndex, List<int> targets)
        {
            int targetIndex = startIndex;

            while (moveLength > 0)
            {
                targetIndex += 1;

                if (targetIndex > targets.Count - 1)
                {
                    targetIndex = 0;
                }
                moveLength--;
            }

            return targetIndex;
        }
    }
}
