﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _03._School_Library
{
    class Program
    {
        static void Main()
        {
            List<string> bookShelf = Console.ReadLine().Split('&').ToList();

            string checkBook = "";

            string command;

            while ((command = Console.ReadLine()) != "Done")
            {
                string[] splittedCommand = command.Split(" | ");
                string theCommand = splittedCommand[0].Split()[0];
                string book = splittedCommand[1];
                string secondBook = null;
                

                if (splittedCommand.Length > 2)
                {
                    secondBook = splittedCommand[2];
                }

                if (theCommand == "Add")
                {
                    AddBook(bookShelf, book);
                }
                else if (theCommand == "Take")
                {
                    TakeBook(bookShelf, book);
                }
                else if (theCommand == "Swap")
                {
                    SwapBooks(bookShelf, book, secondBook);
                }
                else if (theCommand == "Insert")
                {
                    InsertBook(bookShelf, book);
                }
                else if (theCommand == "Check")
                {
                    checkBook = CheckBook(bookShelf, book);
                }
            }

            Console.WriteLine(checkBook);
            Console.WriteLine(string.Join(", ", bookShelf));
        }

        static void AddBook(List<string> bookShelf, string book)
        {
            if (!IsBookExists(bookShelf, book))
            {
                bookShelf.Insert(0, book);
            }
        }

        static void TakeBook(List<string> bookShelf, string book)
        {
            if (IsBookExists(bookShelf, book))
            {
                bookShelf.Remove(book);
            }
        }

        static void SwapBooks(List<string> bookShelf, string firstBook, string secondBook)
        {
            if (IsBookExists(bookShelf, firstBook) && IsBookExists(bookShelf, secondBook))
            {
                string tempFB = firstBook;

                int firstBookIndex = bookShelf.FindIndex(b => b == firstBook);
                int secondBookIndex = bookShelf.FindIndex(b => b == secondBook);

                bookShelf.Remove(firstBook);
                bookShelf.Insert(firstBookIndex, secondBook);
                bookShelf.RemoveAt(secondBookIndex);
                bookShelf.Insert(secondBookIndex, tempFB);
            }
        }

        static void InsertBook(List<string> bookShelf, string book)
        {
            bookShelf.Add(book);
        }

        static string CheckBook(List<string> bookShelf, string book)
        {
            int bookIndex = int.Parse(book);
            string theBook = "";

            if (bookIndex <= bookShelf.Count - 1)
            {
                theBook = bookShelf[bookIndex];
            }

            return theBook;
        }

        static bool IsBookExists(List<string> bookShelf, string book)
        {
            return bookShelf.Contains(book);
        }
    }
}
