﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _02._Array_Modifier
{
    class Program
    {
        static void Main()
        {
            List<int> integerList = Console.ReadLine().Split().Select(x => int.Parse(x)).ToList();

            string command;

            while ((command = Console.ReadLine()) != "end")
            {
                string[] commandArgs = command.Split();

                if (commandArgs[0] == "swap")
                {
                    Swap(integerList, commandArgs[1], commandArgs[2]);
                }
                else if (commandArgs[0] == "multiply")
                {
                    Multiply(integerList, commandArgs[1], commandArgs[2]);
                }
                else if (commandArgs[0] == "decrease")
                {
                    Decrease(integerList);
                }
            }

            Console.WriteLine(string.Join(", ", integerList));
        }

        static void Swap(List<int> integerList, string firstIndex, string secondIndex)
        {
            int[] parsedIndexes = ParseIndexes(firstIndex, secondIndex);

            int[] values = GetValues(integerList, firstIndex, secondIndex);

            integerList[parsedIndexes[0]] = values[1];
            integerList[parsedIndexes[1]] = values[0];
            
        }

        static void Multiply(List<int> integerList, string firstIndex, string secondIndex)
        {
            int[] indexes = ParseIndexes(firstIndex, secondIndex);

            int multiplyResult = integerList[indexes[0]] * integerList[indexes[1]];

            integerList[indexes[0]] = multiplyResult;
        }

        static void Decrease(List<int> integerList)
        {
            for (int i = 0; i < integerList.Count; i++)
            {
                integerList[i] -= 1;
            }
        }




        static int[] GetValues(List<int> integerList, string firstIndex, string secondIndex)
        {
            int[] indexes = ParseIndexes(firstIndex, secondIndex);

            int firstIndexValue = integerList[indexes[0]];
            int secondIndexValue = integerList[indexes[1]];

            int[] indexValues = new int[] { firstIndexValue, secondIndexValue };

            return indexValues;
        }

        static int[] ParseIndexes(string firstIndex, string secondIndex)
        {
            int firstIndexParsed = int.Parse(firstIndex);
            int secondIndexParsed = int.Parse(secondIndex);

            int[] parsedIndexes = new int[] { firstIndexParsed, secondIndexParsed};

            return parsedIndexes;
        }
    }
}
