﻿using System;
using System.Linq;

namespace _03._Numbers
{
    class Program
    {
        static void Main()
        {
            int[] numbers = Console.ReadLine().Split().Select(x => int.Parse(x)).ToArray();

            double average = AverageNumber(numbers);

            int[] result = numbers.Where(x => x > average).OrderByDescending(x => x).Take(5).ToArray();

            if (result.Length == 0)
            {
                Console.WriteLine("No");
            }
            else
            {
                Console.WriteLine(string.Join(' ', result));
            }
        }

        static double AverageNumber(int[] numbers)
        {
            double average = (double)numbers.Sum() / numbers.Length;

            return average;
        }
    }
}
