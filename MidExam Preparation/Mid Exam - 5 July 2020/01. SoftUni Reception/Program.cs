﻿using System;

namespace _01._SoftUni_Reception
{
    class Program
    {
        static void Main()
        {
            int firstEmployee = int.Parse(Console.ReadLine());
            int secondEmployee = int.Parse(Console.ReadLine());
            int thirdEmployee = int.Parse(Console.ReadLine());

            int studentPerHour = firstEmployee + secondEmployee + thirdEmployee;

            int numberStudents = int.Parse(Console.ReadLine());

            int hours = 0;

            while (numberStudents > 0)
            {
                numberStudents -= studentPerHour;
                hours++;

                if (hours % 4 == 0)
                {
                    hours++;
                }
            }

            Console.WriteLine($"Time needed: {hours}h.");
        }
    }
}
