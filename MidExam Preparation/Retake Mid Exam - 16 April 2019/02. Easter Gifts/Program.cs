﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _02._Easter_Gifts
{
    class Program
    {
        static void Main()
        {
            List<string> gifts = Console.ReadLine().Split().ToList();

            string command;
            while ((command = Console.ReadLine()) != "No Money")
            {
                string[] splittedCommand = command.Split();
                string theCommand = splittedCommand[0];
                string newGift = splittedCommand[1];
                int giftIndex = 0;

                if (splittedCommand.Length > 2)
                {
                    giftIndex = int.Parse(splittedCommand[2]);
                }

                if (theCommand == "OutOfStock")
                {
                    Remove(newGift, gifts);
                }
                else if (theCommand == "Required")
                {
                    Replace(newGift, giftIndex, gifts);
                }
                else if (theCommand == "JustInCase")
                {
                    ReplaceLast(newGift, gifts);
                }
            }

            gifts = gifts.Where(x => x != "None").ToList();

            Console.WriteLine(string.Join(' ', gifts));
        }

        static void Remove(string gift, List<string> gifts)
        {
            for (int i = 0; i < gifts.Count; i++)
            {
                if (gifts[i] == gift)
                {
                    gifts[i] = "None";
                }
            }
        }

        static void Replace(string gift, int index, List<string> gifts)
        {
            if (!(index < 0) && !(index > gifts.Count - 1))
            {
                gifts.RemoveAt(index);
                gifts.Insert(index, gift);
            }
        }

        static void ReplaceLast(string gift, List<string> gifts)
        {
            gifts.RemoveAt(gifts.Count - 1);
            gifts.Add(gift);
        }
    }
}
