﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _03._Easter_Shopping
{
    class Program
    {
        static void Main()
        {
            List<string> shops = Console.ReadLine().Split().ToList();

            int numberOfCommands = int.Parse(Console.ReadLine());

            for (int i = 0; i < numberOfCommands; i++)
            {
                string[] commands = Console.ReadLine().Split();
                string theCommand = commands[0];
                string shop = commands[1];
                string shopIndex = "";

                if (commands.Length > 2)
                {
                    shopIndex = commands[2];
                }

                if (theCommand == "Include")
                {
                    Include(shops, shop);
                }
                else if (theCommand == "Visit")
                {
                    Visit(shops, shop, int.Parse(shopIndex));
                }
                else if (theCommand == "Prefer")
                {
                    Prefer(shops, shop, shopIndex);
                }
                else if (theCommand == "Place")
                {
                    Place(shops, shop, int.Parse(shopIndex));
                }

            }

            Console.WriteLine("Shops left:");
            Console.WriteLine(string.Join(' ', shops));
        }

        static void Include(List<string> shops, string shop)
        {
            shops.Add(shop);
        }

        static void Visit(List<string> shops, string shop, int numberOfShops)
        {
            if (numberOfShops <= shops.Count)
            {
                if (shop == "first")
                {
                    shops.RemoveRange(0, numberOfShops);
                }
                else
                {
                    shops.RemoveRange((shops.Count - 1) - (numberOfShops - 1), numberOfShops);
                }
            }
            
        }

        static void Prefer(List<string> shops, string firstShop, string secondShop)
        {
            int firstShopIndex = int.Parse(firstShop);
            int secondShopIndex = int.Parse(secondShop);

            if (firstShopIndex < shops.Count && firstShopIndex >= 0 && secondShopIndex < shops.Count && secondShopIndex >= 0)
            {
                string shopOne = shops[firstShopIndex];
                string shopTwo = shops[secondShopIndex];

                shops.RemoveAt(firstShopIndex);
                shops.Insert(firstShopIndex, shopTwo);
                shops.RemoveAt(secondShopIndex);
                shops.Insert(secondShopIndex, shopOne);
            }
        }

        static void Place(List<string> shops, string shop, int shopIndex)
        {
            if (shopIndex < shops.Count && shopIndex >= 0)
            {
                if (shopIndex == shops.Count - 1)
                {
                    shops.Add(shop);
                }
                else
                {
                    shops.Insert(shopIndex + 1, shop);

                }
            }
        }
    }
}
