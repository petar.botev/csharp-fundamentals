﻿using System;

namespace _01._Easter_Cozonacs
{
    class Program
    {
        static void Main()
        {
            decimal budget = decimal.Parse(Console.ReadLine());

            decimal flourPrice = decimal.Parse(Console.ReadLine());

            decimal eggsPrice = flourPrice * 0.75m;
            decimal milckPrice = (flourPrice + (flourPrice * 0.25m)) / 4;

            decimal cozonacPrice = flourPrice + eggsPrice + milckPrice;

            int colouredEggs = 0;
            int cozonacCount = 0;

            while ((budget - cozonacPrice) >= 0)
            {
                cozonacCount++;
                budget -= cozonacPrice;
                colouredEggs += 3;

                if (cozonacCount % 3 == 0)
                {
                    colouredEggs -= (cozonacCount - 2);
                }
            }

            Console.WriteLine($"You made {cozonacCount} cozonacs! Now you have {colouredEggs} eggs and {budget:f2}BGN left.");
        }
    }
}
