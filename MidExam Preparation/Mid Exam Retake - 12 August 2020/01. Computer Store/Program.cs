﻿using System;
using System.Text;

namespace _01._Computer_Store
{
    class Program
    {
        static void Main()
        {
            decimal fullPrice = 0;
            decimal taxes = 0;
            decimal allTaxes = 0;
            decimal discount = 0;

            while (true)
            {
                string price = Console.ReadLine();

                if (price == "special")
                {
                    discount = fullPrice / 10;
                    fullPrice -= discount;
                    break;
                }
                else if (price == "regular")
                {
                    break;
                }
                else if (decimal.Parse(price) <= 0)
                {
                    Console.WriteLine("Invalid price!");
                }
                else
                {
                    decimal decPrice = decimal.Parse(price);
                    taxes = decPrice / 5;
                    allTaxes += taxes;

                    fullPrice += (decPrice + taxes);

                }
            }

            if (fullPrice == 0)
            {
                Console.WriteLine("Invalid order!");
            }
            else
            {
                PrintResult(fullPrice, allTaxes, discount);
            }
        }

        static void PrintResult(decimal fullPrice, decimal allTaxes, decimal discount)
        {
            StringBuilder strb = new StringBuilder();

            strb.AppendLine("Congratulations you've just bought a new computer!")
                .AppendLine($"Price without taxes: {fullPrice - allTaxes + discount:f2}$")
                .AppendLine($"Taxes: {allTaxes:f2}$")
                .AppendLine("-----------")
                .Append($"Total price: {fullPrice:f2}$");

            Console.WriteLine(strb.ToString());
        }
    }
}
