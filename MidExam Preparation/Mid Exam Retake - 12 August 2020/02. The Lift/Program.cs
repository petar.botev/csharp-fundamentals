﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _02._The_Lift
{
    class Program
    {
        static void Main()
        {
            int people = int.Parse(Console.ReadLine());

            int[] wagons = Console.ReadLine().Split().Select(x => int.Parse(x)).ToArray();

            bool[] isLiftFull = new bool[wagons.Length];

            for (int i = 0; i < isLiftFull.Length; i++)
            {
                isLiftFull[i] = false;
            }

            List<bool> isFullWagon = new List<bool>();

            bool isPassAllWagons = false;

            int maxPeopleInWagon = 4;

            bool isWagonFull = false;

            for (int i = 0; i < wagons.Length; i++)
            {
                int freeSits = 0;

                if (wagons[i] == maxPeopleInWagon)
                {
                    isLiftFull[i] = true;
                    continue;
                }

                freeSits = maxPeopleInWagon - wagons[i];

                if (people < 4)
                {
                    wagons[i] += people;
                    people = 0;
                }
                else
                {
                    people -= freeSits;
                    wagons[i] += freeSits;
                }

                if (wagons[i] == maxPeopleInWagon)
                {
                    isLiftFull[i] = true;
                }
                
                if (people <= 0)
                {
                    break;
                }

                if (i == wagons.Length - 1)
                {
                    isPassAllWagons = true;
                }
            }

            if (people == 0 && isPassAllWagons == false && isLiftFull.Contains(false))
            {
                Console.WriteLine("The lift has empty spots!");
                Console.WriteLine(string.Join(' ', wagons));
            }
            else if (people == 0 && isPassAllWagons == true && isLiftFull.Contains(false))
            {
                Console.WriteLine("The lift has empty spots!");
                Console.WriteLine(string.Join(' ', wagons));
            }
            else if (people > 0)
            {
                Console.WriteLine($"There isn't enough space! {people} people in a queue!");
                Console.WriteLine(string.Join(' ', wagons));
            }
            else if (!isLiftFull.Contains(false) && people == 0)
            {
                Console.WriteLine(string.Join(' ', wagons));
            }
        }
    }
}
