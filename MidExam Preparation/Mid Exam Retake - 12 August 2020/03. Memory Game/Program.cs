﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _03._Memory_Game
{
    class Program
    {
        static void Main()
        {
            List<string> intList = Console.ReadLine().Split().ToList();

            int numberOfMoves = 0;


            if (!CheckMatchingElements(intList))
            {
                Console.WriteLine($"You have won in {numberOfMoves} turns!");
                return;
            }

            string pairIndexes;

            while ((pairIndexes = Console.ReadLine().Trim()) != "end")
            {
                numberOfMoves++;
                int[] parsedIndexes = pairIndexes.Split().Select(x => int.Parse(x)).ToArray();

                if (parsedIndexes[0] == parsedIndexes[1] || parsedIndexes.Any(x => x < 0) || parsedIndexes.Any(x => x >= intList.Count))
                {
                    Console.WriteLine("Invalid input! Adding additional elements to the board");
                    intList.InsertRange(intList.Count / 2, new string[]{ $"-{numberOfMoves}a", $"-{numberOfMoves}a"});
                }
                else
                {
                    if (intList[parsedIndexes[0]] == intList[parsedIndexes[1]])
                    {
                        Console.WriteLine($"Congrats! You have found matching elements - {intList[parsedIndexes[0]]}!");

                        if (parsedIndexes[0] > parsedIndexes[1])
                        {
                            intList.RemoveAt(parsedIndexes[0]);
                            intList.RemoveAt(parsedIndexes[1]);
                        }
                        else
                        {
                            intList.RemoveAt(parsedIndexes[1]);
                            intList.RemoveAt(parsedIndexes[0]);
                        }

                        if (!CheckMatchingElements(intList))
                        {
                            Console.WriteLine($"You have won in {numberOfMoves} turns!");
                            return;
                        }
                    }
                    else
                    {
                        Console.WriteLine("Try again!");
                    }

                }
            }

            Console.WriteLine("Sorry you lose :(");
            Console.WriteLine(string.Join(' ', intList));
        }

        static bool CheckMatchingElements(List<string> intList)
        {
            for (int i = 0; i < intList.Count - 1; i++)
            {
                for (int j = i + 1; j < intList.Count; j++)
                {
                    if (intList[i] == intList[j])
                    {
                        return true;
                    }
                }
            }

            return false;
        }
    }
}
