﻿using System;

namespace P01CounterStrike
{
    class Program
    {
        static void Main()
        {
            int initialEnergy = int.Parse(Console.ReadLine());

            string distance = Console.ReadLine();
            int leftEnergy;
            int wonGames = 0;

            while (distance != "End of battle")
            {
                int distanceInt = int.Parse(distance);

                initialEnergy -= distanceInt;

                if (initialEnergy < 0)
                {
                    leftEnergy = initialEnergy + distanceInt;
                    Console.WriteLine($"Not enough energy! Game ends with {wonGames} won battles and {leftEnergy} energy");
                    return;
                }

                wonGames++;

                if (wonGames % 3 == 0)
                {
                    initialEnergy += wonGames;
                }

                distance = Console.ReadLine();
            }

            Console.WriteLine($"Won battles: {wonGames}. Energy left: {initialEnergy}");
        }
    }
}
