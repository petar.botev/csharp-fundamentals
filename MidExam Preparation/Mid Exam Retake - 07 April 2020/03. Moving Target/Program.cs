﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _03._Moving_Target
{
    public class Program
    {
        static List<double> targets;

        static void Main()
        {
            targets = Console.ReadLine().Split().Select(t => double.Parse(t)).ToList();

            string command;

            while ((command = Console.ReadLine()) != "End")
            {
                string[] splittedCommand = command.Split();

                if (splittedCommand[0].ToString() == "Shoot")
                {
                    Shot(splittedCommand);
                }
                else if (splittedCommand[0].ToString() == "Add")
                {
                    Add(splittedCommand);
                }
                else if (splittedCommand[0].ToString() == "Strike")
                {
                    Strike(splittedCommand);
                }
            }

            End(targets);
        }

        static List<double> Shot(string[] command)
        {
            int index = int.Parse(command[1]);
            double power = double.Parse(command[2]);

            if (targets.Count - 1 >= index && index >= 0)
            {
                targets[index] -= power;

                if (targets[index] <= 0)
                {
                    targets.RemoveAt(index);
                }
            }

            return targets;
        }

        static List<double> Add(string[] command)
        {
            int index = int.Parse(command[1]);
            double value = double.Parse(command[2]);

            if (index > targets.Count - 1 || index < 0)
            {
                Console.WriteLine("Invalid placement!");
            }
            else
            {
                targets.Insert(index, value);
            }

            return targets;
        }

        static List<double> Strike(string[] command)
        {
            int index = int.Parse(command[1]);
            int radius = int.Parse(command[2]);

            if (index - radius < 0 || index + radius > targets.Count - 1)
            {
                Console.WriteLine("Strike missed!");
            }
            else
            {
                targets.RemoveRange(index - radius, radius * 2 + 1);
            }

            return targets;
        }

        static void End(List<double> targets)
        {
            StringBuilder strb = new StringBuilder();

            for (int i = 0; i < targets.Count; i++)
            {
                if (i == targets.Count - 1)
                {
                    strb.Append($"{targets[i]}");
                }
                else
                {
                    strb.Append($"{targets[i]}|");

                }
            }

            Console.WriteLine(strb.ToString());
        }
    }
}
