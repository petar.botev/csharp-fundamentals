﻿using System;
using System.Linq;
using System.Text;

namespace _02._Shoot_for_the_Win
{
    class Program
    {
        static void Main()
        {
            int[] targets = Console.ReadLine().Split().Select(t => int.Parse(t)).ToArray();

            int countShots = 0;

            string index;

            while ((index = Console.ReadLine()) != "End")
            {
                int indexInt = int.Parse(index);

                if (indexInt > targets.Length - 1)
                {
                    continue;
                }

                int safedTarget = targets[indexInt];

                targets[indexInt] = -1;
                countShots++;

                for (int i = 0; i < targets.Length; i++)
                {
                    if (targets[i] != -1)
                    {
                        if (targets[i] > safedTarget)
                        {
                            targets[i] -= safedTarget; 
                        }
                        else
                        {
                            targets[i] += safedTarget;
                        }
                    }
                }
            }

            Console.WriteLine($"Shot targets: {countShots} -> {string.Join(' ', targets)}");
        }
    }
}
