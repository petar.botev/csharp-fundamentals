﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _02._Shopping_List
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> groceries = Console.ReadLine().Split('!').ToList();

            string command;

            while ((command = Console.ReadLine()) != "Go Shopping!")
            {
                string[] commandArgs = command.Split();

                if (commandArgs[0] == "Urgent")
                {
                    if (!groceries.Contains(commandArgs[1]))
                    {
                        Urgent(groceries, commandArgs[1]);
                    }
                }
                else if (commandArgs[0] == "Unnecessary")
                {
                    if (groceries.Contains(commandArgs[1]))
                    {
                        Remove(groceries, commandArgs[1]);
                    }
                }
                else if (commandArgs[0] == "Correct")
                {
                    if (groceries.Contains(commandArgs[1]))
                    {
                        Correct(groceries, commandArgs[1], commandArgs[2]);
                    }
                }
                else if (commandArgs[0] == "Rearrange")
                {
                    if (groceries.Contains(commandArgs[1]))
                    {
                        Rearrange(groceries, commandArgs[1]);
                    }
                }
            }

            Console.WriteLine(string.Join(", ", groceries));
        }

        static List<string> Urgent(List<string> groceries, string toAdd)
        {
            groceries.Insert(0, toAdd);
            return groceries;
        }

        static List<string> Remove(List<string> groceries, string toAdd)
        {
            int index = groceries.FindIndex(g => g == toAdd);
            groceries.RemoveAt(index);

            return groceries;
        }

        static List<string> Correct(List<string> groceries, string oldItem, string newItem)
        {
            int index = groceries.FindIndex(g => g == oldItem);

            groceries.RemoveAt(index);
            groceries.Insert(index, newItem);

            return groceries;
        }

        static List<string> Rearrange(List<string> groceries, string toAdd)
        {
            int index = groceries.FindIndex(g => g == toAdd);

            groceries.RemoveAt(index);
            groceries.Add(toAdd);

            return groceries;
        }
    }
}
