﻿using System;

namespace _01._National_Court
{
    class Program
    {
        static void Main(string[] args)
        {
            double fullEfficiency = 0;

            for (int i = 0; i < 3; i++)
            {
                double efficiencyPerHour = double.Parse(Console.ReadLine());
                fullEfficiency += efficiencyPerHour;
            }

            double allPeople = double.Parse(Console.ReadLine());
            double hoursNeeded = 0;

            while (allPeople > 0)
            {
                hoursNeeded++;
                allPeople -= fullEfficiency;

                if (hoursNeeded % 4 == 0)
                {
                    hoursNeeded++;
                }
            }

            Console.WriteLine($"Time needed: {hoursNeeded}h.");
        }
    }
}
