﻿using System;
using System.Linq;

namespace _03._Heart_Delivery
{
    class Program
    {
        static void Main()
        {
            int[] houses = Console.ReadLine().Split('@').Select(x => int.Parse(x)).ToArray();

            string command;
            int index = 0;

            while ((command = Console.ReadLine()) != "Love!")
            {
                string[] commandArgs = command.Split();
                int step = int.Parse(commandArgs[1]);

                int houseIndex = index + step;

                if (houseIndex > houses.Length - 1)
                {
                    index = 0;
                    houseIndex = 0;
                    
                    if (houses[index] == 0)
                    {
                        Console.WriteLine($"Place {index} already had Valentine's day.");
                    }
                    else
                    {
                        houses[index] = houses[index] - 2;

                        if (houses[index] < 1)
                        {
                            Console.WriteLine($"Place {index} has Valentine's day.");
                        }
                    }

                    
                }
                else
                {
                    if (houses[houseIndex] == 0)
                    {
                        Console.WriteLine($"Place {houseIndex} already had Valentine's day.");
                    }
                    else
                    {
                        houses[houseIndex] = houses[houseIndex] - 2;

                        if (houses[houseIndex] < 1)
                        {
                            Console.WriteLine($"Place {houseIndex} has Valentine's day.");
                        }
                    }

                    index = houseIndex;
                }
            }

            Console.WriteLine($"Cupid's last position was {index}.");

            int[] failedHouses = houses.Where(h => h > 0).ToArray();

            if (failedHouses.Length > 0)
            {
                Console.WriteLine($"Cupid has failed {failedHouses.Length} places.");
            }
            else
            {
                Console.WriteLine($"Mission was successful.");
            }
        }
    }
}
