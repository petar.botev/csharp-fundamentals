﻿using System;

namespace _01._Cooking_Masterclass
{
    class Program
    {
        static void Main()
        {
            decimal budget = decimal.Parse(Console.ReadLine());
            int students = int.Parse(Console.ReadLine());
            decimal flourPrice = decimal.Parse(Console.ReadLine());
            decimal oneRggPrice = decimal.Parse(Console.ReadLine());
            decimal apronPrice = decimal.Parse(Console.ReadLine());

            decimal oneStudentBudget = flourPrice + (oneRggPrice * 10) + apronPrice;

            decimal currentAllStudentBudget = oneStudentBudget * students;

            decimal additionalApronsPrice = (decimal)Math.Ceiling(students * 0.2) * apronPrice;
            decimal freeFlourPrice = flourPrice * ((decimal)Math.Floor((double)students / 5));

            decimal finalExpense = (currentAllStudentBudget + additionalApronsPrice) - freeFlourPrice;

            decimal neededMoney = finalExpense - budget;

            if (budget >= finalExpense)
            {
                Console.WriteLine($"Items purchased for {finalExpense:f2}$.");
            }
            else
            {
                Console.WriteLine($"{neededMoney:f2}$ more needed.");
            }
        }
    }
}
