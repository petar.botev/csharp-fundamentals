﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _03._Messaging
{
    class Program
    {
        static List<string> messages = new List<string>();

        static void Main()
        {
            string command;

            while ((command = Console.ReadLine()) != "end")
            {
                string[] splittedCommand = command.Split();

                if (splittedCommand[0] == "Chat")
                {
                    Chat(splittedCommand);
                }
                else if (splittedCommand[0] == "Delete")
                {
                    Delete(splittedCommand);
                }
                else if (splittedCommand[0] == "Edit")
                {
                    Edit(splittedCommand);
                }
                else if (splittedCommand[0] == "Pin")
                {
                    Pin(splittedCommand);
                }
                else if (splittedCommand[0] == "Spam")
                {
                    Spam(splittedCommand);
                }
            }

            StringBuilder strb = new StringBuilder();

            for (int i = 0; i < messages.Count; i++)
            {
                strb.AppendLine(messages[i]);
            }

            Console.WriteLine(strb.ToString().Trim());
        }

        static void Chat(string[] splittedCommand)
        {
            messages.Add(splittedCommand[1]);
        }

        static void Delete(string[] splittedCommand)
        {
            if (messages.Contains(splittedCommand[1]))
            {
                messages.Remove(splittedCommand[1]);
            }
        }

        static void Edit(string[] splittedCommand)
        {
            int index = messages.FindIndex(x => x == splittedCommand[1]);

            messages.RemoveAt(index);
            messages.Insert(index, splittedCommand[2]);
        }

        static void Pin(string[] splittedCommand)
        {
            string message = messages.FirstOrDefault(x => x == splittedCommand[1]);
            messages.Remove(message);
            messages.Add(message);
        }

        static void Spam(string[] splittedCommand)
        {
            for (int i = 1; i < splittedCommand.Length; i++)
            {
                messages.Add(splittedCommand[i]);
            }
        }
    }
}
