﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _02._Loot
{
    class Program
    {
        static List<string> treasureContent;
        static void Main()
        {
            treasureContent = Console.ReadLine().Split('|', StringSplitOptions.RemoveEmptyEntries).ToList();

            string command;

            while ((command = Console.ReadLine()) != "Yohoho!")
            {
                string[] splittedCommand = command.Split();

                if (splittedCommand[0] == "Loot")
                {
                    GainLoot(splittedCommand);
                }
                else if (splittedCommand[0] == "Drop")
                {
                    DropLoot(int.Parse(splittedCommand[1]));
                }
                else if (splittedCommand[0] == "Steal")
                {
                    Steal(int.Parse(splittedCommand[1]));
                }
            }
            

            if (treasureContent.Count == 0)
            {
                Console.WriteLine("Failed treasure hunt.");
            }
            else
            {
                double averageGain = (double)treasureContent.Sum(x => x.Length) / treasureContent.Count;
                Console.WriteLine($"Average treasure gain: {averageGain:f2} pirate credits.");

            }
        }

        static void GainLoot(string[] command)
        {
            for (int i = 1; i < command.Length; i++)
            {
                if (!treasureContent.Contains(command[i]))
                {
                    treasureContent.Insert(0, command[i]);
                }
            }
        }

        static void DropLoot(int index)
        {
            if (index < treasureContent.Count && index > -1)
            {
                string item = treasureContent[index];

                treasureContent.RemoveAt(index);
                treasureContent.Add(item);
            }
        }

        static void Steal(int count)
        {
            List<string> stollenItems = new List<string>();

            for (int i = 0; i < count; i++)
            {
                if (treasureContent.Count == 0)
                {
                    break;
                }
                string lastItem = treasureContent.Last();
                treasureContent.RemoveAt(treasureContent.Count - 1);
                stollenItems.Add(lastItem);
            }

            stollenItems.Reverse();

            Console.WriteLine(string.Join(", ", stollenItems));
        }
    }
}
