﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace _03._Heroes_of_Code_and_Logic_VII
{
    class Program
    {
        static void Main()
        {
            int numberOfHeroes = int.Parse(Console.ReadLine());

            Dictionary<string, int[]> heroDict = new Dictionary<string, int[]>();

            for (int i = 0; i < numberOfHeroes; i++)
            {
                string[] heroParams = Console.ReadLine().Split();

                heroDict.Add(heroParams[0], new int[] { int.Parse(heroParams[1]), int.Parse(heroParams[2]) });
            }


            string command;
            while ((command = Console.ReadLine()) != "End")
            {
                string[] commandArgs = command.Split(" - ", StringSplitOptions.RemoveEmptyEntries);

                string heroName = commandArgs[1];

                switch (commandArgs[0])
                {
                    case "CastSpell":

                        int spellMP = int.Parse(commandArgs[2]);
                        string spellName = commandArgs[3];

                        if (heroDict[heroName][1] >= spellMP)
                        {
                            Console.WriteLine($"{heroName} has successfully cast {spellName} and now has {heroDict[heroName][1] -= spellMP} MP!");
                        }
                        else
                        {
                            Console.WriteLine($"{heroName} does not have enough MP to cast {spellName}!");
                        }

                        break;

                    case "TakeDamage":

                        int damage = int.Parse(commandArgs[2]);
                        string attacker = commandArgs[3];

                        if (heroDict[heroName][0] > damage)
                        {
                            Console.WriteLine($"{heroName} was hit for {damage} HP by {attacker} and now has {heroDict[heroName][0] -= damage} HP left!");
                        }
                        else
                        {
                            Console.WriteLine($"{heroName} has been killed by {attacker}!");
                            heroDict.Remove(heroName);
                        }

                        break;

                    case "Recharge":

                        int amount = int.Parse(commandArgs[2]);

                        int newMP = heroDict[heroName][1] + amount;

                        int recharge;

                        if (newMP > 200)
                        {
                            recharge = 200 - heroDict[heroName][1];
                            heroDict[heroName][1] = 200;
                        }
                        else
                        {
                            recharge = amount;
                            heroDict[heroName][1] = newMP;
                        }

                        Console.WriteLine($"{heroName} recharged for {recharge} MP!");

                        break;

                    case "Heal":

                        int amountHP = int.Parse(commandArgs[2]);

                        int newHP = heroDict[heroName][0] + amountHP;

                        int rechargeHP;

                        if (newHP > 100)
                        {
                            rechargeHP = 100 - heroDict[heroName][0];
                            heroDict[heroName][0] = 100;
                        }
                        else
                        {
                            rechargeHP = amountHP;
                            heroDict[heroName][0] = newHP;
                        }

                        Console.WriteLine($"{heroName} healed for {rechargeHP} HP!");

                        break;

                    default:
                        break;
                }
            }

            StringBuilder strb = new StringBuilder();

            heroDict = heroDict.OrderByDescending(x => x.Value[0]).ThenBy(x => x.Key).ToDictionary(k => k.Key, v => v.Value);

            foreach (var item in heroDict)
            {
                strb.AppendLine(item.Key);
                strb.AppendLine($"HP: {item.Value[0]}");
                strb.AppendLine($"MP: {item.Value[1]}");
            }

            Console.WriteLine(strb.ToString());
        }
    }
}
