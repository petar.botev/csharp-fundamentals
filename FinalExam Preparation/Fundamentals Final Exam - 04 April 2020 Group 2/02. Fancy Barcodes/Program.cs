﻿using System;
using System.Text;
using System.Text.RegularExpressions;

namespace _02._Fancy_Barcodes
{
    class Program
    {
        static void Main()
        {
            Regex validatingRegex = new Regex(@"@#+[A-Z][A-Za-z0-9]{4,}[A-Z]@#+");

            int barcodesCount = int.Parse(Console.ReadLine());

            StringBuilder strbDigits = new StringBuilder();

            for (int i = 0; i < barcodesCount; i++)
            {
                string barcode = Console.ReadLine();

                Match barcodeMatch = validatingRegex.Match(barcode);

                if (barcodeMatch.Success)
                {
                    for (int j = 0; j < barcode.Length; j++)
                    {

                        if (char.IsDigit(barcode[j]))
                        {
                            strbDigits.Append(barcode[j]);
                        }
                    }

                    if (strbDigits.Length == 0)
                    {
                        Console.WriteLine("Product group: 00");
                    }
                    else
                    {
                        Console.WriteLine($"Product group: {strbDigits}");
                        strbDigits.Clear();
                    }
                }
                else
                {
                    Console.WriteLine("Invalid barcode");
                }
            }
        }
    }
}
