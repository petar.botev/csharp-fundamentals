﻿using System;
using System.Linq;
using System.Text;

namespace _01._Password_Reset
{
    class Program
    {
        static void Main()
        {
            string encriptedPassword = Console.ReadLine();

            StringBuilder strbTakeOdd = new StringBuilder();

            string command;

            while ((command = Console.ReadLine()) != "Done")
            {
                string[] commandArgs = command.Split();

                switch (commandArgs[0])
                {
                    case "TakeOdd":

                        for (int i = 0; i < encriptedPassword.Length; i++)
                        {
                            if (i % 2 != 0)
                            {
                                strbTakeOdd.Append(encriptedPassword[i]);
                            }
                        }

                        encriptedPassword = strbTakeOdd.ToString();

                        Console.WriteLine(strbTakeOdd.ToString());

                        break;

                    case "Cut":

                        int startIndex = int.Parse(commandArgs[1]);
                        int length = int.Parse(commandArgs[2]);
                        
                        string stringToCut = encriptedPassword.Substring(startIndex, length);
                        int cutStartIndex = encriptedPassword.IndexOf(stringToCut);
                        encriptedPassword = encriptedPassword.Remove(cutStartIndex, length);

                        Console.WriteLine(encriptedPassword);

                        break;

                    case "Substitute":

                        string stringToRemove = commandArgs[1];
                        string stringToAdd = commandArgs[2];

                        if (encriptedPassword.Contains(stringToRemove))
                        {
                            encriptedPassword = encriptedPassword.Replace(stringToRemove, stringToAdd);
                            Console.WriteLine(encriptedPassword);
                        }
                        else
                        {
                            Console.WriteLine("Nothing to replace!");
                        }

                        break;

                    default:
                        throw new ArgumentException("The command doesn't exist!");
                }
            }

            Console.WriteLine($"Your password is: {encriptedPassword}");
        }
    }
}
