﻿using System;

namespace _01._Activation_Keys
{
    class Program
    {
        static void Main()
        {
            string inputString = Console.ReadLine();

            string command;

            while ((command = Console.ReadLine()) != "Generate")
            {
                string[] commandArgs = command.Split(">>>", StringSplitOptions.RemoveEmptyEntries);
                string instruction;
                string substring;
                string upperOrLower;
                int startIndex;
                int endIndex;

                if (commandArgs.Length == 2)
                {
                    instruction = commandArgs[0];
                    substring = commandArgs[1];

                    if (inputString.Contains(substring))
                    {
                        Console.WriteLine($"{inputString} contains {substring}");
                    }
                    else
                    {
                        Console.WriteLine("Substring not found!");
                    }
                }
                else if (commandArgs.Length == 4)
                {
                    instruction = commandArgs[0];
                    upperOrLower = commandArgs[1];
                    startIndex = int.Parse(commandArgs[2]);
                    endIndex = int.Parse(commandArgs[3]);

                    string oldSubstring;
                    string changedSubstring;

                    if (upperOrLower == "Upper")
                    {
                        oldSubstring = inputString.Substring(startIndex, endIndex - startIndex);
                        changedSubstring = inputString.Substring(startIndex, endIndex - startIndex).ToUpper();
                        inputString = inputString.Replace(oldSubstring, changedSubstring);

                        Console.WriteLine(inputString);
                    }
                    else
                    {
                        oldSubstring = inputString.Substring(startIndex, endIndex - startIndex);
                        changedSubstring = inputString.Substring(startIndex, endIndex - startIndex).ToLower();
                        inputString = inputString.Replace(oldSubstring, changedSubstring);

                        Console.WriteLine(inputString);
                    }
                }
                else
                {
                    instruction = commandArgs[0];
                    startIndex = int.Parse(commandArgs[1]);
                    endIndex = int.Parse(commandArgs[2]);

                    inputString = inputString.Remove(startIndex, endIndex - startIndex);

                    Console.WriteLine(inputString);
                }
            }

            Console.WriteLine($"Your activation key is: {inputString}");
        }
    }
}
