﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _03._P_rates
{
    class Program
    {
        static void Main()
        {
            Dictionary<string, long[]> cities = new Dictionary<string, long[]>();

            string city;

            while ((city = Console.ReadLine()) != "Sail")
            {
                string[] cityArgs = city.Split("||", StringSplitOptions.RemoveEmptyEntries);

                string cityName = cityArgs[0];
                long population = long.Parse(cityArgs[1]);
                long gold = long.Parse(cityArgs[2]);

                if (cities.ContainsKey(cityName))
                {
                    cities[cityName][0] += population;
                    cities[cityName][1] += gold;
                }
                else
                {
                    cities.Add(cityName, new long[] { population, gold });
                }
            }

            string events;

            while ((events = Console.ReadLine()) != "End")
            {
                string[] eventsArgs = events.Split("=>", StringSplitOptions.RemoveEmptyEntries);

                string townEvent = eventsArgs[0];
                string townName = eventsArgs[1];
                long gold;

                if (eventsArgs.Length == 4)
                {
                    long people = long.Parse(eventsArgs[2]);
                    gold = long.Parse(eventsArgs[3]);

                    long peopleInTown = cities[townName][0];
                    long goldInTown = cities[townName][1];

                    cities[townName][0] -= people;
                    cities[townName][1] -= gold;

                    if (cities[townName][0] <= 0 || cities[townName][1] <= 0)
                    {
                        if (cities[townName][0] <= 0)
                        {
                            Console.WriteLine($"{townName} plundered! {gold} gold stolen, {peopleInTown} citizens killed.");
                        }
                        else if (cities[townName][1] <= 0)
                        {
                            Console.WriteLine($"{townName} plundered! {goldInTown} gold stolen, {people} citizens killed.");
                        }
                        
                        cities.Remove(townName);
                        Console.WriteLine($"{townName} has been wiped off the map!");
                    }
                    else
                    {
                        Console.WriteLine($"{townName} plundered! {gold} gold stolen, {people} citizens killed.");
                    }
                }
                else
                {
                    gold = long.Parse(eventsArgs[2]);

                    if (gold > -1)
                    {
                        cities[townName][1] += gold;
                        Console.WriteLine($"{gold} gold added to the city treasury. {townName} now has {cities[townName][1]} gold.");
                    }
                    else
                    {
                        Console.WriteLine("Gold added cannot be a negative number!");
                    }
                }
            }

            if (cities.Count > 0)
            {
                cities = cities.OrderByDescending(x => x.Value[1]).ThenBy(x => x.Key).ToDictionary(k => k.Key, v => v.Value);

                Console.WriteLine($"Ahoy, Captain! There are { cities.Count } wealthy settlements to go to:");

                foreach (var item in cities)
                {
                    Console.WriteLine($"{item.Key} -> Population: {item.Value[0]} citizens, Gold: {item.Value[1]} kg");
                }
            }
            else
            {
                Console.WriteLine("Ahoy, Captain! All targets have been plundered and destroyed!");
            }

        }
    }
}
