﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text.RegularExpressions;

namespace _02._Emoji_Detector
{
    class Program
    {
        static void Main()
        {
            string sentence = Console.ReadLine();

            long coolnessThreshold = 1;
            long emojiCount;

            for (int i = 0; i < sentence.Length; i++)
            {
                if (char.IsDigit(sentence[i]))
                {
                    coolnessThreshold *= int.Parse(sentence[i].ToString());
                }
            }

            Regex emojiRegex = new Regex(@"([:]{2}|[*]{2})[A-Z][a-z]{2,}\1");

            var emojiMatches = emojiRegex.Matches(sentence).Select(x => x.Value).ToArray();

            emojiCount = emojiMatches.Length;


            List<string> coolEmojies = new List<string>();

            for (int i = 0; i < emojiMatches.Length; i++)
            {
                long emojiCharSum = 0;

                for (int j = 0; j < emojiMatches[i].Length; j++)
                {
                    if (char.IsLetter(emojiMatches[i][j]))
                    {
                        emojiCharSum += emojiMatches[i][j];
                    }
                }

                if (emojiCharSum > coolnessThreshold)
                {
                    coolEmojies.Add(emojiMatches[i]);
                }
            }

            Console.WriteLine($"Cool threshold: {coolnessThreshold}");

            Console.WriteLine($"{emojiCount} emojis found in the text. The cool ones are: ");
            Console.WriteLine(string.Join("\n", coolEmojies));

        }
    }
}
