﻿using System;

namespace _01._World_Tour
{
    class Program
    {
        static void Main()
        {
            string stops = Console.ReadLine();

            string command;

            while ((command = Console.ReadLine()) != "Travel")
            {
                string[] commandArgs = command.Split(':', StringSplitOptions.RemoveEmptyEntries);

                switch (commandArgs[0])
                {
                    case "Add Stop":

                        int index = int.Parse(commandArgs[1]);
                        string insert = commandArgs[2];

                        if (stops.Length > index && index > -1)
                        {
                            stops = stops.Insert(index, insert);
                        }

                        Console.WriteLine(stops);

                        break;

                    case "Remove Stop":

                        int startIndex = int.Parse(commandArgs[1]);
                        int endIndex = int.Parse(commandArgs[2]);

                        if (stops.Length > startIndex && stops.Length > endIndex && startIndex > -1 && startIndex > -1)
                        {
                            stops = stops.Remove(startIndex, (endIndex - startIndex) + 1);
                        }

                        Console.WriteLine(stops);

                        break;

                    case "Switch":

                        string oldStr = commandArgs[1];
                        string newStr = commandArgs[2];

                        if (stops.Contains(oldStr))
                        {
                            stops = stops.Replace(oldStr, newStr);
                        }

                        Console.WriteLine(stops);

                        break;

                    default:
                        break;
                }
            }

            Console.WriteLine($"Ready for world tour! Planned stops: {stops}");
        }
    }
}
