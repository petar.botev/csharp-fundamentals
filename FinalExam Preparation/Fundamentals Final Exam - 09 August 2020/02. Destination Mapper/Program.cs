﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace _02._Destination_Mapper
{
    class Program
    {
        static void Main()
        {
            string places = Console.ReadLine();

            Regex regex = new Regex(@"(=|\/)([A-Z][A-Za-z]{2,})\1");

            MatchCollection matches = regex.Matches(places);

            if (matches.Count > 0)
            {
                List<string> destinations = new List<string>();

                foreach (Match item in matches)
                {
                    destinations.Add(item.Groups[2].Value);
                }

                int points = 0;

                foreach (var item in destinations)
                {
                    points += item.Length; 
                }

                Console.WriteLine($"Destinations: {string.Join(", ", destinations)}");
                Console.WriteLine($"Travel Points: {points}");
            }
            else
            {
                Console.WriteLine($"Destinations:");
                Console.WriteLine($"Travel Points: {0}");
            }
        }
    }
}
