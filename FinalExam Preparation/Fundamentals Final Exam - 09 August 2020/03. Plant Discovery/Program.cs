﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _03._Plant_Discovery
{
    class Program
    {
        static void Main()
        {
            int number = int.Parse(Console.ReadLine());
            List<Plant> plantList = new List<Plant>();

            for (int i = 0; i < number; i++)
            {
                string[] plantInfo = Console.ReadLine().Split("<->", StringSplitOptions.RemoveEmptyEntries);

                Plant plant = new Plant();

                if (plantList.Any(x => x.Name == plantInfo[0]))
                {
                    plant.Rarity = int.Parse(plantInfo[1]);
                }
                else
                {
                    plant.Name = plantInfo[0];
                    plant.Rarity = int.Parse(plantInfo[1]);
                    plantList.Add(plant);
                }
            }

            string command;

            while ((command = Console.ReadLine()) != "Exhibition")
            {
                string[] commandArgs = command.Split(new string[] { ": ", " - " }, StringSplitOptions.RemoveEmptyEntries);

                string plantName = commandArgs[1];

                switch (commandArgs[0])
                {
                    case "Rate":

                        int rating = int.Parse(commandArgs[2]);

                        if (plantList.Any(x => x.Name == plantName))
                        {
                            plantList.FirstOrDefault(x => x.Name == plantName).Rating += rating;
                            plantList.FirstOrDefault(x => x.Name == plantName).numberRatings++;
                        }
                        else
                        {
                            Console.WriteLine("error");
                        }

                        break;
                    case "Update":

                        int rarity = int.Parse(commandArgs[2]);

                        if (plantList.Any(x => x.Name == plantName))
                        {
                            plantList.FirstOrDefault(x => x.Name == plantName).Rarity = rarity;
                        }
                        else
                        {
                            Console.WriteLine("error");
                        }

                        break;
                    case "Reset":

                        if (plantList.Any(x => x.Name == plantName))
                        {
                            plantList.FirstOrDefault(x => x.Name == plantName).Rating = 0;
                            plantList.FirstOrDefault(x => x.Name == plantName).numberRatings = 0;
                        }
                        else
                        {
                            Console.WriteLine("error");
                        }

                        break;
                    default:
                        Console.WriteLine("error");
                        break;
                }
            }

           plantList = plantList.OrderByDescending(x => x.Rarity).ThenByDescending(x => x.Rating / x.numberRatings).ToList();

            Console.WriteLine("Plants for the exhibition:");

            foreach (var item in plantList)
            {
                if (item.Rating != 0)
                {
                    Console.WriteLine($"- {item.Name}; Rarity: {item.Rarity}; Rating: {(item.Rating / item.numberRatings):f2}");
                }
                else
                {
                    Console.WriteLine($"- {item.Name}; Rarity: {item.Rarity}; Rating: {0:f2}");
                }
            }
        }
    }

    class Plant
    {
        public string Name { get; set; }
        public int Rarity { get; set; }
        public double Rating { get; set; }
        public int numberRatings { get; set; }
    }
}
