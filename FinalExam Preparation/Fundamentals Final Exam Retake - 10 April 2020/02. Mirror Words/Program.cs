﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace _02._Mirror_Words
{
    class Program
    {
        static void Main()
        {
            string input = Console.ReadLine();

            Regex regex = new Regex(@"([#|@])[A-Za-z]{3,}\1([#|@])[A-Za-z]{3,}\2");

            List<string> mirrorWords = new List<string>();

            var pairs = regex.Matches(input).Select(x => x.Value).ToArray();

            if (pairs.Length > 0)
            {
                foreach (var item in pairs)
                {
                    string[] words;

                    if (item[0] == '@')
                    {
                        words = item.Split('@', StringSplitOptions.RemoveEmptyEntries);
                    }
                    else
                    {
                        words = item.Split('#', StringSplitOptions.RemoveEmptyEntries);
                    }

                    string firstWord = words[0];
                    string secondWord = words[1];

                    if (firstWord == string.Join("", secondWord.Reverse()))
                    {
                        mirrorWords.Add(firstWord + " <=> " + secondWord);
                    }
                }

                Console.WriteLine($"{pairs.Count()} word pairs found!");

                if (mirrorWords.Count == 0)
                {
                    Console.WriteLine("No mirror words!");
                }
                else
                {
                    Console.WriteLine("The mirror words are:");
                    Console.WriteLine(string.Join(", ", mirrorWords));
                }
            }
            else
            {
                Console.WriteLine("No word pairs found!");
                Console.WriteLine("No mirror words!");
            }
        }
    }
}
