﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _03._Need_for_Speed_III
{
    class Program
    {
        static void Main()
        {
            int numberOfCars = int.Parse(Console.ReadLine());

            Dictionary<string, int[]> carDict = new Dictionary<string, int[]>();

            for (int i = 0; i < numberOfCars; i++)
            {
                string[] carParams = Console.ReadLine().Split('|', StringSplitOptions.RemoveEmptyEntries);

                carDict.Add(carParams[0], new int[] { int.Parse(carParams[1]), int.Parse(carParams[2]) });
            }

            string command;
            while ((command = Console.ReadLine()) != "Stop")
            {
                string[] commandArgs = command.Split(" : ", StringSplitOptions.RemoveEmptyEntries);

                string carName = commandArgs[1];
                int distance;
                int neededFuel;
                int refuelLitters;
                int kilometers = 0;
                int revertKilometers = 0;
                int fuelInTank = carDict[carName][1];
                int carMileage = carDict[carName][0];

                switch (commandArgs[0])
                {
                    case "Drive":

                        distance = int.Parse(commandArgs[2]);
                        neededFuel = int.Parse(commandArgs[3]);

                        if (neededFuel <= fuelInTank)
                        {
                            carDict[carName][1] -= neededFuel;
                            carDict[carName][0] += distance;

                            Console.WriteLine($"{carName} driven for {distance} kilometers. {neededFuel} liters of fuel consumed.");
                        }
                        else
                        {
                            Console.WriteLine("Not enough fuel to make that ride");
                        }

                        if (carDict[carName][0] > 100000)
                        {
                            Console.WriteLine($"Time to sell the {carName}!");
                            carDict.Remove(carName);
                        }

                        break;
                    case "Refuel":

                        refuelLitters = int.Parse(commandArgs[2]);

                        int addedFuel;

                        if ((refuelLitters + fuelInTank) > 75)
                        {
                            addedFuel = 75 - fuelInTank;
                            fuelInTank = 75;

                            carDict[carName][1] = 75;
                        }
                        else
                        {
                            addedFuel = refuelLitters;
                            fuelInTank += refuelLitters;

                            carDict[carName][1] += addedFuel;
                        }

                        Console.WriteLine($"{carName} refueled with {addedFuel} liters");
                        break;
                    case "Revert":

                        revertKilometers = int.Parse(commandArgs[2]);

                        carDict[carName][0] -= revertKilometers;

                        if (carDict[carName][0] < 10000)
                        {
                            carDict[carName][0] = 10000;
                            revertKilometers = carDict[carName][0] - 10000;
                        }
                        else
                        {
                            Console.WriteLine($"{carName} mileage decreased by {revertKilometers} kilometers");
                        }

                        break;
                    default:
                        break;
                }
            }


            carDict = carDict.OrderByDescending(x => x.Value[0]).ThenBy(x => x.Key).ToDictionary(k => k.Key, v => v.Value);

            foreach (var car in carDict)
            {
                Console.WriteLine($"{car.Key} -> Mileage: {car.Value[0]} kms, Fuel in the tank: {car.Value[1]} lt.");
            }
        }
    }
}
