﻿using System;
using System.Linq;

namespace _01._Secret_Chat
{
    class Program
    {
        static void Main()
        {
            string message = Console.ReadLine();

            string command;

            while ((command = Console.ReadLine()) != "Reveal")
            {
                string[] commandArgs = command.Split(":|:", StringSplitOptions.RemoveEmptyEntries);

                string substring;

                switch (commandArgs[0])
                {
                    case "InsertSpace":

                        int insertIndex = int.Parse(commandArgs[1]);

                        message = message.Insert(insertIndex, " ");

                        Console.WriteLine(message);
                        break;

                    case "Reverse":

                        substring = commandArgs[1];

                        if (message.Contains(substring))
                        {
                            int startIndex = message.IndexOf(substring);
                            string reversedSubstring = string.Join("", message.Substring(startIndex, substring.Length).Reverse());
                            message = message.Remove(startIndex, substring.Length) + reversedSubstring;


                            Console.WriteLine(message);
                        }
                        else
                        {
                            Console.WriteLine("error");
                        }

                        break;

                    case "ChangeAll":

                        substring = commandArgs[1];
                        string replacement = commandArgs[2];

                        message = message.Replace(substring, replacement);

                        Console.WriteLine(message);
                        break;

                    default:
                        break;
                }
            }

            Console.WriteLine($"You have a new text message: {message}");
        }
    }
}
