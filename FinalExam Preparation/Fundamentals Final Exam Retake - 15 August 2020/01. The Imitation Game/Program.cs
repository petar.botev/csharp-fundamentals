﻿using System;
using System.Text;

namespace _01._The_Imitation_Game
{
    class Program
    {
        static void Main()
        {
            StringBuilder encryptedMessage = new StringBuilder();

            //string encryptedMessage = Console.ReadLine();

            encryptedMessage.Append(Console.ReadLine());

            string command;

            while ((command = Console.ReadLine()) != "Decode")
            {
                string[] commandArgs = command.Split('|', StringSplitOptions.RemoveEmptyEntries);

                switch (commandArgs[0])
                {
                    case "Move":

                        int n = int.Parse(commandArgs[1]);

                        string moveStr = encryptedMessage.ToString().Substring(0, n);
                        encryptedMessage.Remove(0, n);
                        encryptedMessage.Append(moveStr);

                        break;
                    case "Insert":

                        int index = int.Parse(commandArgs[1]);
                        string value = commandArgs[2];

                        encryptedMessage.Insert(index, value);

                        break;
                    case "ChangeAll":

                        string substring = commandArgs[1];
                        string replacement = commandArgs[2];

                        encryptedMessage.Replace(substring, replacement);

                        break;
                    default:
                        break;
                }
            }

            Console.WriteLine($"The decrypted message is: {encryptedMessage}");
        }
    }
}
