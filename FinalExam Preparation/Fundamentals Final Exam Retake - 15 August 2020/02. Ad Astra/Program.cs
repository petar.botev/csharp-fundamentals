﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace _02._Ad_Astra
{
    class Program
    {
        static void Main()
        {
            string text = Console.ReadLine();

            Regex regex = new Regex(@"(\||#)(?<name>[A-Za-z\s]+)\1(?<date>\d{2}\/\d{2}\/\d{2})\1(?<calories>\d+)\1");

            MatchCollection matches = regex.Matches(text);

            List<Food> foodList = new List<Food>();

            foreach (Match item in matches)
            {
                Food food = new Food();

                food.Name = item.Groups["name"].Value;
                food.Date = item.Groups["date"].Value;
                food.Calories = int.Parse(item.Groups["calories"].Value);

                foodList.Add(food);
            }

            int days = foodList.Sum(x => x.Calories) / 2000;

            Console.WriteLine($"You have food to last you for: { days } days!");

            foreach (var item in foodList)
            {
                Console.WriteLine($"Item: {item.Name}, Best before: {item.Date}, Nutrition: {item.Calories}");
            }
        }
    }

    class Food
    {
        public string Name { get; set; }
        public string Date { get; set; }
        public int Calories { get; set; }
    }
}
