﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace _03._The_Pianist
{
    class Program
    {
        static void Main()
        {
            int numberPieces = int.Parse(Console.ReadLine());

            List<PianoPiece> listPieces = new List<PianoPiece>();

            for (int i = 0; i < numberPieces; i++)
            {
                PianoPiece pieces = new PianoPiece();

                var pieceArgs = Console.ReadLine().Split('|', StringSplitOptions.RemoveEmptyEntries);

                pieces.Name = pieceArgs[0];
                pieces.Composer = pieceArgs[1];
                pieces.Key = pieceArgs[2];

                listPieces.Add(pieces);
            }

            string command;

            while ((command = Console.ReadLine()) != "Stop")
            {
                string[] commandArgs = command.Split('|', StringSplitOptions.RemoveEmptyEntries);

                string pieceName = commandArgs[1];

                switch (commandArgs[0])
                {
                    case "Add":

                        if (listPieces.Any(p => p.Name == pieceName))
                        {
                            Console.WriteLine($"{pieceName} is already in the collection!");
                        }
                        else
                        {
                            PianoPiece pianoPiece = new PianoPiece();

                            string composer = commandArgs[2];
                            string key = commandArgs[3];

                            pianoPiece.Name = pieceName;
                            pianoPiece.Composer = composer;
                            pianoPiece.Key = key;

                            listPieces.Add(pianoPiece);

                            Console.WriteLine($"{pieceName} by {composer} in {key} added to the collection!");
                        }

                        break;
                    case "Remove":

                        string pieceToRemove = commandArgs[1];

                        if (listPieces.Any(p => p.Name == pieceToRemove))
                        {
                            int indexToRemove = listPieces.FindIndex(p => p.Name == pieceToRemove);
                            listPieces.RemoveAt(indexToRemove);

                            Console.WriteLine($"Successfully removed {pieceToRemove}!");
                        }
                        else
                        {
                            Console.WriteLine($"Invalid operation! {pieceToRemove} does not exist in the collection.");
                        }
                        break;
                    case "ChangeKey":

                        string piece = commandArgs[1];
                        string changeKey = commandArgs[2];

                        if (listPieces.Any(p => p.Name == piece))
                        {
                            listPieces.Find(p => p.Name == piece).Key = changeKey;

                            Console.WriteLine($"Changed the key of {piece} to {changeKey}!");
                        }
                        else
                        {
                            Console.WriteLine($"Invalid operation! {piece} does not exist in the collection.");
                        }

                        break;
                    default:
                        break;
                }
            }

            listPieces = listPieces.OrderBy(p => p.Name).ThenBy(p => p.Composer).ToList();

            foreach (var item in listPieces)
            {
                Console.WriteLine($"{item.Name} -> Composer: {item.Composer}, Key: {item.Key}");
            }
        }
    }

    class PianoPiece
    {
        public string Name { get; set; }
        public string Composer { get; set; }
        public string Key { get; set; }
    }
}
